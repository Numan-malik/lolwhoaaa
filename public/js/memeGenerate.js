$(document).ready(function () {

    var canvas = document.getElementById('meme');
    ctx = canvas.getContext('2d');

    // core drawing function
    var drawMeme = function () {
        var img = document.getElementById('start-image');

        var fontSize = parseInt($('#text_font_size').val());

        var memeSize = parseInt($('#canvas_size').val());

        // set form field properties
        $('#text_top_offset').attr('max', memeSize);
        $('#text_bottom_offset').attr('max', memeSize);

        // initialize canvas element with desired dimensions
        canvas.width = memeSize;
        canvas.height = memeSize;

        ctx.clearRect(0, 0, canvas.width, canvas.height);

        // calculate minimum cropping dimension
        var croppingDimension = img.height;
        if (img.width < croppingDimension) {
            croppingDimension = img.width;
        }

        ctx.drawImage(img, 0, 0, croppingDimension, croppingDimension, 0, 0, memeSize, memeSize);


        ctx.lineWidth = parseInt($('#text_stroke_width').val());
        ctx.font = fontSize + 'pt sans-serif';
        ctx.strokeStyle = 'black';
        ctx.fillStyle = 'white';
        ctx.textAlign = 'center';
        ctx.textBaseline = 'top';

        var text1 = $('#text_top').val();
        //text1 = text1.toUpperCase();
        x = memeSize / 2;
        y = parseInt($('#text_top_offset').val());

        var lineHeight = fontSize + parseInt($('#text_line_height').val());
        var maxTextAreaWidth = memeSize * 0.85;

        wrapText(ctx, text1, x, y, maxTextAreaWidth, lineHeight, false);


        ctx.textBaseline = 'bottom';
        var text2 = $('#text_bottom').val();
        //text2 = text2.toUpperCase();
        y = parseInt($('#text_bottom_offset').val());

        wrapText(ctx, text2, x, y, maxTextAreaWidth, lineHeight, true);

    };


    // build inner container for wrapping text inside
    var wrapText = function (context, text, x, y, maxWidth, lineHeight, fromBottom) {
        var pushMethod = (fromBottom) ? 'unshift' : 'push';

        lineHeight = (fromBottom) ? -lineHeight : lineHeight;

        var lines = [];
        var y = y;
        var line = '';
        var words = text.split(' ');

        for (var n = 0; n < words.length; n++) {
            var testLine = line + ' ' + words[n];
            var metrics = context.measureText(testLine);
            var testWidth = metrics.width;

            if (testWidth > maxWidth) {
                lines[pushMethod](line);
                line = words[n] + ' ';
            } else {
                line = testLine;
            }
        }
        lines[pushMethod](line);

        for (var k in lines) {
            context.strokeText(lines[k], x, y + lineHeight * k);
            context.fillText(lines[k], x, y + lineHeight * k);
        }
    };

    // $('#js-url-generate-img').keyup(function () {
    //     //load image from URL
    //     $("#start-image").attr('src', $('#js-url-generate-img').val());
    // });



    // read selected input image from upload field and display it in browser
    $('.js-generate-meme').change(function () {
        var fileExtension = ['jpeg', 'jpg', 'png', 'gif', 'bmp'];
        if (this.files[0].width < 200 || this.files[0].height < 200) {
            $('#generate-img-err').html("Minimum Image dimensions is 200 by 200.");
            $(':input[name="jsGenerateShare"]').prop('disabled', true);
        }
        if (this.files && this.files[0]) {
            if ($.inArray($(this).val().split('.').pop().toLowerCase(), fileExtension) == -1) {
                $('#generate-img-err').html("Only formats are allowed : " + fileExtension.join(', '));
                $(':input[name="jsGenerateShare"]').prop('disabled', true);

            }
            else if (this.files[0].size > 2000000) {
                $('#generate-img-err').html("Max file size is 2 MB .");
                $(':input[name="jsGenerateShare"]').prop('disabled', true);
            }
            else {
                var reader = new FileReader();
                reader.onload = function (e) {
                    var a = $('#start-image').attr('src', e.target.result);
                    var img = a[0].src;
                    var url = $('#js-generate-file').data('url');
                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('[name="_token"]').val()
                        }
                    });
                    $.ajax({
                        type: "POST",
                        url: url,
                        dataType: 'JSON',
                        data: {
                            meme: img
                        },
                        success: function (data) {
                            // var v = data.data.value;

                        },
                    });
                };
                
                reader.readAsDataURL(this.files[0]);
                $('#generate-img-err').html('');
                $(':input[name="jsGenerateShare"]').prop('disabled', false);

            }
        }
        window.setTimeout(function () {
            drawMeme();
        }, 500);

    });

    //check image dimensions

    var _URL = window.URL || window.webkitURL;

    $('#js-generate-file').change(function (e) {
        var file, img;

        if ((file = this.files[0])) {
            img = new Image();
            img.onload = function () {
                if (this.width < 200 || this.height < 200) {
                    $('#generate-img-err').html("Minimum Image dimensions is 200 by 200.");
                    $(':input[name="jsGenerateShare"]').prop('disabled', true);
                }
            };
            img.src = _URL.createObjectURL(file);
        }

    });

    // register event listeners

    $(document).on('change keydown keyup', '#text_top', function () {
        drawMeme();
    });

    $(document).on('change keydown keyup', '#text_bottom', function () {
        drawMeme();
    });

    $(document).on('input change', '#text_top_offset', function () {
        $('#text_top_offset__val').text($(this).val());
        drawMeme();
    });

    $(document).on('input change', '#text_bottom_offset', function () {
        $('#text_bottom_offset__val').text($(this).val());
        drawMeme();
    });

    $(document).on('input change', '#text_font_size', function () {
        $('#text_font_size__val').text($(this).val());
        drawMeme();
    });

    $(document).on('input change', '#text_line_height', function () {
        $('#text_line_height__val').text($(this).val());
        drawMeme();
    });

    $(document).on('input change', '#text_stroke_width', function () {
        $('#text_stroke_width__val').text($(this).val());
        drawMeme();
    });

    $(document).on('input change', '#canvas_size', function () {
        $('#canvas_size__val').text($(this).val());
        drawMeme();
    });

    // TODO: replace this with a server-side processing method
    $('#download_meme').click(function (e) {
        $(this).attr('href', canvas.toDataURL());
        $(this).attr('download', 'meme.png');

    });

    $('#js--GenerateMeme').click(function () {
        var img = canvas.toDataURL();
        var url = $('#js--GenerateMeme').data('url');
        var title = $('#title').val();
        var tags = $('#tags').val();
        var category = $('#category').val();

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('[name="_token"]').val()
            }
        });
        $.ajax({
            type: "POST",
            url: url,
            data: {
                meme: img,
                title:title,
                tags:tags,
                category:category
            },
            success: function (data) {
                if (data.status == 200) {
                    $('#shareMemeWithLolwhoa').modal('hide');
                    window.location = data.redirectUrl;
                }
                else if (data.status == 400) {
                    arr = data.errors;
                    for (i = 0; i < arr.length; i++) {
                        $('#js--msg').html('<div class="alert alert-danger m-15 alert-dismissible">' +
                            '<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>' + arr[i] +
                            '</div>').show();
                        return true;
                    }
                }
                else if (data.status == 401) {
                    arr = data.errors;
                    $('#js--msg').html('<div class="alert alert-danger m-15 alert-dismissible">' +
                        '<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>' +
                        'Image Not found'+
                        '</div>').show();
                }
            },
        });
    });

    // init at startup
    window.setTimeout(function () {
        drawMeme();
    }, 100);

    // //store meme
    // $('#js--share-with-lolwhoa').on('click',function () {
    //     alert();
    // });
    // $.ajaxSetup({
    //     headers: {
    //         'X-CSRF-TOKEN': $('[name="_token"]').val()
    //     }
    // });
    // $.ajax({
    //     type: "POST",
    //     url: url,
    //     dataType: 'JSON',
    //     data: data,
    //     success: function (data) {
    //         // var v = data.data.value;
    //
    //     },
    // });

});
