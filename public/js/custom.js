//Login & Register Authentication
$(document).ready(function () {
    $('#js-loginForm').on('submit', function (e) {
        e.preventDefault();
        $('#js-login-btn').html('Log in...');
        var url = $(this).data('url');

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('[name="_token"]').val()
            }
        });
        $.ajax({
            type: "POST",
            url: url,
            data: new FormData(this),
            dataType: 'JSON',
            contentType: false,
            cache: false,
            processData: false,
            success: function (data) {
                if (data.status == 200) {
                    window.location = data.RedirectUrl;
                }
                else if (data.status == 400) {
                    $('#js-login-btn').html('Log in');
                    $('#login-err').html('<div class="alert alert-danger m-15 alert-dismissible">' +
                        '<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>' +
                        'Invalid Email or password '+
                        '</div>').show();

                }
            },
        });
        $(function () {
            setTimeout(function () {
                $('#login-err').hide()
            }, 8000);
        });
    });

    $('#js-signUpForm').on('submit', function (e) {
        e.preventDefault();
        var url = $(this).data('url');

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('[name="_token"]').val()
            }
        });
        $.ajax({
            type: "POST",
            url: url,
            data: new FormData(this),
            dataType: 'JSON',
            contentType: false,
            cache: false,
            processData: false,
            success: function (data) {
                if (data.status == 200) {
                    window.location = data.RedirectUrl;
                }
                else if (data.status == 400) {
                    arr = data.errors;
                    for (i = 0; i < arr.length; i++) {
                        $('#register-error').html('<div class="alert alert-danger m-15 alert-dismissible">' +
                            '<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>' + arr[i] +
                            '</div>').show();

                    }
                }
            },
        });
        $(function () {
            setTimeout(function () {
                $('#register-error').hide()
            }, 8000);
        });
    });
});

//contact form
$('#js-contactForm').on('submit', function (e) {
    e.preventDefault();
    $('#js-contact').html('Sending....');
    var url = $(this).data('url');

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('[name="_token"]').val()
        }
    });
    $.ajax({
        type: "POST",
        url: url,
        data: new FormData(this),
        dataType: 'JSON',
        contentType: false,
        cache: false,
        processData: false,
        success: function (data) {
            if (data.status == 200) {
                $('#js-contact').html('Send');
                $('#js-contactForm').html('');
                $('#msgSent').html('<img src="images/tick1.png" width="130px">' +
                    '<h4 class="clr-wht m-t-10">Message Sent ! <br>\n' +
                    '<small class="navbar-color font-15">We will get back to you soon.</small>\n' +
                    '</h4>');

            }
            else if (data.status == 400) {
                $('#js-contact').html('Send');
                arr = data.errors;
                for (i = 0; i < arr.length; i++) {
                    $('#js-msg').html('<div class="alert alert-danger alert-dismissible">' +
                        '<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>' + arr[i] +
                        '</div>').show();

                }

            }
        },
    });
    $(function () {
        setTimeout(function () {
            $('#js-msg').hide()
        }, 8000);
    });
});

//Auto complete Search
$(document).ready(function () {
    $('#search').on('keyup', function (e) {
        if (e.keyCode == 32) {
            return false;
        }
        var query = $(this).val();
        var url = $(this).data('url');
        $.ajax({
            url: url,
            type: 'GET',
            data: {
                query: query
            },
            success: function (data) {
                $('#list').html(data);
            }
        })

    });

    $(document).on('click', 'li', function () {
        var value = $(this).text();
        $('#search').val(value);
        $('#list').html("");
    });

});

//scroll back to top

$(document).ready(function () {
    if ($('#back-to-top').length) {
        var scrollTrigger = 100, // px
            backToTop = function () {
                var scrollTop = $(window).scrollTop();
                if (scrollTop > scrollTrigger) {
                    $('#back-to-top').addClass('show');
                } else {
                    $('#back-to-top').removeClass('show');
                }
            };
        backToTop();
        $(window).on('scroll', function () {
            backToTop();
        });
        $('#back-to-top').on('click', function (e) {
            e.preventDefault();
            $('html,body').animate({
                scrollTop: 0
            }, 700);
        });
    }
});

$(document).ready(function () {

    $('#beforeTitle').show();
    $('#afterTitle').hide();
    $('#UrlContent').hide();
    $('.validUrlMsg').hide();
    $(':input[name="submitPost"]').prop('disabled', true);

    //Post Meme image
    $(function () {
        $('.memeImage').change(function () {
            var fileExtension = ['jpeg', 'jpg', 'png', 'gif', 'bmp'];
            if ($.inArray($(this).val().split('.').pop().toLowerCase(), fileExtension) == -1) {
                $('#img-err').html("Only formats are allowed : " + fileExtension.join(', '));
                $(':input[name="submitPost"]').prop('disabled', true);

            }
            else if (this.files[0].size > 2000000) {
                $('#img-err').html("Max file size is 2 MB .");
                $(':input[name="submitPost"]').prop('disabled', true);
            }
            else {
                if (this.files && this.files[0]) {
                    var reader = new FileReader();
                    reader.onload = imageIsLoaded;
                    reader.readAsDataURL(this.files[0]);
                    $('#img-err').html('');
                    $(':input[name="submitPost"]').prop('disabled', false);

                }
            }

        });
    });


    function imageIsLoaded(e) {
        $('#myImg').attr('src', e.target.result);
    };

    //check image dimensions

    var _URL = window.URL || window.webkitURL;

    $('#file').change(function (e) {
        var file, img;

        if ((file = this.files[0])) {
            img = new Image();
            img.onload = function () {
                if (this.width < 200 || this.height < 200) {
                    $('#img-err').html("Minimum Image dimensions is 200 by 200.");
                    $(':input[name="submitPost"]').prop('disabled', true);
                }
            };
            img.src = _URL.createObjectURL(file);
        }

    });

    //User Profile image
    $(function () {
        $('.ProfileImg').change(function () {
            var fileExtension = ['jpeg', 'jpg', 'png', 'gif', 'bmp'];
            if ($.inArray($(this).val().split('.').pop().toLowerCase(), fileExtension) == -1) {
                $('#img-err').html("Only formats are allowed : " + fileExtension.join(', '));
                $(':input[name="profileSubmit"]').prop('disabled', true);

            }
            else if (this.files[0].size > 2000000) {
                $('#img-err').html("Max file size is 2 MB .");
                $(':input[name="profileSubmit"]').prop('disabled', true);
            }
            else if (this.files[0].width < 200 || this.files[0].height < 200) {
                $('#img-err').html("Minimum Image dimensions is 200 by 200.");
                $(':input[name="profileSubmit"]').prop('disabled', true);
            }
            else {
                if (this.files && this.files[0]) {
                    var reader = new FileReader();
                    reader.onload = ProfileimageIsLoaded;
                    reader.readAsDataURL(this.files[0]);
                    $('#img-err').html('');
                    $(':input[name="profileSubmit"]').prop('disabled', false);
                }
            }

        });
    });


    function ProfileimageIsLoaded(e) {
        $('#ProfileImg').attr('src', e.target.result);
    };

    //call back  master meme modal
    $('.GetMasterMeme').on('click', function () {
        $('#beforePostMeme').show();
        $('#beforeTitle').show();
        $('#afterPostMeme').hide();
        $('#UrlContent').hide();
        $('#afterTitle').hide();

    });

    //content change

    var before = $('#beforePostMeme').show();
    var img = $('.memeImage').val();
    var after = $('#afterPostMeme').hide();


    //load meme detail content after upload image
    $('.memeImage').change(function () {
        after.show();
        before.hide();
        $('#beforeTitle').hide();
        $('#afterTitle').show();
    })

    //load url content
    $('#uploadURl').click(function () {
        $('#UrlContent').show();
        before.hide();
        $('#beforeTitle').hide();
        $('#afterTitle').hide();

    })

    //load meme detail content after input url
    $('#imglink').keyup(function () {
        //load image from URL
        $("#ImgURL").attr("src", $("#imglink").val());
    });

    //url validations

    $('#imglink').keyup(function () {
        var url = $('#imglink').val();
        var pattern = /(ftp|http|https):\/\/(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?/;
        if (pattern.test(url)) {
            $(':input[name="submitPost"]').prop('disabled', false);
            $('.validUrlMsg').hide();
        }
        else {
            $(':input[name="submitPost"]').prop('disabled', true);
            $('.validUrlMsg').show();
        }
    })

});

$(document).ready(function () {
    //email validation

    $(':input[name="settingForm"]').prop('disabled', false);
    $(':input[name="registerForm"]').prop('disabled', false);
    $(':input[name="loginForm"]').prop('disabled', false);

    $('#Emailvalidate').keyup(function () {

        var email = $("#Emailvalidate").val();

        if (email != 0) {
            if (isValidEmailAddress(email)) {
                $('#Emailvalidate').css('border-color', 'white');
                $(':input[name="settingForm"]').prop('disabled', false);
                $(':input[name="loginForm"]').prop('disabled', false);
                $('#validMsg').html('');

            } else {
                $('#validMsg').html('Please enter valid email !');
                $('#Emailvalidate').css('border-color', 'red');
                $(':input[name="settingForm"]').prop('disabled', true);
                $(':input[name="loginForm"]').prop('disabled', true);

            }
        }
    });

    $('#RegisterEmail').keyup(function () {

        var email = $('#RegisterEmail').val();

        if (email != 0) {
            if (isValidEmailAddress(email)) {
                $('#RegisterEmail').css('border-color', 'white');
                $(':input[name="registerForm"]').prop('disabled', false);

            } else {
                $('#RegisterEmail').css('border-color', 'red');
                $(':input[name="registerForm"]').prop('disabled', true);
            }
        }
    });

    function isValidEmailAddress(emailAddress) {
        var pattern = new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);
        return pattern.test(emailAddress);
    }
});

//only accept alphabets

function aplhaOnly(txt, e) {

    var inputValue = event.charCode;
    if (!(inputValue >= 65 && inputValue <= 120) && (inputValue != 32 && inputValue != 0)) {
        event.preventDefault();
    }
}

//Meme Votes
$(document).ready(function () {
    $('.vote').on('click', function () {
        var AuthCheck = $('#AuthCheck').val();
        if (AuthCheck == null) {
            $('#login').modal('show');
        }
        else {
            var data = {value: $(this).data('value'), meme_id: $(this).data('id')};
            var url = $(this).data('url');
            //console.log("data ",data);

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('[name="_token"]').val()
                }
            });
            $.ajax({
                type: "POST",
                url: url,
                dataType: 'JSON',
                data: data,
                success: function (data) {
                    // var v = data.data.value;

                },
            });
            var v = $(this).data('value');
            var id = $(this).data('id');
            //console.log("id",id);
            var islike = $('#upvote_' + id).data('islike');
            //console.log(" up count ",$('#upvote_' + id).data('islike'));
            var isdislike = $('#downvote_' + id).data('isdislike');
            //console.log(" down count ",isdislike);
            var id = $(this).data('id');
            var up = parseInt($('#upCount_' + id).html());
            var upcount = $('#upCount_' + id);
            var down = parseInt($('#downCount_' + id).html());

            if (v == 1) {
                // console.log("if");
                // console.log("up count",up);
                if (islike == 0) {
                    $('#upCount_' + id).data('value', up + 1);
                    $('#upCount_' + id).html(up + 1);
                    $(this).data('islike', 1);
                    $('#upvote_' + id).css('background', '#cbb6b64d');
                    // console.log("if and if child",$('#upCount_' + id));
                    var isdislike = $('#downvote_' + id).data('isdislike');
                    if (isdislike == 1) {
                        // console.log("is like");
                        $('#downvote_' + id).css('background', '#545454');
                        $('#downCount_' + id).data('value', down - 1);
                        $('#downCount_' + id).html(down - 1);
                        $('#downvote_' + id).data('isdislike', 0);
                    }
                } else {
                    if (up > 0) {
                        // console.log("jdjjd");
                        $('#upCount_' + id).data('value', up - 1);
                        $('#upCount_' + id).html(up - 1);
                    }
                    $(this).data('islike', 0);
                    // console.log(up-1);
                    $('#upvote_' + id).css('background', '#545454');

                    //console.log("if and else child",$('#upCount_' + id).data('value'));

                }

            }
            else if (v == 0) {
                var isdislike = $('#downvote_' + id).data('isdislike')
                // console.log("else ",isdislike);
                if (isdislike == 0) {

                    $('#downCount_' + id).data('value', down + 1);
                    $('#downCount_' + id).html(down + 1);
                    $('#downvote_' + id).data('isdislike', 1);

                    $('#downvote_' + id).css('background', '#cbb6b64d');
                    //console.log("else and if child",$('#downCount_' + id));
                    var islike = $('#upvote_' + id).data('islike');
                    if (islike == 1) {
                        //console.log("is like");
                        $('#upvote_' + id).css('background', '#545454');
                        $('#upCount_' + id).data('value', up - 1);
                        $('#upCount_' + id).html(up - 1);
                    }
                } else {
                    //  console.log("down ",down);
                    if (down > 0) {
                        $('#downCount_' + id).data('value', down - 1);
                        $('#downCount_' + id).html(down - 1);
                    }
                    $('#downvote_' + id).data('isdislike', 0);
                    // console.log(up-1);
                    $('#downvote_' + id).css('background', '#545454');

                }
                $('#upvote_' + id).data('islike', 0);

            }
        }

    });

});

//Comment Votes
$(document).ready(function () {
    $('.CommentVote').on('click', function () {
        var AuthCheck = $('#AuthCheck').val();
        if (AuthCheck == null) {
            $('#login').modal('show');
        }
        else {
            var data = {value: $(this).data('value'), comment_id: $(this).data('id')};
            var url = $(this).data('url');
            //console.log("data ",data);

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('[name="_token"]').val()
                }
            });
            $.ajax({
                type: "POST",
                url: url,
                dataType: 'JSON',
                data: data,
                success: function (data) {
                    // var v = data.data.value;

                },
            });
            var v = $(this).data('value');
            var id = $(this).data('id');
            //console.log("id",id);
            var islike = $('#upvote_' + id).data('islike');
            //console.log(" up count ",$('#upvote_' + id).data('islike'));
            var isdislike = $('#downvote_' + id).data('isdislike');
            //console.log(" down count ",isdislike);
            var id = $(this).data('id');
            var up = parseInt($('#upCount_' + id).html());
            var upcount = $('#upCount_' + id);
            var down = parseInt($('#downCount_' + id).html());

            if (v == 1) {
                // console.log("if");
                // console.log("up count",up);
                if (islike == 0) {
                    $('#upCount_' + id).data('value', up + 1);
                    $('#upCount_' + id).html(up + 1);
                    $(this).data('islike', 1);
                    $('#upvote_' + id).css('background', '#cbb6b64d');
                    // console.log("if and if child",$('#upCount_' + id));
                    var isdislike = $('#downvote_' + id).data('isdislike');
                    if (isdislike == 1) {
                        // console.log("is like");
                        $('#downvote_' + id).css('background', '#414141');
                        $('#downCount_' + id).data('value', down - 1);
                        $('#downCount_' + id).html(down - 1);
                        $('#downvote_' + id).data('isdislike', 0);
                    }
                } else {
                    if (up > 0) {
                        // console.log("jdjjd");
                        $('#upCount_' + id).data('value', up - 1);
                        $('#upCount_' + id).html(up - 1);
                    }
                    $(this).data('islike', 0);
                    // console.log(up-1);
                    $('#upvote_' + id).css('background', '#414141');

                    //console.log("if and else child",$('#upCount_' + id).data('value'));

                }

            }
            else if (v == 0) {
                var isdislike = $('#downvote_' + id).data('isdislike')
                // console.log("else ",isdislike);
                if (isdislike == 0) {

                    $('#downCount_' + id).data('value', down + 1);
                    $('#downCount_' + id).html(down + 1);
                    $('#downvote_' + id).data('isdislike', 1);

                    $('#downvote_' + id).css('background', '#cbb6b64d');
                    //console.log("else and if child",$('#downCount_' + id));
                    var islike = $('#upvote_' + id).data('islike');
                    if (islike == 1) {
                        //console.log("is like");
                        $('#upvote_' + id).css('background', '#414141');
                        $('#upCount_' + id).data('value', up - 1);
                        $('#upCount_' + id).html(up - 1);
                    }
                } else {
                    //  console.log("down ",down);
                    if (down > 0) {
                        $('#downCount_' + id).data('value', down - 1);
                        $('#downCount_' + id).html(down - 1);
                    }
                    $('#downvote_' + id).data('isdislike', 0);
                    // console.log(up-1);
                    $('#downvote_' + id).css('background', '#414141');

                }
                $('#upvote_' + id).data('islike', 0);

            }
        }

    });

});

//check auth login or not

$(document).ready(function () {
    $('.comment').on('click', function () {
        var AuthCheck = $('#AuthCheck').val();
        if (AuthCheck == null) {
            $(':input[name="SubmitComment"]').prop('disabled', true);
            $(':input[name="rplySubmit"]').prop('disabled', true);
            $('#login').modal('show');
        }
    })
});

//change status if notification seen
$('.notifications').on('click', function () {
    var url = $(this).data('url');

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('[name="_token"]').val()
        }
    });
    $.ajax({
        type: "POST",
        url: url,
        success: function (data) {

        },
    });
    $('#notifyCount').hide();
});


//change isReadStatus if notification read

$('.isReadNotify').on('click', function () {
    var attr = $(this).data('attr');
    var MemeId = $(this).data('meme');
    var CommentId = $(this).data('comment');
    var url = $(this).data('url');

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('[name="_token"]').val()
        }
    });
    $.ajax({
        type: "POST",
        url: url,
        dataType: 'JSON',
        data: {attr: attr, meme_id: MemeId, comment_id: CommentId},
        success: function (data) {

        },
    });
});

//count characters
function countChar(val) {
    if (val) {
        val.value = val.value.replace(/^\s+/, "");
    }

    var len = val.value.length;

    if (len >= 1000) {
        val.value = val.value.substring(0, 1000);
    } else {
        $('#charNum').text(len);
    }
};



//Date validation
$(document).ready(function () {
    $('.date').on('keyup', function () {
        var day = $("#day").val();
        var month = $("#month").val();
        var year = $("#year").val();
        var age = 10;
        var mydate = new Date();
        mydate.setFullYear(year, month - 1, day);

        var currdate = new Date();
        currdate.setFullYear(currdate.getFullYear() - age);
        if ((currdate - mydate) < 0 || year > 2009) {
            $(':input[name="profileSubmit"]').prop('disabled', true);
            $('#date-msg').html('Your age must be more than 10 years.');

        }
        else if (day.length < 2) {
            $(':input[name="profileSubmit"]').prop('disabled', true);
            $('#date-msg').html('Day must be between 01-31.');
        }
        else if (month.length < 2) {
            $(':input[name="profileSubmit"]').prop('disabled', true);
            $('#date-msg').html('Month must be between 01-12.');
        }
        else if (year < 1900) {
            $(':input[name="profileSubmit"]').prop('disabled', true);
            $('#date-msg').html('Please enter valid year of your age.');
        }
        else {
            $(':input[name="profileSubmit"]').prop('disabled', false);
            $('#date-msg').html('');
        }


    });
});

//Meme share counts

$(document).ready(function () {
    $('.share-btn').on('click', function () {
        var data = {meme_id: $(this).data('id')};
        var url = $(this).data('url');

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('[name="_token"]').val()
            }
        });
        $.ajax({
            type: "POST",
            url: url,
            dataType: 'JSON',
            data: data,
            success: function (data) {

            },
        });
    });

});
//check if user login then allow to share post
$(document).ready(function () {
    $('.s-btn').on('click', function () {
        var AuthCheck = $('#AuthCheck').val();
        if (AuthCheck == null) {
            $('#login').modal('show');
        }
    });

});

var links = document.getElementsByTagName("a"),
    i = 0, l = links.length,
    body = document.body;

for (; i < l; i++) {
    links[i].addEventListener("click", function () {
        body.className = "page-loading";
    }, false);
}


// Restricts input for the given textbox to the given inputFilter.
function setInputFilter(textbox, inputFilter) {
    ["input", "keydown", "keyup", "mousedown", "mouseup", "select", "contextmenu", "drop"].forEach(function (event) {
        textbox.addEventListener(event, function () {
            if (inputFilter(this.value)) {
                this.oldValue = this.value;
                this.oldSelectionStart = this.selectionStart;
                this.oldSelectionEnd = this.selectionEnd;
            } else if (this.hasOwnProperty("oldValue")) {
                this.value = this.oldValue;
                this.setSelectionRange(this.oldSelectionStart, this.oldSelectionEnd);
            }
        });
    });
}

// Install input filters.

setInputFilter(document.getElementById('day'), function (value) {
    return /^\d*$/.test(value) && (value === "" || parseInt(value) <= 31);
});
setInputFilter(document.getElementById('month'), function (value) {
    return /^\d*$/.test(value) && (value === "" || parseInt(value) <= 12);
});
setInputFilter(document.getElementById('year'), function (value) {
    return /^\d*$/.test(value)
});

$('#setting-form').on('submit', function (e) {
    e.preventDefault();
    var url = $(this).data('url');

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('[name="_token"]').val()
        }
    });
    $.ajax({
        type: "POST",
        url: url,
        data: new FormData(this),
        dataType: 'JSON',
        contentType: false,
        cache: false,
        processData: false,
        success: function (data) {
            if (data.status == 200) {
                $('#msg').html('<div class="alert setting-msg alert-dismissible">' +
                    '<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>' +
                    'Setting update successfully !' +
                    '</div>').show();

            }
            else if (data.status == 400) {
                arr = data.errors;
                for (i = 0; i < arr.length; i++) {
                    $('#msg').html('<div class="alert setting-msg alert-dismissible">' +
                        '<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>' + arr[i] +
                        '</div>').show();

                }

            }
            else if (data.status == 402) {
                $('#msg').html('<div class="alert setting-msg alert-dismissible">' +
                    '<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>' +
                    'You have no any changes !' +
                    '</div>').show();
            }
        },
        // error: function(xhr, status, error) {
        //     $('#msg').html('<div class="alert setting-msg alert-dismissible">' +
        //         '<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>' +
        //         '<strong>Success !</strong> This Email Has Already Taken !' +
        //         '</div>').show();
        // }
    });
    $(function () {
        setTimeout(function () {
            $('#msg').hide()
        }, 8000);
    });
});

$('#js-profile').on('submit', function (e) {
    e.preventDefault();
    var url = $(this).data('url');

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('[name="_token"]').val()
        }
    });
    $.ajax({
        type: "POST",
        url: url,
        data: new FormData(this),
        dataType: 'JSON',
        contentType: false,
        cache: false,
        processData: false,
        success: function (data) {
            if (data.status == 200) {
                $('#profile-msg').html('<div class="alert setting-msg alert-dismissible">' +
                    '<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>' +
                    'Profile update successfully !' +
                    '</div>').show();

            }
            else if (data.status == 400) {
                arr = data.errors;
                for (i = 0; i < arr.length; i++) {
                    $('#profile-msg').html('<div class="alert setting-msg alert-dismissible">' +
                        '<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>' + arr[i] +
                        '</div>').show();

                }

            }
        },
    });
    $(function () {
        setTimeout(function () {
            $('#profile-msg').hide()
        }, 8000);
    });
});

$('#js-profile-password').on('submit', function (e) {
    e.preventDefault();
    var url = $(this).data('url');

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('[name="_token"]').val()
        }
    });
    $.ajax({
        type: "POST",
        url: url,
        data: new FormData(this),
        dataType: 'JSON',
        contentType: false,
        cache: false,
        processData: false,
        success: function (data) {
            if (data.status == 200) {
                $('#password-msg').html('<div class="alert setting-msg alert-dismissible">' +
                    '<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>' +
                    'Password update successfully !' +
                    '</div>').show();

            }
            else if (data.status == 400) {
                arr = data.errors;
                for (i = 0; i < arr.length; i++) {
                    $('#password-msg').html('<div class="alert setting-msg alert-dismissible">' +
                        '<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>' + arr[i] +
                        '</div>').show();

                }

            }
            else if (data.status == 401) {
                $('#password-msg').html('<div class="alert setting-msg alert-dismissible">' +
                    '<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>' + data.errors +
                    '</div>').show();

            }
            else if (data.status == 402) {
                $('#password-msg').html('<div class="alert setting-msg alert-dismissible">' +
                    '<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>' + data.errors +
                    '</div>').show();

            }
            else if (data.status == 403) {
                $('#password-msg').html('<div class="alert setting-msg alert-dismissible">' +
                    '<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>' + data.errors +
                    '</div>').show();

            }
        },
    });
    $(function () {
        setTimeout(function () {
            $('#password-msg').hide()
        }, 8000);
    });
});


//content loading on scroll

//ajax pagination
// $(document).ready(function () {

//     $('.pagination a').on('click', function (event) {
//         event.preventDefault();

//         if ( $(this).attr('href') != '#' ) {
//             //$("html, body").animate({ scrollTop: 0 }, "slow");
//             $('#app').load($(this).attr('href'));
//             $('#load').append('<img style="position: absolute; left: 0; top: 0; z-index: 100000;"' +
//                 ' src="/images/loading.gif" />');
//         }
//     });

// })

