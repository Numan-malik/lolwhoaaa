<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});

// Route::get('/login/{id}', 'UserApiController\@index');

// Route::middleware('auth:api')->get('/user', function(Request $request) {
//     return $request->user();
// });

Route::post('/apiregister', 'API\UserApiController@register');
Route::post('/apilogin', 'API\UserApiController@login');
Route::post('/password/email', 'API\ForgotPasswordController@sendResetLinkEmail');
Route::post('/password/reset', 'API\ResetPasswordController@reset');
// contact laugh msg
Route::post('/api-inbox-laugh', 'API\ApiContactController@InboxLaughMsg');
// helper routes
Route::get('/api-categories', 'API\ApiHelperController@categories');
Route::get('/api-trending', 'API\ApiHelperController@trending');
Route::get('/api-recentMemes', 'API\ApiHelperController@recent_memes');
Route::get('/api-main-posts', 'API\ApiHelperController@homeMainPosts');
Route::get('/api-category-posts/{categorySlug}', 'API\ApiHelperController@getCategoryPosts');
Route::post('/api-search', 'API\ApiHelperController@apiSearch');
// single meme post details.
Route::get('/{slug}/{uuid}', 'API\ApiMemeController@memeDetail')->where('slug',
    '[\w\d\-\_]+');

Route::group(['middleware' => 'auth:api'], function(){
	Route::get('/apilogout', 'API\UserApiController@logoutApi');
	Route::post('/AuthUserDetails', 'API\UserApiController@AuthUserDetails');
	//setting route
	Route::post('/settings/update', 'API\ApiSettingcontroller@updateSetting');
	Route::post('/profile/update', 'API\ApiSettingcontroller@updateProfile');
	Route::post('/password/update', 'API\ApiSettingcontroller@updatePassword');
	// profile route
	Route::get('/user-profile/{parameter?}', 'API\ApiProfileController@getUserprofile');
	//meme route
	Route::post('api-share-meme','API\ApiMemeController@storeMeme');
	//Meme votes
	Route::post('api-vote-post', 'API\ApiMemeVotesController@apiStoreMemeVotes');
	// comment votes
	Route::post('/api-like-comment', 'API\ApiCommentController@StoreCommentVotes');
	//store post comment and comment reply
	Route::post('api-store-comment', 'API\ApiCommentController@apiStore');
	Route::post('api-store-reply-comment', 'API\ApiCommentController@apiStoreReply');
	// notification status
	Route::get('/api-all-notifications','API\ApiNotifyController@getAllNotify');
	Route::get('/api-recent-notifications','API\ApiNotifyController@getRecentNotify');
	Route::post('/api-update-NotifyStatus', 'API\ApiNotifyController@updateNotifyStatus');
	Route::post('/api-update-IsReadStatus', 'API\ApiNotifyController@updateIsReadStatus');
});


// Route::prefix('UserApi')->group(function () {
//     Route::post('/signUp', 'ApiController@signUp');

//     Route::match(['get', 'post'], '/login', 'ApiController@login');
//     Route::get('/logout', 'ApiController@logout');

// //Auth User Details

//    // Route::get('/AuthUserDetails/{id}', 'ApiController@AuthUserDetails');

// //SignUp Social Login

//     Route::get('auth/{provider}', 'Auth\ApiController@redirectToProvider')->name('auth');
//     Route::get('auth/{provider}/callback', 'Auth\ApiController@handleProviderCallback');
// });
