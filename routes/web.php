<?php

	Route::get('/', function () {
    return view('home');
});



//Route::get('/home', 'HomeController@index')->name('home');
//Route::get('/', 'HomeController@index')->name('home');

//////////////////////////////////////////Admin login/////////////////////////////////////////////////////////////

Route::match(['get', 'post'], '/admin/login', 'AdminController@adminlogin')->name('AdminLogin');
Route::get('/admin/logout', 'AdminController@adminlogout')->name('logoutAdmin');

//admin routes
Route::group(['prefix' => 'admin', 'middleware' => 'adminroute'], function () {

    Route::get('/dashboard', 'Dashboard@dashboard')->name('dashboard');
    Route::get('/user', 'AdminController@Users')->name('user');
    Route::get('/user/{id}', 'AdminController@DeleteUser')->name('delete_user');

    //admin profile update routes

    Route::get('/admin-profile', 'AdminController@AdminProfile')->name('adminProfile');
    Route::post('/admin-profile/update-admin-setting/{id}', 'AdminController@updateAdminSetting')->name('update-admin-setting');
    Route::post('/admin-profile/update-admin-profile/{id}', 'AdminController@updateAdminProfile')->name('update-admin-profile');
    Route::post('/admin-profile/update-admin-password/{id}', 'AdminController@updateAdminPassword')->name('update-admin-password');

    //category routes
    Route::post('/store-category', 'CategoriesController@StoreCategory')->name('create-category');
    Route::get('/category', 'CategoriesController@ShowCategory')->name('category');
    Route::post('/edit-category/{id}', 'CategoriesController@EditCategory');
    Route::post('/update-category/{id}', 'CategoriesController@UpdateCategory')->name('update_category');
    Route::get('/delete-category/{id}', 'CategoriesController@DeleteCategory')->name('delete_category');

    //admin memes routes

    Route::get('/all_memes', 'AdminController@AllMemes')->name('all_memes');
    Route::post('/create-memes', 'AdminController@CreateMemes')->name('create-memes');
    Route::get('/edit-meme/{id}', 'AdminController@EditMeme')->name('EditMeme');
    Route::post('/edit-meme/{id}', 'AdminController@UpdateMeme')->name('UpdateMeme');
    Route::get('/delete-memes/{id}', 'AdminController@deleteMemes')->name('delete_memes');

});

///////////////////////////// Front Routes ////////////////////////////////////////////////

Auth::routes();

//front auth routes

//Route::get('/signup','UserController@signup')->name('signup');
Route::post('/signup', 'UserController@signup')->name('signup');

//Route::get('/login','UserController@login')->name('login');
Route::match(['get', 'post'], '/login', 'UserController@login')->name('login');
Route::get('/logout', 'UserController@logout');

//SignUp Social Login
Route::get('auth/{provider}', 'Auth\RegisterController@redirectToProvider')->name('auth');
Route::get('auth/{provider}/callback', 'Auth\RegisterController@handleProviderCallback');

// User Profile
Route::get('/u/{username}/{id}/{parameter?}', 'FrontProfileController@getUserprofile');

//Contact Us
Route::get('/contact', 'ContactController@ContactUs')->name('ContactUs');
Route::post('SendContactMsg', 'ContactController@SendContactMsg')->name('SendContactMsg');

//setting
Route::get('/setting', 'SettingController@setting')->name('setting');

Route::post('/setting/update-setting/{id}', 'SettingController@updateSetting')->name('update-setting');
Route::post('/setting/update-profile/{id}', 'SettingController@updateProfile')->name('update-profile');
Route::post('/setting/update-password/{id}', 'SettingController@updatePassword')->name('update-password');

//Meme routes

Route::get('make-meme', 'MemeController@MemeGallery')->name('MemeGallery');
Route::get('edit/{id?}', 'MemeController@MemeGallery')->name('editMeme');
Route::post('generatedMemeStore', 'MemeController@generatedMemeStore')->name('generatedMemeStore');
Route::post('save', 'MemeController@CreateMeme')->name('CreateMeme');

Route::post('meme', 'MemeController@StoreMemes')->name('MemeStore');
Route::post('MemeShareCount', 'MemeController@MemeShareCount')->name('MemeShareCount');

//Meme votes
Route::post('storeMemeVotes', 'MemeVotesController@storeMemeVotes')->name('storeMemeVotes');
//Comment votes
Route::post('StoreCommentVotes', 'CommentVotesController@StoreCommentVotes')->name('StoreCommentVotes');

//Notifications
Route::post('updateNotifyStatus', 'NotificationsController@updateNotifyStatus')->name('updateNotifyStatus');
Route::post('updateIsReadStatus', 'NotificationsController@updateIsReadStatus')->name('updateIsReadStatus');
Route::get('me/notifications','NotificationsController@getAllNotify')->name('getAllNotify');


//Comments routes
//Route::resource('comments','CommentsController');
Route::post('comment/store', 'CommentController@store')->name('comment.add');
Route::post('reply/store', 'CommentController@replyStore')->name('reply.add');

//front search
Route::post('/search', 'SearchController@index')->name('search');
Route::get('/search', 'SearchController@search')->name('search');

//slug route

Route::get('/{slug}/{uuid}', ['as' => 'front.memes.meme_details', 'uses' => 'MemeController@memeDetail'])->where('slug',
    '[\w\d\-\_]+');

//front category routes
Route::get('/{categorySlug}', ['as' => 'front.categories.category_details', 'uses' => 'CategoriesController@getCategoryDetail']);
