@extends('front.main')
@section('content')
    <h5 class="clr-wht">Home</h5>
    <!-- <p class="lead font-weight-light navbar-color font-12">Your personalized feed according to your browsing history</p> -->
    <hr class="hr-bg">
<?php
use Carbon\Carbon;
$CurrentMonth = date('m');
$memes = \App\Meme::latest()->whereDate('created_at', '>', Carbon::now()->subDays(30))->get();
?>
    @include('front.memes.master-meme',['memes' => $memes])
@endsection

