<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>lolwhoa</title>
    <link rel="stylesheet" href="{{asset('assets/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="{{asset('assets/css/login.css')}}">
</head>
<body style="background: #002932">
<div class="login-form">
    <form action="{{url ('signup') }}" method="post">
        {{ csrf_field() }}
        <h2 class="text-center">Sign up</h2>
        <div class="text-center social-btn">
            <a href="#" class="btn btn-primary btn-block"><i class="fa fa-facebook"></i> Sign
                in with
                <b>Facebook</b></a>
            <a href="#" class="btn btn-info btn-block"><i class="fa fa-twitter"></i> Sign in
                with <b>Twitter</b></a>
            <a href="{{route('auth','google')}}" class="btn btn-danger btn-block"><i class="fa fa-google"></i> Sign in
                with <b>Google</b></a>
            <a href="{{route('auth','linkedin')}}" class="btn btn-primary btn-block" style="background: #0077B5"><i
                    class="fa fa-linkedin"></i> Sign
                up with
                <b>Linkedin</b></a>
        </div>
        <div class="or-seperator"><i>or</i></div>
        <div class="form-group">
            <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-user"></i></span>
                <input type="text" class="form-control" name="first_name" placeholder="First Name" required="required">
            </div>
        </div>
        <div class="form-group">
            <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-user"></i></span>
                <input type="text" class="form-control" name="last_name" placeholder="Last Name">
            </div>
        </div>
        <div class="form-group">
            <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-envelope icon"></i></span>
                <input type="email" class="form-control @error('email') is-invalid @enderror" value="{{ old('email')
                }}" name="email"
                       placeholder="Email" required="required">
            </div>
            @error('email')
            <span class="invalid-feedback" role="alert">
                <strong style="color: red">{{ $message }}</strong>
            </span>
            @enderror
        </div>
        <div class="form-group">
            <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-lock"></i></span>
                <input type="password" class="form-control" name="password" placeholder="Password" required="required">
            </div>
        </div>
        <div class="clearfix">
            <label class="pull-left checkbox-inline"><input type="checkbox"> Remember me</label>
        </div>
        <div class="form-group margin-top-10">
            <input type="submit" value="Sign Up" class="btn btn-success btn-block login-btn"/>
        </div>
        <div class="hint-text small">Already have an account? <a href="{{route ('login')}}" class="text-success">Login
                Now!</a></div>
    </form>

</div>

</body>
</html>
