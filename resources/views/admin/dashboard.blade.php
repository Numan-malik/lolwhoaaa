@extends('admin.master')
@section('top-title','| Dashbaord')
@section('breadcome-title','Dashbaord')
@section('content')
    <div class="analytics-sparkle-area">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
                    <div class="analytics-sparkle-line reso-mg-b-30 table-mg-t-pro dk-res-t-pro-30">
                        <div class="analytics-content">
                            <h5>Users</h5>
                            <h2><span class="counter">{{@$registerUser}}</span> <span class="tuition-fees"></span></h2>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
                    <div class="analytics-sparkle-line reso-mg-b-30">
                        <div class="analytics-content">
                            <h5>Memes</h5>
                            <h2><span class="counter">{{@$totalMemes}}</span></h2>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
                    <div class="analytics-sparkle-line reso-mg-b-30">
                        <div class="analytics-content">
                            <h5>Total Views</h5>
                            <h2><span class="counter">{{@$totalView[0]->total_views}}</span></h2>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
                    <div class="analytics-sparkle-line table-mg-t-pro dk-res-t-pro-30">
                        <div class="analytics-content">
                            <h5>Categories</h5>
                            <h2><span class="counter">{{@$categories}}</span></h2>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    {{--<div class="traffic-analysis-area" style="margin-top: 30px">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
                    <div class="social-media-edu">
                        <i class="fa fa-facebook"></i>
                        <div class="social-edu-ctn">
                            <h3>50k Likes</h3>
                            <p>You main list growing</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
                    <div class="social-media-edu twitter-cl res-mg-t-30 table-mg-t-pro-n">
                        <i class="fa fa-twitter"></i>
                        <div class="social-edu-ctn">
                            <h3>30k followers</h3>
                            <p>You main list growing</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
                    <div class="social-media-edu linkedin-cl res-mg-t-30 res-tablet-mg-t-30 dk-res-t-pro-30">
                        <i class="fa fa-linkedin"></i>
                        <div class="social-edu-ctn">
                            <h3>7k Connections</h3>
                            <p>You main list growing</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
                    <div class="social-media-edu youtube-cl res-mg-t-30 res-tablet-mg-t-30 dk-res-t-pro-30">
                        <i class="fa fa-youtube"></i>
                        <div class="social-edu-ctn">
                            <h3>50k Subscribers</h3>
                            <p>You main list growing</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>--}}
    <div class="library-book-area mg-t-30">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
                    <div class="single-cards-item">
                        <div class="single-product-image">
                            <a href="#"><img src="{{asset('admin/img/product/profile-bg.jpg')}}" alt=""></a>
                        </div>
                        <div class="single-product-text">
                            <img src="{{asset('images/random_profile_pic.jpg')}}" alt="">
                            <h4><a class="cards-hd-dn">{{Auth::user()->name}}</a></h4>
                            <h5>Web Designer & Developer</h5>
                            <a class="follow-cards" href="{{route('adminProfile')}}">Edit Profile</a>
                            <div class="row">
                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                    <div class="cards-dtn">
                                        <h3><span class="counter">{{@$totalMemes}}</span></h3>
                                        <p>Memes</p>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                    <div class="cards-dtn">
                                        <h3><span class="counter">{{@$totalLikes}}</span></h3>
                                        <p>Like</p>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                    <div class="cards-dtn">
                                        <h3><span class="counter">{{@$totalComments}}</span></h3>
                                        <p>Comment</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
                    <div class="single-review-st-item res-mg-t-30 table-mg-t-pro-n">
                        <div class="single-review-st-hd">
                            <h2>New Users</h2>
                        </div>
                        @foreach($NewUsers as $user)
                            <div class="single-review-st-text">
                                @if(@$user->avatar !== null)
                                    <img src="{{asset('uploads/users/'.@$user->avatar)}}" alt="N/A">
                                @else
                                    <img src="{{asset('images/random_profile_pic.jpg')}}" alt="N/A">
                                @endif
                                <div class="review-ctn-hf">
                                    <a href="{{ url('u'.'/'.$user->name.'/'.$user->id) }}" target="_blank"><h3>{{@$user->name}}</h3></a>
                                    <p>{{@$user->bio ? @$user->bio : ''}}</p>
                                </div>
                                <div class="review-item-rating">
                                    <a>{{@$user->updated_at->diffForHumans(null,false) }}</a>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                    <div class="single-product-item res-mg-t-30 table-mg-t-pro-n tb-sm-res-d-n dk-res-t-d-n">
                        <div class="single-product-image">
                            <a href="{{ url (''.@$MemehasMaxViews[0]->slug ) }}" target="_blank">
                                @if(@$MemehasMaxViews[0]->file_description == 'upload from pc')
                                    <img src="{{asset('uploads/memes/'.@$MemehasMaxViews[0]->file)}}" alt="image"
                                         onerror="this.onerror=null;this.src='{{ asset ('images/notfound.jpg')}}';">
                                @elseif(@$MemehasMaxViews[0]->file_description == 'from URL')
                                    <img src="{{ url(@$MemehasMaxViews[0]->file )}}" alt="image"
                                         onerror="this.onerror=null;this.src='{{ asset ('images/notfound.jpg')}}';">
                                @endif
                            </a>
                        </div>
                        <div class="single-product-text edu-pro-tx">
                            <h4><a href="{{ url (''.@$MemehasMaxViews[0]->slug ) }}"
                                   target="_blank">{{@$MemehasMaxViews[0]->title}}</a></h4>
                            <h5>{{@$MemehasMaxViews[0]->slug}}</h5>
                            {{-- <small>
                                 <i class="fa fa-tag"></i>
                                 @foreach($MemehasMaxViews[0]->categories as $c)
                                     <div>
                                         <a class="navbar-color">{{@lcfirst($c->category->name)}} @if (!$loop->last),@endif</a>
                                     </div>
                                 @endforeach
                             </small>--}}

                            <div class="product-price">
                                <i class="fa fa-eye"></i>
                                <h3>{{@$MemehasMaxViews[0]->view_count}}</h3>
                                <div class="single-item-rating">
                                    <i class="educate-icon educate-star"></i>
                                    <i class="educate-icon educate-star"></i>
                                    <i class="educate-icon educate-star"></i>
                                    <i class="educate-icon educate-star"></i>
                                    <i class="educate-icon educate-star-half"></i>
                                </div>
                            </div>
                            <div class="product-buttons">
                                <button type="button" class="button-default cart-btn">Read More</button>
                                <button type="button" class="button-default"><i class="fa fa-heart"></i></button>
                                <button type="button" class="button-default"><i class="fa fa-share"></i></button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="footer-copyright-area" style="margin-top: 30px">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="footer-copy-right">
                        <p>Copyright © 2019. All rights reserved. <a href="https:veteranlogix.com/" target="_blank">Veteranlogix</a>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
