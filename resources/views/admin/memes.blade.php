@extends('admin.master')
@section('top-title','| Memes')
@section('breadcome-title','Memes')
@section('css')
    <link rel="stylesheet" href="{{asset('assets/css/chosen.css')}}">
@endsection
@section('content')
    <div class="single-pro-review-area mt-t-30 mg-b-15">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="product-payment-inner-st">
                        <ul id="myTabedu1" class="tab-review-design">
                            <li class="active">
                                <a href="#description">@if(isset($meme)) Edit @else Add @endif Meme</a>
                            </li>
                        </ul>
                        <div id="myTabContent" class="tab-content custom-product-edit">
                            <div class="product-tab-list tab-pane fade active in" id="description">
                                <div class="row">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <div class="review-content-section">
                                            <div id="dropzone1" class="pro-ad addcoursepro">
                                                <form method="post"
                                                      @if(isset($meme)) action="{{route('UpdateMeme',$meme['id'])}}"
                                                      @else action="{{route('create-memes')}}" @endif
                                                      enctype="multipart/form-data">
                                                    @csrf
                                                    @if(isset($meme))
                                                        <div class="form-group col-md-2">
                                                        @if($meme['file_description'] == 'upload from pc')
                                                            <img id="myImg" src="{{asset('storage/uploads/thumbnails/'.$meme['file']) }}"
                                                                 alt="" width="130px" style="margin-top: 20px"/>
                                                        @elseif($meme['file_description'] == 'from URL')
                                                            <img id="ImgURL" src="{{$meme['file']}}" width="130px" style="margin-top: 20px"/>
                                                        @endif
                                                        </div>
                                                    @else
                                                        <div class="form-group col-md-2">
                                                            <label for="avatar">Avatar</label>
                                                            <input type="file" class="memeImage" name="img" id="file">
                                                            <span class="text-danger">{{ $errors->first('img') }}</span>
                                                            <img id="myImg" src="" alt="" width="130px" style="margin-top: 20px"/>
                                                            <p id="img-err" style="color: red"></p>
                                                        </div>
                                                        <div class="form-group col-md-4" id="image-url">
                                                            <label for="avatar">Or</label>
                                                            <input type="text" name="imgUrl" class="form-control"
                                                                   id="imglink" pattern="https?://.+" placeholder="https://">
                                                            <img id="ImgURL" src="" alt="" class="img-fluid"
                                                                 style="margin-top: 20px"/>
                                                            <p id="img-err" style="color: red"></p>
                                                        </div>
                                                    @endif

                                                    <div class="form-group col-md-2">
                                                        <label for="formGroupExampleInput">Title</label>
                                                        <input type="text" name="title" class="form-control"
                                                               id="formGroupExampleInput"
                                                               placeholder="Please enter name" @if(isset($meme))
                                                               value="{{$meme['title']}}" @endif required>
                                                    </div>
                                                    <div class="form-group col-md-2">
                                                        <label for="formGroupExampleInput">Tags</label>
                                                        <input id="tags" class="form-control" data-role="tagsinput"
                                                               type="text" name="tags" @if(isset($meme))
                                                               value="{{$meme['tags']}}" @endif
                                                               placeholder="tags 1 , tags 2 , tags 3" required>
                                                    </div>

                                                    <div class="form-group col-md-2">
                                                        <label for="multiple" class="control-label">Select
                                                            Category</label>
                                                        <select name="category" data-placeholder="Choose a Category..."
                                                                class="col-md-6 form-control">
                                                            <option value=""></option>
                                                            @foreach($categories as $c)
                                                                <option @if(isset($meme))
                                                                        value="{{$meme['category_id']}}"
                                                                        {{'selected'}}@else value="{{$c->id}}"
                                                                    @endif>{{$c->name}}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                    <div class="form-group col-md-12">
                                                        <button type="submit" id="send_form" class="btn btn-success">
                                                            @if(isset($meme))
                                                                Update
                                                            @else
                                                                Submit
                                                            @endif
                                                        </button>
                                                    </div>
                                                    @if(Session::has('message'))
                                                        <div class="form-group col-md-4">
                                                            <div class="alert alert-success alert-dismissible fade in"
                                                                 id="msg">
                                                                <a href="#" class="close" data-dismiss="alert"
                                                                   aria-label="close">&times;</a>
                                                                <strong>Success!</strong> {!! session('message') !!}
                                                            </div>
                                                        </div>
                                                    @endif
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="data-table-area mg-b-15">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="sparkline13-list">
                        <div class="sparkline13-hd">
                            <div class="main-sparkline13-hd">
                                <h1>All <span class="table-project-n">Memes</span></h1>
                            </div>
                        </div>
                        <div class="sparkline13-graph">
                            <div class="datatable-dashv1-list custom-datatable-overright">
                                <table id="table" data-toggle="table" data-pagination="true" data-search="true"
                                       data-show-columns="true" data-show-pagination-switch="true"
                                       data-show-refresh="true" data-key-events="true" data-show-toggle="true"
                                       data-resizable="true" data-cookie="true"
                                       data-cookie-id-table="saveId" data-show-export="true" data-click-to-select="true"
                                       data-toolbar="#toolbar">
                                    <thead>
                                    <tr>
                                        <th data-field="id">#</th>
                                        <th data-field="title">Title</th>
                                        <th data-field="slug">Slug</th>
                                        <th data-field="category">Tags</th>
                                        <th data-field="file">File Source</th>
                                        <th data-field="image">Image</th>
                                        <th data-field="created">Created By</th>
                                        <th data-field="view">Views</th>
                                        <th data-field="action">Actions</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($memes as $m)
                                        <tr>
                                            <td>{{$loop->iteration}}</td>
                                            <td>{{$m->title}}</td>
                                            <td><a href="{{url($m->slug.'/'.$m->uuid)}}" target="_blank">{{$m->slug}}</a></td>
                                            <td>{{$m->tags}}</td>
                                            <td>{{$m->file_description}}</td>
                                            <td>
                                                @if($m->file_description == 'upload from pc')
                                                    <img src="{{asset('storage/uploads/thumbnails/'.$m->file) }}"
                                                         width="80px" align="N/A" 
                                                         onerror="this.onerror=null;this.src='{{ asset('images/notfound.jpg')}}';">
                                                @else
                                                    <img src="{{url ($m->file) }}" width="80px" align="N/A"
                                                         onerror="this.onerror=null;this.src='{{ asset('images/notfound.jpg')}}';">
                                                @endif
                                            </td>
                                            <td>{{$m->user->name }}</td>
                                            <td>{{$m->view_count }}</td>
                                            <td>
                                                <a type="button" class="btn btn-info"
                                                   href="{{route('EditMeme',$m->id)}}" style="color: white">Edit</a>
                                                <a type="button" class="btn btn-danger"
                                                   href="{{route('delete_memes',$m->id)}}"
                                                   onclick="return confirm('Are you sure?')" style="color: white">Delete
                                                </a>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('javascript')
    <script src="{{asset('assets/js/jquery-3.2.1.min.js')}}"
            type="text/javascript"></script>
    <script src="{{asset('assets/js/chosen.jquery.js')}}"
            type="text/javascript"></script>
    <script src="{{asset('assets/js/init.js')}}" type="text/javascript"
            charset="utf-8"></script>
    <script>

    </script>
@endsection
