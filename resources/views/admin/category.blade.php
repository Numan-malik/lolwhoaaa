@extends('admin.master')
@section('top-title','| Category')
@section('breadcome-title','Category')
@section('content')
    <div class="single-pro-review-area mt-t-30 mg-b-15">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="product-payment-inner-st">
                        <ul id="myTabedu1" class="tab-review-design">
                            <li class="active"><a href="#description">Add Category</a></li>
                        </ul>
                        <div id="myTabContent" class="tab-content custom-product-edit">
                            <div class="product-tab-list tab-pane fade active in" id="description">
                                <div class="row">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <div class="review-content-section">
                                            <div id="dropzone1" class="pro-ad addcoursepro">
                                                <form action="{{route('create-category')}}" method="post" enctype="multipart/form-data">
                                                    {{ csrf_field() }}
                                                    <div class="row">
                                                        <div class="col-lg-4 col-md-6 col-sm-6 col-xs-6">
                                                            <div class="form-group">
                                                                <input name="name" type="text" class="form-control"
                                                                       placeholder="Name of Category" required>
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="avatar">Image</label>
                                                                <input type="file" name="img">
                                                            </div>
                                                            <button type="submit" class="btn btn-primary
                                                                waves-effect waves-light">Save
                                                            </button>
                                                        </div>
                                                        <div class="col-lg-4 col-md-6 col-sm-6"></div>
                                                        @if(Session::has('msg'))
                                                            <div class="col-lg-4 col-md-6 col-sm-6">
                                                                <div class="alert alert-success alert-success-style1 alert-success-stylenone">
                                                                    <button type="button" class="close sucess-op"
                                                                            data-dismiss="alert" aria-label="Close">
                                                                        <span class="icon-sc-cl"
                                                                              aria-hidden="true">×</span>
                                                                    </button>
                                                                    <i class="fa fa-check edu-checked-pro admin-check-sucess admin-check-pro-none"
                                                                       aria-hidden="true"></i>
                                                                    <p class="message-alert-none">
                                                                        <strong>Saved!</strong>
                                                                        {!! session ('msg') !!}</p>
                                                                </div>
                                                            </div>
                                                        @endif
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="sparkline10-list mt-b-30">
            <div class="sparkline10-hd">
                <div class="main-sparkline10-hd">
                    <h1>Categories</h1>
                </div>
            </div>
            <div class="sparkline10-graph">
                <div class="static-table-list">
                    <table class="table border-table">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Name</th>
                            <th>Image</th>
                            <th>Created by</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($categories as $cat)
                            <tr id="tr_hide">
                                <td>{{$loop->iteration}}</td>
                                <td>{{$cat->name}}</td>
                                <td><img src="{{asset('storage/uploads/categoryImage/'.$cat->img) }}" width="30px"
                                         align="N/A"></td>
                                <td>{{@$cat->user->name}}</td>
                                <td>
                                    <a type="button" class="btn btn-info" data-toggle="modal"
                                       data-target="#edit_{{$cat->id}}">Edit</a>
                                    <a type="button" class="btn btn-danger" href="{{route('delete_category',$cat->id)}}"
                                       onclick="return confirm('Are you sure?')">Delete
                                    </a>
                                </td>
                            </tr>

                            <div id="edit_{{$cat->id}}" class="modal modal-edu-general modal-zoomInDown fade"
                                 role="dialog">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-body">
                                            <div class="modal-login-form-inner">
                                                <div class="row">
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <div class="basic-login-inner modal-basic-inner">
                                                            <h3>Edit</h3>
                                                            <form action="{{route('update_category',$cat->id)}}" method="post" enctype="multipart/form-data">
                                                                {{ csrf_field() }}
                                                                <div class="form-group-inner">
                                                                    <div class="row">
                                                                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                                                            <label>Category Name</label>
                                                                        </div>
                                                                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                                                                            <input type="text" class="form-control" name="name" placeholder="Name of Category"
                                                                                   value="{{$cat->name}}" required>
                                                                        </div>
                                                                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                                                                            <label for="avatar">Image</label>
                                                                            <input type="file" name="img" value="{{$cat->img}}">
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="login-btn-inner">
                                                                    <div class="row">
                                                                        <div
                                                                            class="col-lg-4 col-md-4 col-sm-4 col-xs-12"></div>
                                                                        <div
                                                                            class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                                                                            <div class="login-horizental">
                                                                                <button
                                                                                    class="btn btn-sm btn-primary"
                                                                                    type="submit">Update
                                                                                </button>
                                                                                <a class="btn btn-sm btn-info"
                                                                                   type="button"
                                                                                   data-dismiss="modal"
                                                                                   href="#">Cancel</a>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </form>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

@endsection

