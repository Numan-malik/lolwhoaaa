@extends('admin.master')
@section('top-title','| Users')
@section('breadcome-title','Users')
@section('content')
    <style>
        .badge-fb
        {
            background: #3b5998;
        }
        .badge-twitter
        {
            background: #38A1F3;
        }
        .badge-gmail
        {
            background: #d34836;
        }
        .badge-linkedin
        {
            background: #0e76a8;
        }
        .badge-web
        {
            background: #1caf9a;
        }
        .clr-white{
            color: white !important;
        }
    </style>
    <div class="data-table-area mg-b-15">

        <div class="container-fluid">
            <div class="row">
                 @if(Session::has('msg'))
                <div class="col-lg-4 col-md-6 col-sm-6">
                    <div class="alert alert-success alert-success-style1 alert-success-stylenone">
                        <button type="button" class="close sucess-op" data-dismiss="alert" aria-label="Close">
                            <span class="icon-sc-cl" aria-hidden="true">×</span>
                        </button>
                        <i class="fa fa-check edu-checked-pro admin-check-sucess admin-check-pro-none" aria-hidden="true"></i>
                        <p class="message-alert-none">
                            <strong>Saved!</strong>
                            {!! session ('msg') !!}</p>
                    </div>
                </div>
                 @endif
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="sparkline13-list">
                        <div class="sparkline13-hd">
                            <div class="main-sparkline13-hd">
                                <h1>All <span class="table-project-n">Users</span></h1>
                            </div>
                        </div>

                        <div class="sparkline13-graph">
                            <div class="datatable-dashv1-list custom-datatable-overright">
                                <table id="table" data-toggle="table" data-pagination="true" data-search="true"
                                       data-show-columns="true" data-show-pagination-switch="true"
                                       data-show-refresh="true" data-key-events="true" data-show-toggle="true"
                                       data-resizable="true" data-cookie="true"
                                       data-cookie-id-table="saveId" data-show-export="true" data-click-to-select="true"
                                       data-toolbar="#toolbar">
                                    <thead>
                                    <tr>
                                        <th data-field="no">No#</th>
                                        <th data-field="name">Name</th>
                                        <th data-field="email">Email</th>
                                        <th data-field="phone">Profile</th>
                                        <th data-field="complete">Register From</th>
                                        <th data-field="task">Gender</th>
                                        <th data-field="date">Country</th>
                                        <th data-field="action">Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($users as $user)
                                        <tr>
                                            <td>{{$loop->iteration}}</td>
                                            <td>{{$user->name}}</td>
                                            <td>{{$user->email}}</td>
                                            <td><img src="{{asset('uploads/users/'.$user->avatar) }}" alt="image"
                                                     style="border-radius: 15%;width: 50px"></td>
                                            <td>
                                                @foreach($user->RegisterFrom as $u)
                                                    <a class="clr-white badge @if($u->provider == 'facebook')
                                                        badge-fb @elseif($u->provider == 'twitter')
                                                        badge-twitter @elseif($u->provider == 'google') badge-gmail @elseif($u->provider== 'linkedin') badge-linkedin @endif">
                                                        {{$u->provider}}
                                                    </a>
                                                @endforeach
                                                @if($user->password !== null)
                                                    <a class="clr-white badge badge-web">Web</a>
                                                @endif
                                               {{-- @foreach($user->RegisterFrom as $u)
                                                    <a>{{$u->provider}} @if (!$loop->last),@endif</a>
                                                @endforeach
                                                @if($user->password !== null)
                                                    <a>Web</a>
                                                @endif--}}
                                            </td>
                                            <td>{{$user->gender ? $user->gender : 'N/A'}}</td>
                                            <td>{{$user->country ? $user->country : 'N/A'}}</td>
                                            <td>
                                                <a type="button" class="btn btn-danger" href="{{route('delete_user',
                                                $user->id)}}"
                                                   onclick="return confirm('Are you sure?')" style="color: white">Delete
                                                </a>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
