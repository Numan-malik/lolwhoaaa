@extends('admin.master')
@section('top-title','| Profile')
@section('breadcome-title','Profile')
@section('content')

    <?php $user = Auth::user(); ?>
    <!-- Single pro tab review Start-->
    <div class="single-pro-review-area mt-t-30 mg-b-15">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="product-payment-inner-st">
                        <ul id="myTabedu1" class="tab-review-design">
                            <li class="active"><a href="#setting">Settings</a></li>
                            <li><a href="#profile">Profile</a></li>
                            <li><a href="#password">Change Password</a></li>
                        </ul>
                        @if(Session::has('success-msg'))
                            <div class="col-lg-4 col-md-6 col-sm-6">
                                <div class="alert alert-success alert-success-style1 alert-success-stylenone">
                                    <button type="button" class="close sucess-op" data-dismiss="alert" aria-label="Close">
                                        <span class="icon-sc-cl" aria-hidden="true">×</span>
                                    </button>
                                    <p class="message-alert-none">
                                        <strong> {!! session ('success-msg') !!}</strong>
                                    </p>
                                </div>
                            </div>
                        @endif
                         @if(Session::has('error-msg'))
                            <div class="col-lg-4 col-md-6 col-sm-6">
                                <div class="alert alert-danger alert-danger-style1 alert-success-stylenone">
                                    <button type="button" class="close sucess-op" data-dismiss="alert" aria-label="Close">
                                        <span class="icon-sc-cl" aria-hidden="true">×</span>
                                    </button>
                                    <p class="message-alert-none">
                                        <strong> {!! session ('error-msg') !!}</strong>
                                    </p>
                                </div>
                            </div>
                        @endif
                        <div id="myTabContent" class="tab-content custom-product-edit">
                            <div class="product-tab-list tab-pane fade active in" id="setting">
                                <div class="row">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <div class="review-content-section">
                                            <div id="dropzone1" class="pro-ad">
                                                <form action="{{route('update-admin-setting',$user->id)}}" class="dropzone dropzone-custom needsclick add-professors" method="post">
                                                    @csrf
                                                    <div class="row">
                                                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                                            <div class="form-group">
                                                                <label>Name</label>
                                                                <input type="text" class="form-control" placeholder="Enter your name" value="{{$user->name}}" name="name" maxlength="30">
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                                            <div class="form-group">
                                                                <label>Email</label>
                                                                <input type="email" class="form-control" placeholder="Enter your email" value="{{$user->email}}" name="email">
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                                            <div class="form-group">
                                                                <button type="submit" class="btn btn-primary waves-effect waves-light">
                                                                    Save
                                                                </button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="product-tab-list tab-pane fade" id="profile">
                                <div class="row">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <div class="review-content-section">
                                            <form action="{{route('update-admin-profile',$user->id)}}"
                                                  class="dropzone dropzone-custom needsclick add-professors" id="demo1-upload" method="post" enctype="multipart/form-data">
                                                @csrf
                                                <div class="row">

                                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                                        <div class="form-group">
                                                            <label>Full Name</label>
                                                            <input type="text" class="form-control" placeholder="Enter your full name" value="{{$user->full_name}}" name="full_name" maxlength="30">
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                                        <div class="form-group">
                                                            <label>Gender</label>
                                                            <select class="form-control" name="gender">
                                                                @if($user->gender != null)
                                                                    <option
                                                                        value="male" @if($user->gender == 'male') {{'selected'}} @endif>
                                                                        Male
                                                                    </option>
                                                                    <option
                                                                        value="female" @if($user->gender == 'female') {{'selected'}} @endif>
                                                                        Female
                                                                    </option>
                                                                @else
                                                                    <option value="male">Male</option>
                                                                    <option value="female">Female</option>
                                                                @endif
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                                        <div class="form-group">
                                                            <label>Birthday</label>
                                                            <input type="date" class="form-control"
                                                                   name="birthday"
                                                                   @if($user->birthday !== '')
                                                                   value="{{ $user->birthday }}" @endif>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                                        <div class="form-group">
                                                            <label>Country</label>
                                                            <select class="form-control" name="country">
                                                                @if($user->country != null)
                                                                    <option
                                                                        value="pakistan" @if($user->country == 'pakistan') {{'selected'}} @endif>
                                                                        Pakistan
                                                                    </option>
                                                                    <option
                                                                        value="UAE" @if($user->country == 'UAE') {{'selected'}} @endif>
                                                                        UAE
                                                                    </option>
                                                                @else
                                                                    <option value="pakistan">Pakistan</option>
                                                                    <option value="UAE">UAE</option>
                                                                @endif
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                                        <div class="form-group edit-ta-resize res-mg-t-15">
                                                            <label>Bio</label>
                                                            <textarea name="bio" class="form-control" placeholder="BIO"
                                                            >{{$user->bio}}</textarea>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                                        <div class="form-group">
                                                            <label>Profile Image</label>
                                                            <input type="file" class="img-fluid" name="avatar"/>
                                                            @if($user->avatar == null)
                                                                <img  src="" alt=""
                                                                     class="mx-w-25 m-t-10" width="50px"/>
                                                            @else
                                                                <img src="{{asset('uploads/users/'.$user->avatar)}}" width="50px"/>
                                                            @endif
                                                        </div>
                                                    </div>

                                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                                        <div class="form-group">
                                                            <button type="submit"
                                                                    class="btn btn-primary waves-effect waves-light">
                                                                Save
                                                            </button>
                                                        </div>
                                                    </div>
                                                </div>

                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="product-tab-list tab-pane fade" id="password">
                                <div class="row">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <div class="review-content-section">
                                            <form action="{{route('update-admin-password',$user->id)}}" method="post" class="dropzone dropzone-custom needsclick add-professors" id="demo1-upload">
                                                @csrf
                                                <div class="row">

                                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                                        <div class="form-group">
                                                            <label>Old Password</label>
                                                            <input type="password" class="form-control" value=""
                                                                   name="old_password" minlength="8" maxlength="20" required>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                                        <div class="form-group">
                                                            <label>New Password</label>
                                                            <input type="password" class="form-control"
                                                                   name="password" minlength="8" maxlength="20"
                                                                   required/>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                                        <div class="form-group">
                                                            <label>Confirm New Password</label>
                                                            <input type="password" class="form-control"
                                                                   name="password_confirmation"
                                                                   autocomplete="password" minlength="8"
                                                                   maxlength="20" required />
                                                            <span
                                                                class="text-danger">{{ $errors->first('password') }}</span>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <div class="form-group">
                                                            <button type="submit"
                                                                    class="btn btn-primary waves-effect waves-light">
                                                                Save
                                                            </button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
