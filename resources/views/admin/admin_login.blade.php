<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>LOLWHOA | Admin</title>
    <link rel="stylesheet" href="{{asset('assets/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="{{asset('assets/css/login.css')}}">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
</head>
<body style="background: #414141">

<div class="login-form">
    <div>
       {{-- <img src="{{asset('images/lolwhoa.png')}}" style="margin-left: 110px;width:30%">--}}
    </div>

    <form action="{{route ('AdminLogin') }}" method="post" style="background: #545454">
        <div class="form-group" style="text-align: center">
            <img src="{{asset('images/lol-whoa-logo-done.png')}}" width="100px">
        </div>

        {{ csrf_field() }}
        @if(Session::has('login-error'))
            <div class="alert alert-danger alert-block">
                <button type="button" class="close" data-dismiss="alert">×</button>
                <strong>{!! Session('login-error') !!}</strong>
            </div>
        @endif
        @if(Session::has('logout-success'))
            <div class="alert alert-success alert-block">
                <button type="button" class="close" data-dismiss="alert">×</button>
                <strong>{!! Session('logout-success') !!}</strong>
            </div>
        @endif
        {{--<h2 class="text-center">Sign in</h2>

         <div class="text-center social-btn">
             <a href="#" class="btn btn-primary btn-block"><i class="fa fa-facebook"></i> Sign
                 in with
                 <b>Facebook</b></a>
             <a href="#" class="btn btn-info btn-block"><i class="fa fa-twitter"></i> Sign in
                 with <b>Twitter</b></a>
             <a href="{{route('auth','google')}}" class="btn btn-danger btn-block"><i class="fa fa-google"></i> Sign in
                 with <b>Google</b></a>
             <a href="{{route('auth','linkedin')}}" class="btn btn-primary btn-block" style="background: #0077B5"><i
                     class="fa fa-linkedin"></i> Sign
                 up with
                 <b>Linkedin</b></a>
         </div>
        <div class="or-seperator"><i>or</i></div>--}}
        <div class="form-group">
            <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-user"></i></span>
                <input type="email" class="form-control" name="email" placeholder="Email" required="required">
            </div>
        </div>
        <div class="form-group">
            <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-lock"></i></span>
                <input type="password" class="form-control" name="password" placeholder="Password" required="required">
            </div>
        </div>
        <div class="clearfix">
            <label class="pull-left checkbox-inline" style="color: white"><input type="checkbox"> Remember me</label>
        </div>
        <div class="form-group margin-top-10">
            <input type="submit" value="Sign in" class="btn btn-success btn-block login-btn" style=""/>
        </div>
        <div class="form-group margin-top-10" style="text-align: center;color: white">
           <small><a href="https://www.veteranlogix.com" style="color: white">Veteranlogix</a> © 2019. All rights reserved.</small>
        </div>
        {{--<div class="hint-text small">Don't have an account? <a href="#"
                                                               class="text-success">Register Now!</a></div>--}}
    </form>

</div>
</body>
</html>
