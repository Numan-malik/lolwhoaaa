<!doctype html>
<html class="no-js" lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>LOLWHOA @yield('top-title','| Admin')</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!-- Google Fonts
		============================================ -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,700,900" rel="stylesheet">
    <!-- Bootstrap CSS
		============================================ -->
    <link rel="stylesheet" href="{{asset('admin/css/bootstrap.min.css')}}">
    <!-- Bootstrap CSS
		============================================ -->
    <link rel="stylesheet" href="{{asset('admin/css/font-awesome.min.css')}}">

    <!-- animate CSS
		============================================ -->
    <link rel="stylesheet" href="{{asset('admin/css/animate.css')}}">
    <!-- normalize CSS
		============================================ -->
    <link rel="stylesheet" href="{{asset('admin/css/normalize.css')}}">
    <!-- meanmenu icon CSS
		============================================ -->
    <link rel="stylesheet" href="{{asset('admin/css/meanmenu.min.css')}}">
    <!-- main CSS
		============================================ -->
    <link rel="stylesheet" href="{{asset('admin/css/main.css')}}">
    <!-- metisMenu CSS
		============================================ -->
    <link rel="stylesheet" href="{{asset('admin/css/metisMenu/metisMenu.min.css')}}">
    <link rel="stylesheet" href="{{asset('admin/css/metisMenu/metisMenu-vertical.css')}}">

    <link rel="stylesheet" href="{{asset('admin/css/data-table/bootstrap-table.css')}}">
    <link rel="stylesheet" href="{{asset('admin/css/data-table/bootstrap-editable.css')}}">

    <!-- style CSS
		============================================ -->
    <link rel="stylesheet" href="{{asset('admin/css/style.css')}}">
    <!-- responsive CSS
		============================================ -->
    <link rel="stylesheet" href="{{asset('admin/css/responsive.css')}}">
    <link rel="stylesheet" href="{{asset('css/bootstrap-tagsinput.css')}}"/>

@yield('css')
<!-- modernizr JS
		============================================ -->
    <script src="{{asset('admin/js/vendor/modernizr-2.8.3.min.js')}}"></script>
</head>

<body>

<!-- Start Left menu area -->
<div class="left-sidebar-pro">
    <nav id="sidebar" class="">
        <div class="sidebar-header" style="background: #414141;height: 100px">
            <a href="{{route('dashboard')}}">
                <img src="{{asset('images/lol-whoa-logo-done.png')}}" width="100px" style="margin-top: 10px">
            </a>
        </div>
        <div class="left-custom-menu-adp-wrap comment-scrollbar">
            <nav class="sidebar-nav left-sidebar-menu-pro">
                <ul class="metismenu" id="menu1">
                    <li class="active">
                        <a href="{{route('dashboard')}}">
                            <i class="fa fa-dashboard"></i>
                            <span class="mini-click-non">Dashboard</span>
                        </a>
                    </li>
                    <li>
                        <a href="{{route('user')}}">
                            <i class="fa fa-user"></i>
                            <span class="mini-click-non"> Users</span>
                        </a>
                    </li>
                    <li>
                        <a href="{{route('category')}}">
                            <i class="fa fa-list-alt"></i>
                            <span class="mini-click-non"> Category</span>
                        </a>
                    </li>
                    <li>
                        <a href="{{route('all_memes')}}">
                            <i class="fa fa-picture-o" aria-hidden="true"></i>
                            <span class="mini-click-non"> Memes</span>
                        </a>
                    </li>
                    {{-- <li>
                         <a class="has-arrow" href="index.html">
                             <span class="educate-icon educate-home icon-wrap"></span>
                             <span class="mini-click-non">Dropdown</span>
                         </a>
                         <ul class="submenu-angle" aria-expanded="true">
                             <li><a title="Dashboard v.1" href="index.html"><span
                                         class="mini-sub-pro">Dashboard v.1</span></a></li>
                             <li><a title="Dashboard v.2" href="index-1.html"><span
                                         class="mini-sub-pro">Dashboard v.2</span></a></li>
                         </ul>
                     </li>--}}
                </ul>
            </nav>
        </div>
    </nav>
</div>
<!-- End Left menu area -->
<!-- Start Welcome area -->
<div class="all-content-wrapper">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" >
                <div class="logo-pro">
                    <a href="{{route('dashboard')}}"><img class="main-logo" src="{{asset('admin/img/logo/logo.png')}}"
                                                          alt=""/></a>
                </div>
            </div>
        </div>
    </div>
    <div class="header-top-area">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="background: #414141">
                    <div class="header-top-wraper">
                        <div class="row">
                            <div class="col-lg-1 col-md-0 col-sm-1 col-xs-12">
                                <div class="menu-switcher-pro">
                                    <button type="button" id="sidebarCollapse"
                                            class="btn bar-button-pro header-drl-controller-btn btn-info navbar-btn">
                                        <i class="fa fa-angle-left pull-right-container"></i>
                                    </button>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-7 col-sm-6 col-xs-12">
                                <div class="header-top-menu tabl-d-n">
                                </div>
                            </div>
                            <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12">
                                <div class="header-right-info">
                                    <ul class="nav navbar-nav mai-top-nav header-right-menu">
                                        {{-- <li class="nav-item"><a href="#" data-toggle="dropdown" role="button"
                                                                 aria-expanded="false"
                                                                 class="nav-link dropdown-toggle"><i
                                                         class="fa fa-bell" aria-hidden="true"></i>
                                                 <span class="indicator-nt"></span></a>
                                             <div role="menu"
                                                  class="notification-author dropdown-menu animated zoomIn">
                                                 <div class="notification-single-top">
                                                     <h1>Notifications</h1>
                                                 </div>
                                                 <ul class="notification-menu">
                                                     <li>
                                                         <a href="#">
                                                             <div class="notification-icon">
                                                                 <i class="educate-icon educate-checked edu-checked-pro admin-check-pro"
                                                                    aria-hidden="true"></i>
                                                             </div>
                                                             <div class="notification-content">
                                                                 <span class="notification-date">16 Sept</span>
                                                                 <h2>Advanda Cro</h2>
                                                                 <p>Please done this project as soon possible.</p>
                                                             </div>
                                                         </a>
                                                     </li>
                                                     <li>
                                                         <a href="#">
                                                             <div class="notification-icon">
                                                                 <i class="fa fa-line-chart edu-analytics-arrow"
                                                                    aria-hidden="true"></i>
                                                             </div>
                                                             <div class="notification-content">
                                                                 <span class="notification-date">16 Sept</span>
                                                                 <h2>Victor Jara</h2>
                                                                 <p>Please done this project as soon possible.</p>
                                                             </div>
                                                         </a>
                                                     </li>
                                                 </ul>
                                                 <div class="notification-view">
                                                     <a href="#">View All Notification</a>
                                                 </div>
                                             </div>
                                         </li>--}}
                                        <li class="nav-item">
                                            <a href="#" data-toggle="dropdown" role="button" aria-expanded="false"
                                               class="nav-link dropdown-toggle">
                                                @if(Auth::user()->avatar !== null)
                                                    <img src="{{asset('uploads/users/'.Auth::user()->avatar)}}"
                                                         alt="" width="35px"/>
                                                @else
                                                    <img src="{{asset('images/random_profile_pic.jpg')}}" alt=""/>
                                                @endif
                                                <span class="admin-name">{{Auth::user()->name}}</span>
                                                <i class="fa fa-angle-down edu-icon edu-down-arrow"></i>
                                            </a>
                                            <ul role="menu"
                                                class="dropdown-header-top author-log dropdown-menu animated zoomIn">
                                                <li><a href="{{route('adminProfile')}}"><span class="edu-icon edu-user-rounded
                                                author-log-ic"></span>My Profile</a>
                                                </li>
                                                <li><a href="{{route('logoutAdmin')}}"><span
                                                            class="edu-icon edu-locked author-log-ic"></span>Log Out</a>
                                                </li>
                                            </ul>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="header-advance-area">
        <div class="breadcome-area">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="breadcome-list">
                            <div class="row">
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                    <ul class="breadcome-menu">
                                        <li><a href="{{route('dashboard')}}">Home</a> <span class="bread-slash">/</span>
                                        </li>
                                        <li><span class="bread-blod">@yield('breadcome-title')</span>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @yield('content')
</div>

@yield('javascript')


<!-- jquery
    ============================================ -->
<script src="{{asset('admin/js/vendor/jquery-1.12.4.min.js')}}"></script>
<script src="{{asset('js/custom.js') }}" defer></script>
<!-- bootstrap JS
    ============================================ -->
<script src="{{asset('admin/js/bootstrap.min.js')}}"></script>
<script src="{{asset('js/bootstrap-tagsinput.js')}}"></script>
<script>
    $(function () {
        $('#tags').tagsinput({
            maxTags: 3
        });
    });
</script>
<!-- wow JS
    ============================================ -->
<script src="{{asset('admin/js/wow.min.js')}}"></script>

<!-- meanmenu JS
    ============================================ -->
<script src="{{asset('admin/js/jquery.meanmenu.js')}}"></script>
<script src="{{asset('admin/js/data-table/bootstrap-table.js')}}"></script>
<script src="{{asset('admin/js/data-table/data-table-active.js')}}"></script>

<script src="{{asset('admin/js/jquery.sticky.js')}}"></script>

<!-- counterup JS
    ============================================ -->
<script src="{{asset('admin/js/counterup/jquery.counterup.min.js')}}"></script>
<script src="{{asset('admin/js/counterup/waypoints.min.js')}}"></script>
<script src="{{asset('admin/js/counterup/counterup-active.js')}}"></script>
<!-- metisMenu JS
    ============================================ -->
<script src="{{asset('admin/js/metisMenu/metisMenu.min.js')}}"></script>
<script src="{{asset('admin/js/metisMenu/metisMenu-active.js')}}"></script>

<script src="{{asset('admin/js/plugins.js')}}"></script>
<!-- main JS
    ============================================ -->
<script src="{{asset('admin/js/main.js')}}"></script>

</body>

</html>
