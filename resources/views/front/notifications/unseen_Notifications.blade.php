@if(@notify() !== null)
    @php
        $not=array();
       $meme_id = '';
       $action_type = '';
       $counter=0;
       $a=array();
    @endphp
    @foreach(@notify() as $n => $value)
        @if(!in_array($n,$a))
            @php
                $meme_id=$value['meme_id'];
                $action_type=$value['type'];
                $user_id=$value['user_id'];
            @endphp
            @foreach (@notify() as $key1 => $value1)

                @if($value['comment_id']!=null)
                    @if($value1['type']==$value['type'] && $value1['comment_id']==$value['comment_id'] &&
                    $value1['comment_parent_id'] == $value['comment_parent_id'] &&
                    $value1['meme_id']==$value['meme_id'])
                        @php
                            $counter++;
                            $a[] = $key1;
                        @endphp
                    @elseif ($value1['type'] == $value['type'] && $value1['comment_id'] !== $value['comment_id']
                    && $value1['comment_parent_id'] !== $value['comment_parent_id'] && $value1['meme_id'] ==
                    $value['meme_id'])
                        @php
                            $counter++;
                            $a[] = $key1;
                        @endphp

                    @endif
                @else
                    @if($value1['type']==$value['type']  && $value1['meme_id']==$value['meme_id'])
                        @php
                            $counter++;
                        $array[$key1]['status'] = 'Done';
                        $a[] = $key1;
                        @endphp

                    @endif
                @endif
            @endforeach
        @else
            @continue
        @endif
        <div class="{{$value->isRead == 1 ? 'unseen-notify' : 'seen-notify'}}">
            @if(@$value->user->avatar !== null)
                <img src="{{ url ('uploads/users/'.@$value->user->avatar) }}" class="img-notify m-l-10 m-t-5">
            @else
                <img class="img-notify m-l-10 m-t-5" src="{{ asset ('images/random_profile_pic.jpg') }}">
            @endif
            <a class="clr-wht font-12 isReadNotify" href="{{url(@$value->meme->slug.'/'.@$value->meme->uuid)}}"
               data-url="{{url('updateIsReadStatus')}}" data-attr="{{$value->type}}"
               data-meme="{{@$value->meme_id}}" data-comment="{{@$value->comment_id}}">
                @if($counter == 1)
                    <span class="actor">{{@$value->user->name}}</span>{{' '.$action_type.'.'}}
                @endif
                @if($counter >= 2)
                    @php
                        $c=$counter-1;
                    @endphp
                    <span class="actor">{{@$value->user->name}}</span>{{' & ' .$c. ' other '.' '.$action_type.'.'}}
                @endif
                @if($counter = 0)
                <span class="actor">{{@$value->user->name}}</span>{{' '.$action_type.'.'}}
                @endif
                @if(@$value->meme->file_description == 'upload from pc')
                    <img class="notify-post" src="{{ asset('storage/uploads/thumbnails/'.@$value->meme->file)}}"
                         onerror="this.onerror=null;this.src='{{ asset ('images/notfound.jpg')}}';" alt="img">
                @elseif(@$value->meme->file_description == 'from URL')
                    <img class="notify-post" src="{{ url (@$value->meme->file) }}"
                         onerror="this.onerror=null;this.src='{{ asset ('images/notfound.jpg')}}';" alt="img">
                @endif
            </a>
            <br>
            @if(@$value->created_at !== null)
                <small class="navbar-color notify-date">{{@$value->created_at->diffForHumans(null,true)}}</small>
            @endif
            <hr class="hr-bg">
        </div>
    @endforeach

@endif

