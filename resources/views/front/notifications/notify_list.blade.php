@extends('front.main')
@section('top-title','| Notifications')
@section('content')
    <div class="row m-0">
        <h3 class="clr-wht">Notifications</h3>
    </div>
    <hr class="hr-bg">
    @if(count($notifications)>0)
        @php
            $not=array();
           $meme_id = '';
           $action_type = '';
           $counter=0;
           $a=array();
        @endphp
        @foreach(@$notifications as $n => $value)
            @if(!in_array($n,$a))
                @php
                    $meme_id=$value['meme_id'];
                    $action_type=$value['type'];
                    $user_id=$value['user_id'];
                @endphp
                @foreach (@$notifications as $key1 => $value1)

                    @if($value['comment_id']!==null)
                        @if($value1['type']==$value['type'] && $value1['comment_id']==$value['comment_id'] &&
                        $value1['comment_parent_id'] == $value['comment_parent_id'] &&
                        $value1['meme_id']==$value['meme_id'])
                            @php
                                $counter++;
                            $array[$key1]['status'] = 'Done';
                            $a[] = $key1;
                            @endphp
                        @elseif ($value1['type'] == $value['type'] && $value1['comment_id'] !== $value['comment_id']
                        && $value1['comment_parent_id'] !== $value['comment_parent_id']
                        && $value1['meme_id'] == $value['meme_id'])
                            @php
                                $counter++;
                                $a[] = $key1;
                            @endphp

                        @endif
                    @else
                        @if($value1['type']==$value['type']  && $value1['meme_id']==$value['meme_id'])
                            @php
                                $counter++;
                            $array[$key1]['status'] = 'Done';
                            $a[] = $key1;
                            @endphp

                        @endif
                    @endif
                @endforeach
            @else
                @continue
            @endif
            @if(@$value->user->avatar !== null)
                <img src="{{ url ('uploads/users/'.@$value->user->avatar) }}" class="w-30">
            @else
                <img class="w-30" src="{{ asset ('images/random_profile_pic.jpg') }}">
            @endif
            <a class="clr-wht font-12 isReadNotify" href="{{url(@$value->meme->slug.'/'.@$value->meme->uuid)}}"
               data-url="{{url('updateIsReadStatus')}}" data-attr="{{$value->type}}"
               data-meme="{{@$value->meme_id}}" data-comment="{{@$value->comment_id}}">
                @if($counter == 1)
                    <span class="">{{@$value->user->name}}</span>{{' '.$action_type.'.'}}
                @endif
                @if($counter >= 2)
                    @php
                        $c=$counter-1;
                    @endphp
                    <span class="">{{@$value->user->name}}</span>{{' & ' .$c. ' other '.' '.$action_type.'.'}}
                @endif
                @if($counter = 0)
                <span class="">{{@$value->user->name}}</span>{{' '.$action_type.'.'}}
                @endif
                @if(@$value->meme->file_description == 'upload from pc')
                    <img class="w-30 float-right" src="{{ asset('storage/uploads/thumbnails/'.@$value->meme->file)}}"
                         onerror="this.onerror=null;this.src='{{ asset ('images/notfound.jpg')}}';" alt="img">
                @elseif(@$value->meme->file_description == 'from URL')
                    <img class="w-30 float-right" src="{{ url (@$value->meme->file) }}"
                         onerror="this.onerror=null;this.src='{{ asset ('images/notfound.jpg')}}';" alt="img">
                @endif
            </a>
            <br>
            @if(@$value->created_at !== null)
                <small class="navbar-color">{{@$value->created_at->diffForHumans(null,true)}}</small>
            @endif
            <hr class="hr-bg">
        @endforeach
    @else
        <p class="navbar-color">You have no any notification.</p>
    @endif

@endsection
