<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <title> {{ config('app.name') }} @yield('top-title','| Go fun the World') </title>
    <link rel="icon" type="image" href="{{asset('images/lol-whoa-logo-done.png')}}">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description"
          content="@yield('post-description','LOLWHOA has the best funny pics, gifs, gaming, anime,sport, food, memes, cute, fail, wtf photos on the internet.')"/>
    <meta property="og:url" content="@yield('url',config('app.url'))">
    <meta property="og:type" content="website">
    <meta property="og:title" content="@yield('title',config('app.name').'|'.'Go Fun the World')">
    <meta property="og:image" content="@yield('image',asset('images/lolwhoalogo.png'))">
    <meta property="og:image:secure_url" content="@yield('secure-image-url',asset('images/lolwhoalogo.png'))">
    <meta property="og:image:type"
          content="@yield('image-type','image/'.(pathinfo(asset('images/lolwhoalogo.png'), PATHINFO_EXTENSION)))">
    <?php list($width, $height, $type, $attr) = @getimagesize(asset('images/lolwhoalogo.png'));?>
    <meta property="og:image:width" content="@yield('image-width',$width)">
    <meta property="og:image:height" content="@yield('image-height',$height)">
    <meta property="og:description"
          content="@yield('og:description','LOLWHOA has the best funny pics, gifs, gaming,anime,sport, food, memes, cute, fail, wtf photos on the internet.')">
    <meta property="og:site_name" content="{{ config('app.name') }}">
    <meta name="twitter:card" content="summary_large_image">
    <meta name="twitter:site" content="{{config('app.name')}}">
    <meta name="twitter:image" content="@yield('twitter:image',asset('images/lol-whoa-logo-done.png'))">
    <meta name="twitter:description" content="@yield('post-description','LOLWHOA has the best funny pics, gifs,
    gaming, anime,sport, food, memes, cute, fail, wtf photos on the internet.')"/>

    <meta property="fb:app_id" content="{{ config('services.facebook.client_id') }}">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
    <link rel="stylesheet" href="{{asset('css/style.css')}}">
    <link rel="stylesheet" href="{{asset('css/newstylesheet.css')}}">
    <link rel="stylesheet" href="{{asset('css/bootstrap-tagsinput.css')}}"/>

    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css"
          integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
    {{--<style type="text/css">
        .ajax-load {
            padding: 10px 0px;
            width: 100%;
        }
    </style>--}}

    @yield('css')
    <script>
        (function (d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) return;
            js = d.createElement(s);
            js.id = id;
            js.src = "https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v3.0";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));
    </script>
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-135455034-3"></script>
    <script>
        window.dataLayer = window.dataLayer || [];

        function gtag() {
            dataLayer.push(arguments);
        }

        gtag('js', new Date());

        gtag('config', 'UA-135455034-3');
    </script>
    <script async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
    <script>
        (adsbygoogle = window.adsbygoogle || []).push({
            google_ad_client: "ca-pub-6794522269986226",
            enable_page_level_ads: true
        });
    </script>
    <script async custom-element="amp-auto-ads"
            src="https://cdn.ampproject.org/v0/amp-auto-ads-0.1.js">
    </script>
</head>
<body>
<div id="app">
    <nav class="navbar navbar-expand-md navbar-light bg-black shadow-sm header">
        <div class="container">
            <a class="navbar-brand clr-wht font-25" href="{{ url('/')}}">
                <img src="{{asset('images/lol-whoa-logo-done.png')}}" class="logo">
            <!-- {{ config('app.name', 'LOLWHOA') }} -->
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                    aria-controls="navbarSupportedContent" aria-expanded="false"
                    aria-label="{{ __('Toggle navigation') }}">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse text-center" id="navbarSupportedContent">
                <div class="col-6 mx-auto mb-search">
                </div>
                <!-- Authentication Links -->
                @guest
                    <button type="button" class="butn butn-color-black clr-wht auth-butn mb-login-btn" data-toggle="modal"
                            data-target="#login">Login
                    </button>&nbsp;
                    <button type="button" class="butn butn-color-black clr-wht m-l-10 auth-butn mb-register-btn"
                            data-toggle="modal"
                            data-target="#register">Register
                    </button>
                @else
                    <ul class="navbar-nav text-right">
                        <li class="nav-item dropdown">
                            <a id="navbarDropdown" class="nav-link dropdown-toggle clr-wht" href="#" role="button"
                               data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                @if(Auth::user()->avatar == null)
                                    <img src="{{ asset('images/random_profile_pic.jpg') }}" alt=""
                                         class="img-fluid mr-1">
                                @else
                                    <img src="{{ asset('uploads/users/'.Auth::user()->avatar) }}" alt=""
                                         class="img-fluid mr-1">
                                @endif
                                {{ Auth::user()->name }}
                                <span class="caret"></span>
                            </a>



                            <div class="dropdown-menu dropdown-menu-right navbar-black"
                                 aria-labelledby="navbarDropdown">
                                <a class="dropdown-item clr-wht share-menu"
                                   href="{{ url('u'.'/'.Auth::user()->name.'/'.Auth::user()->id) }}">
                                    <i class="fas fa-user mr-2"></i> Profile
                                </a>

                                <a class="dropdown-item clr-wht share-menu" href="{{ route('logout') }}"
                                   onclick="event.preventDefault();document.getElementById('logout-form').submit();">
                                    <i class="fas fa-sign-out-alt mr-2"></i> {{ __('Logout') }}
                                </a>
                                <form id="logout-form" action="{{ route('logout') }}" method="POST"
                                      style="display: none;">
                                    @csrf
                                </form>
                                <!-- <div class="dropdown-divider"></div>
                                <a class="dropdown-item clr-wht share-menu" href="#">
                                    <i class="fas fa-moon mr-2"></i> Night Mode
                                </a> -->
                            </div>
                        </li>
                        <li class="nav-item dropdown notifications" data-url="{{url('updateNotifyStatus')}}">
                            <a id="notifsDropdown" class="nav-link dropdown-toggle clr-wht" href="#" role="button"
                               data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                <i class="fas fa-bell"></i>
                                @if(count(@notifyCount()) > 0)
                                    <span class="badge badge-success notify" id="notifyCount">
                                    {{count(@notifyCount())}}
                                </span>
                                @endif
                            </a>
                            <div class="dropdown-menu dropdown-menu navbar-black" aria-labelledby="notifsDropdown"
                                 @if (count(@notify()) == 0) style="width: 19rem;overflow: auto !important;" @else
                                 style="width: 20rem;height: auto;overflow: auto;" @endif>
                                <div class="recent">
                                    <p class="navbar-color m-l-10">Recent Notifications</p>
                                    <hr class="hr-bg">
                                </div>
                                @include('front.notifications.unseen_Notifications')
                                @if(count(@SeenNotify()) == 0)
                                    @include('front.notifications.seenNotification')
                                @endif
                                @if (count(@SeenNotify()) == 0)
                                    <div class="">
                                        <i class="fa fa-envelope notification  icon-size-35 {{count(@notify()) !== 0 ?
                                     'm-t-50 m-l-60' : 'm-l-85'}}"
                                           aria-hidden="false"></i><br>
                                        <small class="notification font-12 {{count(@notify()) !== 0 ? 'm-l-10':
                                     'm-l-30'}}">You've
                                            got no new notifications
                                        </small>
                                    </div>
                                @endif
                                <div class="m-l-20">
                                    <a class="clr-wht font-11" href="{{route('getAllNotify')}}"><span>See All</span></a>
                                </div>
                            </div>
                        </li>

                        <li class="nav-item dropdown clr-wht">
                            <a id="navbarDropdown" class="nav-link dropdown-toggle clr-wht" href="#" role="button"
                               data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre><i class="fas fa-cog"></i></a>

                               <div class="dropdown-menu dropdown-menu-right navbar-black"
                                 aria-labelledby="navbarDropdown">
                                <a class="dropdown-item clr-wht share-menu"
                                   href="{{ route('setting') }}">
                                    <i class="fas fa-cog"></i> Settings
                                </a>

                                <div class="dropdown-item clr-wht share-menu">
                                     <div class="theme-switch-wrapper">
                                            <span class="clr-wht" style="margin-right: 7px">Change Theme : </span>
                                            <label class="theme-switch" for="checkbox" style="margin-bottom: 0px">
                                                <input type="checkbox" id="checkbox" />
                                                <div class="tb round"></div>
                                            </label>
                                        </div>
                                </div>

                                <!-- <div class="dropdown-divider"></div>
                                <a class="dropdown-item clr-wht share-menu" href="#">
                                    <i class="fas fa-moon mr-2"></i> Night Mode
                                </a> -->
                            </div>
                        </li>

                           <!--  -->

                    </ul>



                   <!-- <button type="button" class="butn ml-2 clr-wht butn-color-black postAMeme" data-toggle="modal"
                            data-target="#postaMeme">
                        <i class="fa fa-pencil"></i>&nbsp;Post a meme
                    </button> -->
                @endguest
            </div>
        </div>
    </nav>
    <div class="navbar shadow-sm navbar-black navbar-pad category-nav" id="nav">
        <div class="container col-8">
            @foreach(@categories() as $c)
                <div class="mb-category">
                    <a class="navbar-color mb-cat" href="{{ url ($c->slug) }}" style="margin-left: 3px">
                        <img src="{{asset('storage/uploads/categoryImage/'.$c->img)}}" alt="{{$c->slug}}" class="mb-margin m-r-5">{{ucwords($c->name)}}
                    </a>
                </div>
            @endforeach
        </div>
        <div class="col-3 pad-right-0 mx-auto mb-search">
                    <form action="{{route('search')}}" method="post">
                        @csrf
                        <input type="text" class="navbar-search butn-color-black clr-wht search" id="search" name="search" data-url="{{ route('search') }}"
                               placeholder="Search for images or tags..." autocomplete="off">
                        <div id="list" class="SearchAutoComplete"></div>
                    </form>
                </div>
    </div>
    <main class="py-4 bg-black">
        <div class="container">
            <div class="row">
                <amp-auto-ads type="adsense" data-ad-client="ca-pub-6794522269986226">
                </amp-auto-ads>
            </div>
            <div class="mb-trending" id="trending-sidebar">
                 <h5 class="clr-wht">Trending Today</h5>
                    <hr class="hr-bg">
            </div>
                 <div class="mb-trending" id="trending-sidebar">
                  @include('front.memes.trending')
                </div>
            <div class="row justify-content-center">
                <div class="col" id="macy-container">
                    @guest
                    @else
                    <div class="navbar shadow-sm navbar-black navbar-pad">
                        
                       <span style="display: inline;"><b><p class="clr-wht" style="font-size: 18px; font-weight: 900; margin-bottom: 0px;margin-left: 7px">Let's Make a Laugh</p></b></span>
                            <button type="button" class="butn clr-wht butn-color-black postAMeme" data-toggle="modal"
                            data-target="#postaMeme">
                            <i class="fa fa-pencil"></i>&nbsp;Post a meme
                            </button>
                        
                    </div>
                    @endguest
                    <br>
                    @yield('content')
                </div>
                <div class="col-sm-12 col-md-4 mb-recent pr-0" id="recent-sidebar">
                    <h5 class="clr-wht">Recent</h5>
                    <hr class="hr-bg">
                    @include('front.memes.recent')
                </div>
            </div>
        </div>

        <a href="#" id="back-to-top" title="Back to top">&uarr;</a>
    </main>

    <!-- Login Modal -->
@include('front.auth.login')

<!-- Register Modal -->
@include('front.auth.register')

<!-- Meme Modal -->
    @include('front.Post-a-meme')

   <!-- <footer class="pad-2 text-muted navbar-black">
        <div class="container">
            {{-- <p class="float-right">
                 <a href="#nav" class="clr-wht">Back to top</a>
             </p>--}}
            <p class="float-right clr-wht">
                <a href="{{route('ContactUs')}}" class="clr-wht">Contact</a>
                 <img src="{{asset('images/plane.png')}}"> Want some Inbox Laugh!
            </p>
            <p class="navbar-color clr-wht">Veteranlogix © 2019. All rights reserved.</p>
        </div>
    </footer> -->


    <footer class="page-footer font-small teal pt-4 pb-4 navbar-black">

  <!-- Footer Text -->
  <div class="container text-center clr-wht">

    <!-- Grid row -->
    <div class="row">

      <!-- Grid column -->
      <div class="col-md-7 mt-md-0 mt-3">

        <!-- Content -->
            <div class="navbar navbar-black category-nav" id="nav">
        <div class="container col-8">
          <!--  @foreach(@categories() as $c)
                <div class="mb-category">
                    <a class="navbar-color mb-cat" href="{{ url ($c->slug) }}" style="margin-left: 3px">
                        <img src="{{asset('storage/uploads/categoryImage/'.$c->img)}}" alt="{{$c->slug}}" class="mb-margin m-r-5">{{ucwords($c->name)}}
                    </a>
                </div>
            @endforeach -->
        </div>
        <div class="col-8 mx-auto mb-search">
                    <form action="{{route('search')}}" method="post">
                        @csrf
                       
                            <input type="text" class="footer-search footer-search-height butn-color-black clr-wht" id="search" name="search" data-url="{{ route('search') }}"
                               placeholder="Search for images or tags..." autocomplete="off">
                        <div id="list" class="SearchAutoComplete"></div>
                    </form>
                </div>
    </div>

        <hr class="hr-bg">

         <div class="container">
            <div class="row justify-content-center">
                <span class="marg-35"><a href="https://www.facebook.com/Lolwhoadotcom/" class="fa fa-facebook fa-lg" style="color: #fff;"></a><p class="text-center">Follow us on Facebook</p></span>
                <span class="marg-35"><a href="#" class="fa fa-instagram fa-lg" style="color: #fff;"></a><p class="justify-content-center">Follow us on Instagram</p></span>
            </div>
        </div>

        <hr class="hr-bg">
          <div class="container footer-copyright text-md-left clr-wht">
    <a href="https://veteranlogix.com/">Veteranlogix</a>
     © 2019. All rights reserved.
  </div>

      </div>
      <!-- Grid column -->

      <hr class="clearfix w-100 d-md-none pb-3">

      <!-- Grid column -->
      <div class="col-md-5 mb-md-0 mb-3">

        <!-- Content -->
        <p class="text-center"> <img src="{{asset('images/plane.png')}}"> Want some Inbox Laugh! </p>
        <form method="POST" action="{{route('SendContactMsg')}}" class="form-horizontal" id="js-contactForm"
          data-url="{{route('SendContactMsg')}}">
        @csrf
        <div class="row justify-content-center">
            <div class="col-md-6">
                <div class="form-group">
                    <input type="text" id="name" placeholder="Name" name="name" class="form-control" required>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <input type="email" id="Emailvalidate" placeholder="Email" name="UserEmail"
                           class="form-control" required>
                </div>
                <span class="text-red" id="validMsg"></span>
            </div>
            <div class="col-md-12">
                <div class="form-group">
                    <textarea type="text" id="message" rows="6" placeholder="Type your message here...."
                              name="message" class="form-control" required></textarea>
                </div>
            </div>
            <div class="col-md-2 col-xs-2 center-block">
                <div class="form-group">
                    <button type="submit" value="Send" class="send-butn butn next clr-wht" id="js-contact" name="loginForm">Send</button>
                </div>
            </div>
        </div>
    </form>
        

      </div>
      <!-- Grid column -->

    </div>
    <!-- Grid row -->

  </div>
  <!-- Footer Text -->

  <!-- Copyright -->

  <!-- Copyright -->

</footer>
<!-- Footer -->

</div>


<div id="fb-root"></div>
<script async defer crossorigin="anonymous"
        src="https://connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v4.0&appId=428753677971754&autoLogAppEvents=1"></script>
<!-- Scripts -->
<script src="{{asset('js/app.js') }}" defer></script>
<script src="{{asset('js/themeChange.js')}}" defer></script>
<script src="{{asset('js/custom.js') }}" defer></script>
<script src="{{asset('js/memeGenerate.js') }}" defer></script>
<script src="{{asset('js/jquery.min.js')}}"></script>
<script src="{{asset('js/bootstrap-tagsinput.js')}}"></script>

{{--<script>
    $('input').tagsinput({
        maxTags : 3
    });

</script>--}}
<script>
    $(document).ready(function () {
        var url = "{{url('/')}}";
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('[name="_token"]').val()
            }
        });
        $.ajax({
            type: "Get",
            url: url,
            dataType: 'JSON',
            success: function (data) {
                window.location = url;
                return true;
            },
        });
    });
</script>
{{--<script type="text/javascript">
    var page = 1;
    $(window).scroll(function () {

        if ($(window).height() <= $(document).height()) {

            page++;
            loadMoreData(page);
        }
    });
    function loadMoreData(page) {
        $.ajax(
            {
                url: '?page=' + page,
                type: "get",
                beforeSend: function () {
                    $('.ajax-load').show();
                }
            })
            .done(function (data) {
                if (data.html == " ") {
                    $('.ajax-load').html("No more records found");
                    return;
                }

                $('.ajax-load').hide();

                $("#post-data").append(data.html);
            })
            .fail(function (jqXHR, ajaxOptions, thrownError) {
               // alert('server not responding...');

            });
    }

</script>--}}
@yield('js')

</body>
</html>
