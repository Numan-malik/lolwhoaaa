@extends('front.main')
@section('top-title','| Page Not Found - Error-404')

@section('content')
    <div class="error-404">
        <div class="error-code m-b-10 m-t-20">404</div>
        <h2 class="font-bold navbar-color">Oops 404! That page can’t be found.</h2>

        <div class="error-desc">
            Sorry, but the page you are looking for was either not found or does not exist. <br/>
            Try refreshing the page or click the button below to go back to the Homepage.
            <div><br/>
                <a href="{{url('/')}}" class="btn go-back-btn">Go back to Homepage</a>
            </div>
        </div>
    </div>
@endsection

