@if(Session::has('success-msg'))
    <div class="alert alert-success alert-dismissible">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        <strong>Success !</strong> {!! session ('success-msg') !!}
    </div>
@endif
@if(Session::has('error-msg'))
    <div class="alert alert-danger alert-dismissible">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        <strong>Alert !</strong> {!! session ('error-msg') !!}
    </div>
@endif
@error('name')
<div class="alert alert-danger alert-dismissible">
    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
    <strong>{!! $errors->first('name', ':message') !!}</strong>
</div>
@enderror
@error('UserEmail')
<div class="alert alert-danger alert-dismissible">
    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
    <strong>{!! $errors->first('UserEmail', ':message') !!}</strong>
</div>
@enderror
@error('full_name')
<div class="alert alert-danger alert-dismissible">
    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
    <strong>{!! $errors->first('full_name', ':message') !!}</strong>
</div>
@enderror
@error('day')
<div class="alert alert-danger alert-dismissible">
    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
    <strong>{!! $errors->first('day', ':message') !!}</strong>
</div>
@enderror
@error('month')
<div class="alert alert-danger alert-dismissible">
    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
    <strong>{!! $errors->first('month', ':message') !!}</strong>
</div>
@enderror
@error('year')
<div class="alert alert-danger alert-dismissible">
    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
    <strong>{!! $errors->first('year', ':message') !!}</strong>
</div>
@enderror
@error('password')
<div class="alert alert-danger alert-dismissible">
    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
    <strong>{!! $errors->first('password', ':message') !!}</strong>
</div>
@enderror
@error('old_password')
<div class="alert alert-danger alert-dismissible">
    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
    <strong>{!! $errors->first('old_password', ':message') !!}</strong>
</div>
@enderror
@error('password_confirmation')
<div class="alert alert-danger alert-dismissible">
    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
    <strong>{!! $errors->first('password_confirmation', ':message') !!}</strong>
</div>
@enderror
