@extends('front.main')
@section('top-title','|'.' '.@$category->name)
@section('content')
<amp-auto-ads type="adsense"
              data-ad-client="ca-pub-6794522269986226">
</amp-auto-ads>
    <div class="row">
        <img src="{{asset('storage/uploads/categoryImage/'.@$category->img)}}" alt="" class="" style="width: 25px; height: 25px; margin-left: 15px;">
        <h5 class="clr-wht m-l-15">{{ucwords(@$category->name)}}</h5>
    </div>
    <hr class="hr-bg" style="margin-top: 9px;">
    @include('front.memes.master-meme',['memes' => $memes])

@endsection

