@extends('front.main')
@section('top-title','| Contact')
@section('content')

    <div class="row">
        <div class="col-md-12">
            <h1 class="mb-2 text-center clr-wht">Feel Free to Contact Us</h1>
        </div>
    </div>
    <hr class="hr-bg">
    <div id="js-msg" class="m-t-20"></div>
    <div id="msgSent" class="m-t-20 text-center"></div>
    <form method="POST" action="{{route('SendContactMsg')}}" class="form-horizontal" id="js-contactForm"
          data-url="{{route('SendContactMsg')}}">
        @csrf
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="Name" class="navbar-color">Name * </label>
                    <input type="text" id="name" placeholder="Your name" name="name" class="form-control" required>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="email" class="navbar-color">Email * </label>
                    <input type="email" id="Emailvalidate" placeholder="Lolwhoa@example.com" name="UserEmail"
                           class="form-control" required>
                </div>
                <span class="text-red" id="validMsg"></span>
            </div>
            <div class="col-md-12">
                <div class="form-group">
                    <label for="message" class="navbar-color">Message * </label>
                    <textarea type="text" id="message" rows="6" placeholder="Type your message here...."
                              name="message" class="form-control" required></textarea>
                </div>
            </div>
            <div class="col-md-12">
                <div class="form-group">
                    <button type="submit" value="Send" class="btn next clr-wht" id="js-contact" name="loginForm">Send</button>
                </div>
            </div>
        </div>
    </form>

@endsection

