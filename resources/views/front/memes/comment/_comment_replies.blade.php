@foreach($comments as $comment)
    <?php
    $replyCount = \App\Comment::where('parent_id', $comment->id)->get()->count();
    $Commentupvotes = \App\CommentVote::where('comment_id', $comment->id)->where('value', 1)->get()->count();
    $Commentdownvotes = \App\CommentVote::where('comment_id', $comment->id)->where('value', 0)->get()->count();
    if (Auth::check())
        $Commentvote = Auth::user()->commentVotes()->where('comment_id', $comment->id)->first();
    ?>
    <a href="{{ url('u'.'/'.$comment->user->name.'/'.$comment->user->id) }}">
        @if($comment->user->avatar !== null)
            <img src="{{ url ('uploads/users/'.@$comment->user->avatar) }}"
                 class="b-r-5 {{$comment->parent_id !== null ?'m-t-15 m-l-50' : 'm-t-10'}}" width="50px">
        @else
            <img class="b-r-5 {{$comment->parent_id !== null ?'m-t-15 m-l-50' : 'm-t-10'}}"
                 src="{{ asset('images/random_profile_pic.jpg') }}" width="50px">
        @endif
    </a>

    <div class="{{$comment->parent_id !== null ? 'parent-Cmnt' : 'rply-Cmnt'}}">
        <a href="{{ url('u'.'/'.$comment->user->name.'/'.$comment->user->id) }}" class="clr-wht">{{@$comment->user->name }}</a>
        &nbsp;&nbsp;
        <small class="navbar-color">@if(@$comment->created_at){{@$comment->created_at->diffForHumans(null,true)}}@endif
            - {{$replyCount}} replies
        </small>
        <div class="navbar-color row m-l-0">{{ $comment->body }} <br>
            <a href="#" data-toggle="collapse" data-target="#rply_{{$comment->id}}" class="comment m-l-10">Reply</a>
            <div class="m-l-10">
                @if(isset($Commentvote))
                    @if($Commentvote->value == 1)
                        <button type="button" class="upvote CommentVote btn btn-sm clr-wht like" data-islike="1"
                                data-value="1" id="upvote_{{$comment->id}}" data-id="{{ $comment->id }}"
                                data-url="{{url('StoreCommentVotes')}}"
                                style="background: #cbb6b64d">
                            <i class="fas fa-arrow-up"></i>
                        </button>
                    @else
                        <button type="button" class="upvote CommentVote btn btn-sm clr-wht like"
                                id="upvote_{{$comment->id}}" data-value="1" data-islike="0" data-id="{{ $comment->id }}"
                                data-url="{{url('StoreCommentVotes')}}">
                            <i class="fas fa-arrow-up"></i>
                        </button>
                    @endif
                @else
                    <button type="button" id="upvote_{{$comment->id}}"
                            class="upvote CommentVote btn btn-sm clr-wht like" data-value="1"
                            data-id="{{ $comment->id }}" data-islike="0"
                            data-url="{{url('StoreCommentVotes')}}">
                        <i class="fas fa-arrow-up"></i>
                    </button>
                @endif

                <b class="clr-wht m-t-3" id="upCount_{{$comment->id}}"
                   data-value="{{$Commentupvotes}}">{{$Commentupvotes ?$Commentupvotes : 0}}</b>

                @if(isset($Commentvote))
                    @if($Commentvote->value == 0)
                        <button type="button" class="downvote CommentVote btn btn-sm clr-wht dislike m-l-5"
                                id="downvote_{{$comment->id}}" data-value="0" data-isdislike="1"
                                data-id="{{ $comment->id }}"
                                data-url="{{url('StoreCommentVotes')}}" style="background: #cbb6b64d">
                            <i class="fas fa-arrow-down"></i>
                        </button>
                    @else
                        <button type="button" class="downvote CommentVote btn btn-sm clr-wht dislike m-l-5"
                                id="downvote_{{$comment->id}}" data-value="0" data-isdislike="0"
                                data-id="{{ $comment->id }}"
                                data-url="{{url('StoreCommentVotes')}}">
                            <i class="fas fa-arrow-down"></i>
                        </button>
                    @endif
                @else
                    <button type="button" class="downvote CommentVote btn btn-sm clr-wht dislike m-l-5"
                            data-isdislike="0"
                            data-value="0" id="downvote_{{$comment->id}}" data-id="{{ $comment->id }}"
                            data-url="{{url('StoreCommentVotes')}}">
                        <i class="fas fa-arrow-down"></i>
                    </button>
                @endif

                <b class="clr-wht m-t-3" id="downCount_{{ $comment->id }}"
                   data-value="{{$Commentdownvotes}}">{{$Commentdownvotes ? $Commentdownvotes : 0}}</b>
            </div>

            @if($comment->parent_id == null)
                <button type="button" class="btn btn-sm clr-wht m-l-5">
                    <i class="fa fa-comment"></i>
                </button>
                <b class="clr-wht m-l-5"> {{$replyCount}}</b>
            @endif
        </div>

        <div id="rply_{{$comment->id}}" class="collapse">
            <form method="post" action="{{ route('reply.add') }}">
                @csrf
                <div class="form-group m-t-10">
                    <textarea type="text" name="comment_body" class="form-control comment" maxlength="1000"
                              placeholder="Max length is 1000" onkeyup="countChar(this)" required>
                    </textarea>
                    <input type="hidden" name="meme_id" value="{{ $meme_id }}"/>
                    <input type="hidden" name="comment_id" value="{{ $comment->parent_id !== null ? $comment->parent_id: $comment->id }}"/>
                </div>
                <div class="form-group">
                    <input type="submit" class="butn btn-success btn-width butn-color-black btn-submit" name="rplySubmit"
                           value="Submit"/>
                </div>
            </form>
        </div>
        {{--  @if($comment->parent_id == null)
              <a href="javascript:void(0)" class="ShowHideReplies" data-id="{{$comment->id}}">Hide Replies</a>
          @endif--}}
    </div>
    {{-- @if($comment->parent_id == null) id="comments_{{$comment->id}}" @endif--}}
    @include('front.memes.comment._comment_replies', ['comments' => $comment->replies, 'superComment'])
@endforeach


