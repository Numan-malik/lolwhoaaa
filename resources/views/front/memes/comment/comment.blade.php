<form method="post" action="{{ route('comment.add') }}">
    @csrf
    <div class="form-group" id="comments">
        @if(@Auth::user()->avatar == null)
            <img src="{{asset('images/random_profile_pic.jpg')}}" class="cmnt-img">
        @else
            <img src="{{asset('uploads/users/'.@Auth::user()->avatar)}}" class="cmnt-img">
        @endif
            <textarea type="text" name="comment_body" placeholder="Post your comment Here" rows="3" onkeyup="countChar(this)" required="required" maxlength="1000"
                      class="form-control col-md-12 comment textarea">
            </textarea>
        <div class="character-count">
            <span id="charNum" class="clr-wht"></span>
            <span class="clr-wht">/1000 characters</span>
        </div>
        <input type="hidden" name="meme_id" value="{{ $memes->id }}"/>
    </div>
    <div class="form-group m-t--10">
        <button type="submit" class="btn btn-success btn-width navbar-bg btn-submit" name="SubmitComment">Submit
        </button>
    </div>
</form>

@include('front.memes.comment._comment_replies', ['comments' => $memes->comments, 'meme_id' => $memes->id])

