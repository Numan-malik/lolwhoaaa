@extends('front.main')
@section('top-title','|'.' '.$memes->title)
@section('post-description',$upvotes.' '.'likes'.' '.','.' '.'comments'.' '.$upvotes.' '.'-'.' '.$memes->title.' '.'-'.' '.'LOLWHOA has the best funny pics, gifs, gaming, anime,sport, food, memes, cute, fail, wtf photos on the internet')
@section('url',url($memes->slug.'/'.$memes->uuid))
@section('title',$memes->title)
@section('og:description','LOLWHOA has the best funny pics, gifs, gaming, anime,sport, food, memes, cute, fail, wtf
photos on the internet')
@if($memes->file_description == 'upload from pc')
    @section('image',asset('storage/uploads/thumbnails/'.$memes->file))
    @section('secure-image-url',asset('storage/uploads/thumbnails/'.$memes->file))
    @section('image-type','image/'.(pathinfo(asset('storage/uploads/thumbnails/'.$memes->file), PATHINFO_EXTENSION)))
    <?php list($width, $height, $type, $attr) = @getimagesize(asset('storage/uploads/thumbnails/'.$memes->file));?>
    @section('image-width',$width)
    @section('image-height',$height)
@section('twitter:image',asset('storage/uploads/thumbnails/'.$memes->file))
@elseif($memes->file_description == 'from URL')
    @section('image',$memes->file)
@section('twitter:image',$memes->file)
@endif

@section('content')
    <div class="card meme-detail border-0 col-md-11 col-sm-12 border-0 main-theme-color mn-w-40">
        <amp-auto-ads type="adsense"
              data-ad-client="ca-pub-6794522269986226">
        </amp-auto-ads>
        <div class="card-header navbar-black">
            <small class="recent-meme-meta">
                <a href="{{ url('u'.'/'.@$memes->user->name.'/'.@$memes->user->id) }}">
                @if(@$memes->user->avatar !== null)
                    <img class="poster-icon mr-1 img-fluid w-30" src="{{ asset ('uploads/users/'.@$memes->user->avatar) }}">
                @else
                    <img class="poster-icon mr-1 img-fluid w-30" src="{{ asset ('images/random_profile_pic.jpg') }}">
                @endif

                 <b class="">
                    <a href="{{url( $memes->slug.'/'.$memes->uuid )}}" class="clr-wht meme-heading">{{$memes->title}}</a>
                </b>
                <br>
                @if(count($MemeRandom)>0)
                    <a type="button" href="{{url( $MemeRandom[0]->slug.'/'.$MemeRandom[0]->uuid )}}" class="btn btn-sm clr-wht float-right butn-color-black">Next Post</a>
                @endif




                    <b class="navbar-color" style="font-size: 11px;font-weight: initial">{{@$memes->user->name}}</b>
                </a>
                <b class="navbar-color" style="font-size: 9px;font-weight: initial">
                    - {{$memes->created_at->diffForHumans(null,true)}}</b>
            </small>
            <div>
                
            </div>
            <small>
                <i class="fa fa-tag navbar-color"></i>&nbsp;
                <b class="navbar-color font-11 font-inherit">{{$memes->tags}}</b>
            </small>
        </div>
        @if($memes->file_description == 'upload from pc')
            @if(@pathinfo(asset ('storage/uploads/thumbnails/'.$memes->file), PATHINFO_EXTENSION) == 'gif')
                <img class="card-img-top border-radius-0" src="{{ asset ('storage/uploads/thumbnails/'.$memes->file)}}"
                     alt="{{$memes->title}}" onerror="this.onerror=null;this.src='{{ asset ('images/notfound.jpg')}}';">
            @else
                <img class="card-img-top border-radius-0" src="{{ asset ('storage/uploads/memes/'.$memes->file) }}"
                     alt="{{$memes->title}}" onerror="this.onerror=null;this.src='{{ asset ('images/notfound.jpg')}}';">
            @endif
        @elseif($memes->file_description == 'from URL')
            <img class="card-img-top border-radius-0" src="{{ url ($memes->file) }}" alt="{{$memes->title}}"
                 onerror="this.onerror=null;this.src='{{ asset ('images/notfound.jpg')}}';">
        @endif

        <div class="card-block navbar-black ht-50">
            <div class="card-text m-l-10 m-t-10">
                <div class="row m-l-5">
                    @if(Auth::check())
                        <?php $vote = Auth::user()->votes()->where('meme_id', $memes->id)->first() ?>
                    @endif
                    @if(isset($vote))
                        @if($vote->value == 1)
                            <button type="button" class="upvote vote btn btn-sm clr-wht like" data-islike="1"
                                    data-value="1" id="upvote_{{$memes->id}}" data-id="{{ $memes->id }}"
                                    data-url="{{url('storeMemeVotes')}}"
                                    style="background: #cbb6b64d">
                                <i class="fas fa-thumbs-up"></i>
                            </button>
                        @else
                            <button type="button" class="upvote vote btn btn-sm clr-wht like" id="upvote_{{$memes->id}}"
                                    data-value="1" data-islike="0" data-id="{{ $memes->id }}"
                                    data-url="{{url('storeMemeVotes')}}">
                                <i class="fas fa-thumbs-up"></i>
                            </button>
                        @endif
                    @else
                        <button type="button" id="upvote_{{$memes->id}}" class="upvote vote btn btn-sm clr-wht like"
                                data-value="1" data-id="{{ $memes->id }}" data-islike="0"
                                data-url="{{url('storeMemeVotes')}}">
                            <i class="fas fa-thumbs-up"></i>
                        </button>
                    @endif

                    <b class="clr-wht m-t-3 m-l-5" id="upCount_{{$memes->id}}"
                       data-value="{{$upvotes}}">{{$upvotes?$upvotes : 0}}</b>

                    @if(isset($vote))
                        @if($vote->value == 0)
                            <button type="button" class="downvote vote btn btn-sm clr-wht dislike m-l-5"
                                    id="downvote_{{$memes->id}}" data-value="0" data-isdislike="1"
                                    data-id="{{ $memes->id }}"
                                    data-url="{{url('storeMemeVotes')}}" style="background: #cbb6b64d">
                                <i class="fas fa-thumbs-down"></i>
                            </button>
                        @else
                            <button type="button" class="downvote vote btn btn-sm clr-wht dislike m-l-5"
                                    id="downvote_{{$memes->id}}" data-value="0" data-isdislike="0"
                                    data-id="{{ $memes->id }}"
                                    data-url="{{url('storeMemeVotes')}}">
                                <i class="fas fa-thumbs-down"></i>
                            </button>
                        @endif
                    @else
                        <button type="button" class="downvote vote btn btn-sm clr-wht dislike m-l-5" data-isdislike="0"
                                data-value="0" id="downvote_{{$memes->id}}" data-id="{{ $memes->id }}"
                                data-url="{{url('storeMemeVotes')}}">
                            <i class="fas fa-thumbs-down"></i>
                        </button>
                    @endif

                    <b class="clr-wht m-l-5 m-t-3" id="downCount_{{ $memes->id }}"
                       data-value="{{$downvotes}}">{{$downvotes ? $downvotes : 0}}</b>

                    <a type="button" href="{{url ($memes->slug.'/'.$memes->uuid)}}#comments" class="btn btn-sm
                    clr-wht m-l-5">
                        <i class="fa fa-comment"></i>
                    </a>
                    <b class="clr-wht m-t-3"> {{$totalComments}}</b>

                    <a type="button" class="btn btn-sm clr-wht s-btn m-l-5" id="notifsDropdown" data-toggle="dropdown"
                       aria-haspopup="true" aria-expanded="false" v-pre>
                        <i class="fa fa-share"></i>
                    </a>
                    <b class="clr-wht m-t-3">{{$memes->share_count}}</b>

                    <div class="dropdown-menu dropdown-menu share" aria-labelledby="notifsDropdown">
                        <a class="dropdown-item clr-wht share-menu share-btn" data-url="{{url('MemeShareCount')}}"
                           data-id="{{$memes->id}}"
                           href="https://www.facebook.com/sharer/sharer.php?u=https%3A%2F%2Flolwhoa.com%2F<?php echo $memes->slug . '/' . $memes->uuid;?>&amp;src=sdkpreparse"
                           target="_blank">
                            <i class="fa fa-facebook icon-size mr-2"></i> Facebook
                        </a>
                        <a class="dropdown-item clr-wht share-menu share-btn" data-url="{{url('MemeShareCount')}}"
                           data-id="{{$memes->id}}"
                           href="https://twitter.com/intent/tweet?url=https%3A%2F%2Flolwhoa.com%2F<?php echo
                               $memes->slug . '/' . $memes->uuid?>&layout=button_count&size=small&appId=313767242902864&width=70&height=20"
                           target="_blank">
                            <i class="fa fa-twitter icon-size mr-2"></i> Twitter
                        </a>

                        <a class="dropdown-item clr-wht share-menu share-btn" data-url="{{url('MemeShareCount')}}"
                           data-id="{{$memes->id}}"
                           href="http://www.linkedin.com/shareArticle?mini=true&amp;url=https%3A%2F%2Flolwhoa.com%2F<?php echo $memes->slug . '/' . $memes->uuid;
                           ?>&layout=button_count&size=small&appId=313767242902864&width=70&height=20"
                           target="_blank"><span
                                class="fa fa-linkedin icon-size mr-2"></span> Linkedin</a>
                    </div>
                </div>
                <button type="button" class="btn btn-sm clr-wht butn-color-black float-right m-r-10 m-t--25">
                    <i class="fa fa-eye"></i>
                    <b class="clr-wht">views {{$memes->view_count}}</b>
                </button>
            </div>
        </div>
    </div>

    <h1 class="clr-wht h1-font m-30">{{$totalComments}} Comments</h1>
    <hr class="hr-bg m-30">
    <div class="card-body">
        @include('front.memes.comment.comment')
    </div>

    @if(Auth::check())
        <div id="AuthCheck" style="display: none">{{Auth::user()->id}}</div>
    @endif
@endsection

