<div class="row justify-content-center">
{{-- {{trending_memes()}} --}}
@foreach(trending_memes() as $t)
<div class="col-xl-2 col-lg-2 col-md-4 .col-sm-4 col-12"  style="text-align: center; padding: 0px;">
    <div class="card trending-meme main-theme-color ">

        <div>
            <a href="{{ url ($t->slug.'/'.$t->uuid ) }}" target="_blank">
                @if($t->file_description == 'upload from pc')
                    <img class="trend-img-fluid" src="{{ asset ('storage/uploads/thumbnails/'.$t->file) }}"
                         onerror="this.onerror=null;this.src='{{ asset ('images/notfound.jpg')}}';">
                @elseif($t->file_description == 'from URL')
                    <img class="trend-img-fluid" src="{{ url ($t->file) }}" onerror="this.onerror=null;this.src='images/notfound.jpg';">
                @endif
            </a>
        </div>

        <div class="card-body m-l--8">

            <a href="{{ url ($t->slug.'/'.$t->uuid ) }}"
               target="_blank" class="trending-meme-title clr-wht sidebar-font">{{$t->title}}</a>
            <small class="trending-meme-meta">
                <a href="{{ url('u'.'/'.$t->user->name.'/'.$t->user->id) }}">
                @if(@$t->user->avatar !== null)
                    <img src="{{ url ('uploads/users/'.@$t->user->avatar) }}" class="img-fluid poster-icon mr-1"
                         onerror="this.onerror=null;this.src='images/notfound.jpg';">
                @else
                    <img class="poster-icon mr-1 img-fluid" src="{{ asset ('images/random_profile_pic.jpg') }}"
                         onerror="this.onerror=null;this.src='{{ asset ('images/notfound.jpg')}}';">
                @endif
                    <b class="navbar-color" style="font-size: 9px;font-weight: initial">{{@$t->user->name}}</b>
                </a>
                <b class="navbar-color" style="font-size: 9px;font-weight: initial">
                    - {{$t->created_at->diffForHumans(null,true)}}</b>
            </small>
        </div>
    </div>
</div>

@endforeach 
</div>
