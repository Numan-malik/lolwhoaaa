<div class="row justify-content-center">
@foreach(recent_memes() as $row)
<div class="col-xl-6 col-lg-6 col-md-12 .col-sm-12 col-12">
    <div class="card recent-meme main-theme-color">
        <div class="">
            <a href="{{ url ($row->slug.'/'.$row->uuid ) }}" target="_blank">
                @if($row->file_description == 'upload from pc')
                    <img class="img-fluid" src="{{ asset ('storage/uploads/thumbnails/'.$row->file) }}"
                         onerror="this.onerror=null;this.src='{{ asset ('images/notfound.jpg')}}';">
                @elseif($row->file_description == 'from URL')
                    <img class="img-fluid" src="{{ url ($row->file) }}"
                         onerror="this.onerror=null;this.src='{{ asset('images/notfound.jpg')}}';">
                @endif
            </a>
        </div>
        <div class="card-body m-l--8">
            <a href="{{ url ($row->slug.'/'.$row->uuid ) }}"
               target="_blank" class="recent-meme-title clr-wht sidebar-font">{{$row->title}}</a>
            <small class="recent-meme-meta">
                <a href="{{ url('u'.'/'.$row->user->name.'/'.$row->user->id) }}">
                    @if(@$row->user->avatar !== null)
                        <img src="{{ url ('uploads/users/'.@$row->user->avatar) }}" class="img-fluid poster-icon mr-1"
                             onerror="this.onerror=null;this.src='{{ asset ('images/notfound.jpg')}}';">
                    @else
                        <img class="poster-icon mr-1 img-fluid" src="{{ asset ('images/random_profile_pic.jpg') }}"
                             onerror="this.onerror=null;this.src='{{ asset ('images/notfound.jpg')}}';">
                    @endif

                    <b class="navbar-color" style="font-size: 9px;font-weight: initial">{{@$row->user->name}}</b>
                </a>
                <b class="navbar-color" style="font-size: 9px;font-weight: initial">
                    - {{$row->created_at->diffForHumans(null,true)}}</b>
            </small>
        </div>
    </div>
</div>
@endforeach
</div>