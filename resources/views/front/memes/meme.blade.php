<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Laravel</title>
    <link rel="stylesheet" href="{{asset('assets/css/chosen.css')}}">
    <link rel="stylesheet" href="{{asset('css/style.css')}}">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
          integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css"
          integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">

</head>
<body>
<div class="container">
    <h1>Post a Meme</h1>
    <br>
    <br>
    <form method="post" action="{{route('memes')}}" enctype="multipart/form-data">
        @csrf
        <div class="form-group col-md-6">
            <label for="avatar">Avatar</label>
            <input type="file" name="img">
            <img id="myImg" src="" alt="" width="130px"/>
            <span class="text-danger">{{ $errors->first('img') }}</span>
        </div>
        <div class="form-group col-md-6" id="image-url">
            <label for="avatar">Or</label>
            <input type="text" name="imgUrl" class="col-md-12" placeholder="Paste your Image URL">
            @if(Session::has('message'))
                <span class="text-danger">{{ Session::get('message') }}</span>
            @endif
        </div>
        <div class="form-group col-md-6">
            <label for="formGroupExampleInput">Title</label>
            <input type="text" name="title" class="form-control" id="formGroupExampleInput"
                   placeholder="Please enter name" value="" required>
            <span class="text-danger">{{ $errors->first('title') }}</span>
        </div>

        <div class="form-group col-md-6">
            <label for="multiple" class="control-label">Select Category</label>
            <select name="category[]" data-placeholder="Choose a Category..." class="chosen-select col-md-6" multiple
                    tabindex="4">
                <option value=""></option>
                @foreach($categories as $cat)
                    <option value="{{$cat->id}}">{{$cat->name}}</option>
                @endforeach
            </select>
        </div>
        <div class="form-group col-md-12">
            <button type="submit" id="send_form" class="btn btn-success">Submit</button>
        </div>
        <script src="{{asset('assets/js/jquery-3.2.1.min.js')}}" type="text/javascript"></script>
        <script src="{{asset('assets/js/chosen.jquery.js')}}" type="text/javascript"></script>
        <script src="{{asset('assets/js/init.js')}}" type="text/javascript" charset="utf-8"></script>

    </form>
</div>
<div class="col-md-12">
    @foreach($memes as $row)
        <div class="col-sm-6 col-md-4 col-lg-3 mt-4" style="width: 15% !important;">

            <div class="card">
                @if($row->file_description == 'upload from pc')
                    <img class="card-img-top" src="{{ asset ('uploads/'.$row->file) }}">
                @elseif($row->file_description == 'from URL')
                    <img class="card-img-top" src="{{ url ($row->file) }}">
                @endif

                <div class="card-block">
                    <h4 class="card-title"><a href="{{ url (''.$row->slug ) }}"
                                              target="_blank">{{$row->title}}</a></h4>
                    <div class="card-text">
                        <i class="fa fa-tag"></i>
                        @foreach($row->categories as $c)
                            <a>{{$c->category->name}}, </a>
                        @endforeach
                    </div>
                </div>
                <div class="card-footer">
                    <span class="float-right">posted in {{date('d-M-y',strtotime($row->created_at))}}</span>
                </div>
            </div>

        </div>
    @endforeach
</div>

<script>
    //image
    $(function () {
        $(":file").change(function () {
            if (this.files && this.files[0]) {
                var reader = new FileReader();
                reader.onload = imageIsLoaded;
                reader.readAsDataURL(this.files[0]);
            }
        });
    });

    function imageIsLoaded(e) {
        $('#myImg').attr('src', e.target.result);
    };

</script>
</body>
</html>
