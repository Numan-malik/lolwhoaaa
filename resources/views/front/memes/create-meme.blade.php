@extends('front.main')
@section('top-title','| Create your own Meme')
@section('content')
     <div class="container">
         <div class="page-header">
             <h5 class="clr-wht"><i class="fa fa-lg fa-paint-brush clr-wht" aria-hidden="true" style="margin-right: 5px"></i>Create your awesome Meme now!</h5>
         </div>
         <hr class="hr-bg">
         <p>&nbsp;</p>
         <div class="row">
             <div class="col-md-4">
                 <!-- BEGIN: Canvas rendering of generated Meme -->
                 <canvas id="meme" class="img-thumbnail img-fluid">
                     Unfortunately your browser does not support canvas.
                 </canvas>
                 <!-- END: Canvas rendering of generated Meme -->
                 <a href="javascript:;" class="btn btn-primary btn-block" id="download_meme">
                     <i class="fa fa-download" aria-hidden="true"></i> Download
                 </a>
                 <button type="button" class="btn navbar-bg btn-block clr-wht" data-toggle="modal"
                         data-target="#shareMemeWithLolwhoa" name="jsGenerateShare" id="js--share-with-lolwhoa">
                     Share on <img src="{{asset('images/lol-whoa-logo-done.png')}}" width="30px">
                 </button>
                 <img id="start-image" src="{{ asset('storage/uploads/generatedMeme/'.@$SelectedMeme['file'])}}" alt=""
                      style="display: none" onerror="this.onerror=null;this.src='{{ asset('images/notfound.jpg')}}';"/>
             </div>
             <div class="col-md-8">
                 <div class="row">
                     <div class="col-md-12">
                         <label class="navbar-color">Image:</label><br>
                         <div class="files color">
                             <input type="file" class="form-control js-generate-meme" id="js-generate-file" name="img" style="opacity:0.25;"
                                    accept="image/x-png,image/gif,image/jpeg" data-url="{{route('generatedMemeStore')}}">
                         </div>
                         <p id="generate-img-err" class="text-red"></p>
                          {{-- <b class="clr-wht">or</b>--}}
                     </div>
                      {{-- <div class="col-md-12">
                           <label class="navbar-color">Image Url:</label><br>
                           <div class="m-t-10">
                               <input type="text" class="form-control" id="js-url-generate-img" pattern="https?://.+" name="file" placeholder="https://" required/>
                           </div>
                       </div>--}}
                 </div>
                 <h3 class="navbar-color font-15 m-t-10">Top Text:</h3>
                 <div class="form-group">
                     <input type="text" class="form-control form-control-lg" value="Have you expected this to be"
                            id="text_top" maxlength="30"/>
                 </div>

                 <h3 class="navbar-color font-15">Bottom Text:</h3>
                 <div class="form-group">
                     <input type="text" class="form-control form-control-lg" value="funny?" id="text_bottom"
                            maxlength="30"/>
                 </div>

                 <div class="card">
                     <div class="card-header">
                         <i class="fa fa-cogs" aria-hidden="true"></i> More options
                     </div>
                     <div class="card-body">

                         <div class="row">
                             <div class="col-md-12">
                                 <div class="form-group">
                                     <div class="row">
                                         <label class="control-label col-md-3" for="text_top_offset">Offset from
                                             top:</label>
                                         <div class="col-md-7">
                                             <input style="width:100%" id="text_top_offset" type="range"
                                                    class="navbar-color" min="0" max="500" value="50"/>
                                         </div>
                                         <div class="col-md-2 setting-value">
                                             <span id="text_top_offset__val">50px</span>
                                         </div>
                                     </div>
                                 </div>
                                 <div class="form-group">
                                     <div class="row">
                                         <label class="control-label col-md-3" for="text_bottom_offset">Offset from
                                             top:</label>
                                         <div class="col-md-7">
                                             <input style="width:100%" id="text_bottom_offset" type="range" min="0"
                                                    max="500" value="450"/>
                                         </div>
                                         <div class="col-md-2 setting-value">
                                             <span id="text_bottom_offset__val">450px</span>
                                         </div>
                                     </div>
                                 </div>

                                 <div class="form-group">
                                     <div class="row">
                                         <label class="control-label col-md-3" for="canvas_size">Meme size:</label>
                                         <div class="col-md-7">
                                             <input style="width:100%" id="canvas_size" type="range" min="0" max="1000"
                                                    value="500"/>
                                         </div>
                                         <div class="col-md-2 setting-value">
                                             <span id="canvas_size__val">500px</span>
                                         </div>
                                     </div>
                                 </div>

                                 <div class="form-group">
                                     <div class="row">
                                         <label class="control-label col-md-3" for="text_font_size">Font size:</label>
                                         <div class="col-md-7">
                                             <input style="width:100%" id="text_font_size" type="range" min="0" max="72"
                                                    value="28"/>
                                         </div>
                                         <div class="col-md-2 setting-value">
                                             <span id="text_font_size__val">28</span>pt
                                         </div>
                                     </div>
                                 </div>

                                 <div class="form-group">
                                     <div class="row">
                                         <label class="control-label col-md-3" for="text_line_height">Line
                                             height:</label>
                                         <div class="col-md-7">
                                             <input style="width:100%" id="text_line_height" type="range" min="0"
                                                    max="100" value="15"/>
                                         </div>
                                         <div class="col-md-2 setting-value">
                                             <span id="text_line_height__val">15</span>pt
                                         </div>
                                     </div>
                                 </div>

                                 <div class="form-group">
                                     <div class="row">
                                         <label class="control-label col-md-3" for="text_stroke_width">Stroke
                                             width:</label>
                                         <div class="col-md-7">
                                             <input style="width:100%" id="text_stroke_width" type="range" min="0"
                                                    max="20" value="4"/>
                                         </div>
                                         <div class="col-md-2 setting-value">
                                             <span id="text_stroke_width__val">4</span>pt
                                         </div>
                                     </div>
                                 </div>

                             </div>
                         </div>

                     </div>
                 </div>

             </div>
         </div>
     </div>
    <!-- Modal -->
    <div class="modal fade" id="shareMemeWithLolwhoa" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content main-theme-color">
                <div>
                    <button type="button" class="close text-red float-right m-r-10" data-dismiss="modal">&times;
                    </button>
                </div>
                <div id="js--msg"></div>
                <div class="m-20">
                    <div class="form-group">
                        <label class="control-label clr-wht">Title</label>
                        <input type="text" class="form-control" id="title" name="title"
                               placeholder="e.g. it really do be like that" maxlength="50" required/>
                    </div>
                    <div class="form-group">
                        <label class="control-label clr-wht">Tags</label><br>
                        <input id="tags" class="form-control" data-role="tagsinput" type="text" name="tags"
                               placeholder="tags 1 , tags 2 , tags 3" required>
                    </div>
                    <div class="form-group">
                        <label for="multiple" class="control-label clr-wht">Categories</label><br>
                        <select name="category" data-placeholder="Choose a Category..." class="form-control"
                                id="category" required>
                            <option value=""></option>
                            @foreach(@categoryForMemeDetails() as $cat)
                                <option value="{{$cat->id}}">{{$cat->name}}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="form-group">
                        <button type="button" id="js--GenerateMeme" class="btn btn-primary btn-width navbar-bg
                            next m-b-20 m-l-10" data-url="{{route('CreateMeme')}}">Post
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
