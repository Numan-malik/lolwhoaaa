
<div class="row justify-content-center" id="post-data">
    @if(isset($memes))
        @foreach($memes as $row)
            <?php
            $upvotes = \App\MemeVote::where('meme_id', $row->id)->where('value', 1)->get()->count();
            $downvotes = \App\MemeVote::where('meme_id', $row->id)->where('value', 0)->get()->count();
            $totalComments = \App\Comment::where('meme_id', $row->id)->get()->count();
            ?>
            <div class="card meme-detail col-md-11 col-sm-12 border-0 main-theme-color mn-w-40">
                <div class="card-header navbar-black">
                    <small class="recent-meme-meta">
                        <a href="{{ url('u'.'/'.$row->user->name.'/'.$row->user->id) }}">
                            @if(@$row->user->avatar !== null)
                                <img class="poster-icon mr-1 img-fluid w-30"
                                     src="{{ asset ('uploads/users/'.@$row->user->avatar) }}">
                            @else
                                <img class="poster-icon mr-1 img-fluid w-30"
                                     src="{{ asset ('images/random_profile_pic.jpg') }}">
                            @endif
                            <b><a href="{{url( $row->slug.'/'.$row->uuid )}}" class="clr-wht meme-heading"
                              target="_blank">{{$row->title}}</a></b>
                              <br>


                        <button type="button" class="btn btn-sm clr-wht float-right butn-color-black">
                            <i class="fa fa-eye"></i>
                        <b class="clr-wht font-inherit">views {{$row->view_count}}</b></button>

                            <b class="navbar-color"
                               style="font-size: 11px;font-weight: initial">{{@$row->user->name}}</b>
                        </a>
                        <b class="navbar-color" style="font-size: 9px;font-weight: initial">
                            - {{$row->created_at->diffForHumans(null,true)}}</b>

                    </small>
                    <div>
                    </div>
                    <small>
                        <i class="fa fa-tag navbar-color font-15"></i>&nbsp;
                        <b class="navbar-color font-11 font-inherit">{{$row->tags}}</b>
                    </small>
                </div>
                <a href="{{url( $row->slug.'/'.$row->uuid )}}" target="_blank">
                    @if($row->file_description == 'upload from pc')
                        @if(@pathinfo(asset ('storage/uploads/thumbnails/'.$row->file), PATHINFO_EXTENSION) == 'gif')
                            <img class="card-img-top border-radius-0" src="{{ asset ('storage/uploads/thumbnails/'.$row->file)}}"
                                 alt="{{$row->title}}" onerror="this.onerror=null;this.src='{{ asset ('images/notfound.jpg')}}';">
                        @else
                            <img class="card-img-top border-radius-0" src="{{ asset ('storage/uploads/memes/'.$row->file) }}"
                                 alt="{{$row->title}}" onerror="this.onerror=null;this.src='{{ asset ('images/notfound.jpg')}}';">
                        @endif
                    @elseif($row->file_description == 'from URL')
                        <img class="card-img-top border-radius-0" src="{{ url ($row->file) }}"
                             onerror="this.onerror=null;this.src='{{ asset ('images/notfound.jpg')}}';" alt="">
                    @endif
                </a>

                <div class="card-block navbar-black ht-50">
                    <div class="card-text m-l-10 m-t-10">
                        <div class="row m-l-5">
                            @if(Auth::check())
                                <?php $vote = Auth::user()->votes()->where('meme_id', $row->id)->first() ?>
                            @endif
                            @if(isset($vote))
                                @if($vote->value == 1)
                                    <button type="button" class="upvote vote btn btn-sm clr-wht like" data-islike="1"
                                            data-value="1" id="upvote_{{$row->id}}" data-id="{{ $row->id }}"
                                            data-url="{{url('storeMemeVotes')}}"
                                            style="background: #cbb6b64d">
                                        <i class="fas fa-thumbs-up"></i>
                                    </button>
                                @else
                                    <button type="button" class="upvote vote btn btn-sm clr-wht like"
                                            id="upvote_{{$row->id}}" data-value="1" data-islike="0"
                                            data-id="{{ $row->id }}"
                                            data-url="{{url('storeMemeVotes')}}">
                                        <i class="fas fa-thumbs-up"></i>
                                    </button>
                                @endif
                            @else
                                <button type="button" id="upvote_{{$row->id}}"
                                        class="upvote vote btn btn-sm clr-wht like"
                                        data-value="1" data-id="{{ $row->id }}" data-islike="0"
                                        data-url="{{url('storeMemeVotes')}}">
                                    <i class="fas fa-thumbs-up"></i>
                                </button>
                            @endif

                            <b class="clr-wht m-t-3 m-l-5" id="upCount_{{$row->id}}"
                               data-value="{{$upvotes}}">{{$upvotes ?$upvotes : 0}}</b>

                            @if(isset($vote))
                                @if($vote->value == 0)
                                    <button type="button" class="downvote vote btn btn-sm clr-wht dislike m-l-5"
                                            id="downvote_{{$row->id}}" data-value="0" data-isdislike="1"
                                            data-id="{{ $row->id }}"
                                            data-url="{{url('storeMemeVotes')}}" style="background: #cbb6b64d">
                                        <i class="fas fa-thumbs-down"></i>
                                    </button>
                                @else
                                    <button type="button" class="downvote vote btn btn-sm clr-wht dislike m-l-5"
                                            id="downvote_{{$row->id}}" data-value="0" data-isdislike="0"
                                            data-id="{{ $row->id }}"
                                            data-url="{{url('storeMemeVotes')}}">
                                        <i class="fas fa-thumbs-down"></i>
                                    </button>
                                @endif
                            @else
                                <button type="button" class="downvote vote btn btn-sm clr-wht dislike m-l-5"
                                        data-isdislike="0" data-value="0" id="downvote_{{$row->id}}"
                                        data-id="{{ $row->id }}"
                                        data-url="{{url('storeMemeVotes')}}">
                                    <i class="fas fa-thumbs-down"></i>
                                </button>
                            @endif

                            <b class="clr-wht m-l-5 m-t-3" id="downCount_{{ $row->id }}"
                               data-value="{{$downvotes}}">{{$downvotes ? $downvotes : 0}}</b>

                            <a type="button" href="{{url ($row->slug.'/'.$row->uuid)}}#comments" class="btn btn-sm
                        clr-wht m-l-5 navbar-black">
                                <i class="fa fa-comment"></i>
                            </a>
                            <b class="clr-wht m-t-3 m-l-5"> {{$totalComments}}</b>

                            <a type="button" class="btn btn-sm clr-wht s-btn m-l-5 navbar-black" id="notifsDropdown"
                               data-toggle="dropdown"
                               aria-haspopup="true" aria-expanded="false" v-pre>
                                <i class="fa fa-share"></i>
                            </a>
                            <b class="clr-wht m-t-3 m-l-5">{{$row->share_count}}</b>

                            <div class="dropdown-menu dropdown-menu share" aria-labelledby="notifsDropdown">
                                <a class="dropdown-item clr-wht share-menu share-btn"
                                   data-url="{{url('MemeShareCount')}}"
                                   data-id="{{$row->id}}"
                                   href="https://www.facebook.com/sharer/sharer.php?u=https%3A%2F%2Flolwhoa.com%2F<?php echo $row->slug . '/' . $row->uuid;?>&amp;src=sdkpreparse"
                                   target="_blank">
                                    <i class="fa fa-facebook icon-size mr-2"></i> Facebook
                                </a>
                                <a class="dropdown-item clr-wht share-menu share-btn"
                                   data-url="{{url('MemeShareCount')}}"
                                   data-id="{{$row->id}}"
                                   href="https://twitter.com/intent/tweet?url=https%3A%2F%2Flolwhoa.com%2F<?php echo
                                       $row->slug . '/' . $row->uuid?>&layout=button_count&size=small&appId=313767242902864&width=70&height=20"
                                   target="_blank">
                                    <i class="fa fa-twitter icon-size mr-2"></i> Twitter
                                </a>

                                <a class="dropdown-item clr-wht share-menu share-btn"
                                   data-url="{{url('MemeShareCount')}}"
                                   data-id="{{$row->id}}"
                                   href="http://www.linkedin.com/shareArticle?mini=true&amp;url=https%3A%2F%2Flolwhoa.com%2F<?php echo $row->slug . '/' . $row->uuid;
                                   ?>&layout=button_count&size=small&appId=313767242902864&width=70&height=20"
                                   target="_blank"><span
                                        class="fa fa-linkedin icon-size mr-2"></span> Linkedin</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        @endforeach
    @endif

</div>
{{--<div class="ajax-load text-center" style="display:none">
    <p class="clr-wht"><img src="https://www.jfklibrary.org/themes/custom/jfkl/images/spinner.gif"></p>
</div>--}}
@if(Auth::check())
    <div id="AuthCheck" style="display: none">{{Auth::user()->id}}</div>
@endif


