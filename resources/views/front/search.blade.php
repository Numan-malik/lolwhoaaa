@extends('front.main')
@section('top-title','| Search')
@section('content')
    <div class="row" style="margin: 0px">
        <div class="col-12">
            <div class="form-group">
                <form action="{{route('search')}}" method="post">
                    @csrf
                    <input type="text" class="form-control main-theme-color clr-wht" name="search"  id="filter" placeholder="Type to search..." value="{{$search}}" autocomplete="off"
                           style="border-color: #545454!important;">
                    <i id="filtersubmit" class="fa fa-search"></i>
                </form>
            </div>
        </div>
    </div>
    <div class="col-12 m-b-20">
        @foreach($tags as $t)
            <div style="display: contents;">
                <a href="{{$t->slug}}" class="navbar-bg navbar-color" style="padding: 5px">{{$t->name}}</a>
            </div>
        @endforeach
    </div>
    @if(count($data) > 0)
        @include('front.memes.master-meme',['memes' => $data])
    @else
        <div class="error-template">
            <h2 class="navbar-color">Sorry ! No posts found.</h2>
        </div>
    @endif
@endsection

