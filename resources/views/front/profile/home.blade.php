@extends('front.profile.profile')

@section('profile-content')
    @if(count($home)>0)
        @include('front.memes.master-meme',['memes' => $home])
    @else
        <div class="m-150">
            <h3 class="navbar-color">User have no any Activity.</h3>
            <a type="button" class="btn next clr-wht m-t-20" href="{{url('/')}}" style="margin-right: 70px !important;
            width:130px !important;">See new posts
            </a>
        </div>
    @endif
@endsection
