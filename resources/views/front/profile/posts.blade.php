@extends('front.profile.profile')
@section('top-title','| '.$user['name'])
@section('profile-content')
    @if(count($posts)>0)
        @include('front.memes.master-meme',['memes' => $posts])
    @else
     <div class="m-150">
         <h3 class="navbar-color">User have no any posts.</h3>
         <a type="button" class="btn next clr-wht m-t-20" data-toggle="modal" data-target="#postaMeme"
            style="margin-right:55px !important;
         width: 130px !important;">Make new post
         </a>
     </div>
    @endif
@endsection
