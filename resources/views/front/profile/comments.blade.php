@extends('front.profile.profile')
@section('profile-content')
    @if(count(@$CommentedMeme) > 0)
     @include('front.memes.master-meme',['memes' => $CommentedMeme])
    @else
        <div class="m-150">
        <h3 class="navbar-color">Commented Posts will show here</h3>
        <a type="button" class="btn next clr-wht m-t-20" href="{{url('/')}}"
           style="margin-right:125px !important;
         width: 150px !important;">See Latest
        </a>
        </div>
    @endif
@endsection
