<div class="modal fade" id="login" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content main-theme-color">
            <div>
                <button type="button" class="close text-red float-right m-r-10" data-dismiss="modal">&times;</button>
            </div>
            <div id="login-err"></div>
         {{--   @if(Session::has('login-error'))
                <div class="alert alert-danger alert-dismissible m-15">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    <strong>{!! Session('login-error') !!}</strong>
                </div>
            @section('js')
                <script>
                    $(document).ready(function () {
                        $('#login').modal('show');
                    });

                </script>
            @endsection
            @endif--}}

            <form action="{{url ('login') }}" method="post" class="form-horizontal form-height" id="js-loginForm"
                  data-url="{{route ('login') }}">
                {{ csrf_field() }}
                <div>
                    <h2 class="clr-wht m-l-20">Log in</h2>
                    <small class="m-l-20 clr-wht">it's great to see you back!</small>
                </div>
                <div class="form-group m-b-0">
                    <div class="input-group">
                        <input type="email" class="form-control m-15 ht-50" name="email" placeholder="Email Address" required="required" id="Emailvalidate">
                    </div>
                    <span class="text-red m-l-15" id="validMsg"></span>
                </div>

                <div class="form-group m-b-0">
                    <div class="input-group">
                        <input type="password" class="form-control m-15 ht-50" name="password" placeholder="Password" required="required" minlength="8" maxlength="20">
                    </div>

                </div>
                {{--  <div class="clearfix">
                      <label class="pull-left checkbox-inline m-15"><input type="checkbox"> Remember
                          me</label>
                  </div>--}}
                @if (Route::has('password.request'))
                    <div class="hint-text small text-center m-t-20">
                        <a href="{{ route('password.request') }}" class="clr-wht text-center">
                            <u>{{ __('Forgot Your Password?') }}</u>
                        </a>
                    </div>
                @endif
                <div class="form-group m-15">
                    <button type="submit" name="loginForm" class="btn btn-success btn-block ht-50 btn-login" id="js-login-btn">Log in</button>
                </div>

                <b class="clr-wht font-11 font-inherit m-15">Don't have an account ?</b>
                <a href="javascript:;" data-toggle="modal" data-target="#register" class="clr-green font-12 m-l--10">Sign up now </a>

                <div class="clr-wht text-center">
                    Or
                </div>

                <div class="form-group m-15 mb-text-center">
                    <a href="{{route('auth','facebook')}}" class="btn btn-primary fb-social float-left ht-50 width-45
                    p-12" id="mb-login-facebook">
                        <i class="fa fa-facebook-square font-15"></i>
                        Login With Facebook
                    </a>
                </div>

                <div class="form-group m-15 mb-text-center">
                    <a href="{{route('auth','twitter')}}" class="btn btn-primary twitter-social float-right ht-50
                    width-45 p-12"
                       id="mb-login-twitter">
                        <i class="fa fa-twitter font-15"></i>
                        Login With Twitter
                    </a>
                    {{--  <a href="{{route('auth','google')}}" class="btn btn-danger"
                         style="margin-bottom:20px !important;"><i class="fa
                               fa-google"></i> Sign in
                          with <b>Google</b></a>
                      <a href="{{route('auth','linkedin')}}" class="btn btn-primary m-t-10 " style="background:
                               #0077B5;m-15-bottom: 30px"><i
                              class="fa fa-linkedin"></i> Sign
                          in with
                          <b>Linkedin</b></a>--}}

                </div>

            </form>

        </div>
    </div>
</div>

