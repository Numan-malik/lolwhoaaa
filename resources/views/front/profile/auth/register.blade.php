<div class="modal fade" id="register" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content main-theme-color">
            <div>
                <button type="button" class="close text-red float-right m-r-10" data-dismiss="modal">&times;</button>
            </div>
            <div id="register-error"></div>
            <form action="{{url ('signup') }}" method="post" class="form-horizontal form-height-register"
                  id="js-signUpForm" data-url="{{route('signup')}}">
                {{ csrf_field() }}
                <div>
                    <h2 class="clr-wht m-l-20">Register</h2>
                    <small class="m-l-20 clr-wht">Sign up using your email address</small>
                </div>
                <div class="form-group ">
                    <div class="input-group">
                        <input type="text" class="form-control m-15 ht-50" onkeypress="return aplhaOnly(this,event)" maxlength="30" name="name" placeholder="Name"/>
                    </div>
                </div>
                <div class="form-group {{ $errors->has('email') ? 'has-error' : ''}}">
                    <div class="input-group">
                        <input type="email" class="m-15 ht-50 form-control @error('email') is-invalid @enderror" value="{{ old('email')}}" name="email" placeholder="Email"
                               id="RegisterEmail" required/>
                    </div>
                  {{--  @error('email')
                    @section('js')
                        <script>
                            $(document).ready(function () {
                                $('#register').modal('show');
                            });
                        </script>
                    @endsection
                    <strong>{!! $errors->first('email', '<p class="help-block text-red m-l-15">:message</p>') !!}</strong>
                    @enderror--}}

                </div>
                <div class="form-group">
                    <div class="input-group">
                        <input type="password" class="form-control m-15 ht-50" name="password"
                               placeholder="Password" minlength="8" maxlength="20" required="required">
                    </div>
                </div>

                <div class="form-group m-15">
                    <input type="submit" value="Register" name="registerForm" class="btn btn-success btn-block ht-50
                    btn-login"/>
                </div>
                <b class="clr-wht font-11 font-inherit m-15">Already have an account ?</b>
                <a href="javascript:;" data-toggle="modal" data-target="#login" class="clr-green font-12 m-l--10">Sign in now </a>
                <div class="clr-wht text-center">
                    Or
                </div>

                <div class="form-group m-15 mb-text-center">
                    <a href="{{route('auth','facebook')}}" class="btn btn-primary fb-social float-left ht-50 width-45
                     p-12" id="mb-login-facebook">
                        <i class="fa fa-facebook-square"></i>
                        Register using Facebook
                    </a>
                </div>
                {{-- <a href="{{route('auth','google')}}" class="btn btn-danger"
                        style="margin-bottom:20px !important;"><i class="fa
                              fa-google"></i> Sign in
                         with <b>Google</b></a>
                     <a href="{{route('auth','linkedin')}}" class="btn btn-primary m-t-10 " style="background:
                              #0077B5;margin-bottom: 30px"><i
                             class="fa fa-linkedin"></i> Sign
                         in with
                         <b>Linkedin</b></a>--}}
                <div class="form-group m-15 mb-text-center">
                <a href="{{route('auth','twitter')}}" class="btn btn-primary twitter-social float-right ht-50 width-45
                    p-12" id="mb-login-twitter">
                    <i class="fa fa-twitter"></i>
                    Register using Twitter
                </a>
                </div>
            </form>

        </div>
    </div>
</div>
