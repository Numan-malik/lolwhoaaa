@extends('front.main')
@section('top-title','| '.$user['name'])
@section('content')

    <div class="container">
        @if(@$user['avatar'] == null)
            <img src="{{ url('/images/random_profile_pic.jpg') }}" alt="{{ @$user['name'] }}" class="img-profile mr-1">
        @else
            <img src="{{ asset('uploads/users/'.@$user['avatar']) }}" alt="{{ @$user['name'] }}" class="img-profile
            mr-1">
        @endif

        <a href="{{ url('u'.'/'.@$user['name'].'/'.@$user['id']) }}">
            <b class="clr-wht font-inherit">{{@$user['name']}}</b>
        </a>

        @if(@$user['id'] == @Auth::user()->id)
            <a type="button" href="{{route('setting')}}#profile" class="btn btn-success next m-t-10">Edit Profile</a>
        @endif

        <ul id="myTab" class="nav nav-tabs m-t-20" role="tablist"
            style="border-bottom: 1px solid #FFFFFF29 !important">

            <li class="nav-item width-15 text-center mb-w-20">
                <a class="nav-link clr-wht" href="{{ url('u'.'/'.@$user['name'].'/'.@$user['id']) }}"
                   aria-controls="mentions">Home</a>
            </li>
            <li class="nav-item width-15 text-center mb-w-20 mb-w-20">
                <a class="nav-link clr-wht" href="{{ url('u'.'/'.@$user['name'].'/'.@$user['id'].'/'.'posts' ) }}"
                   aria-controls="posts" aria-expanded="true">Posts</a>
            </li>
            @if(@$user['id'] == @Auth::user()->id)
            <li class="nav-item width-15 text-center mb-w-20">
                <a class="nav-link clr-wht" href="{{ url('u'.'/'.@$user['name'].'/'.@$user['id'].'/'.'upvotes' ) }}"
                   aria-controls="hats">Upvotes</a>
            </li>
            @endif
            <li class="nav-item width-15 text-center mb-w-20">
                <a class="nav-link clr-wht" href="{{url('u'.'/'.@$user['name'].'/'.@$user['id'].'/'.'comments' ) }}"
                   aria-controls="comments">Comments</a>
            </li>

        </ul>

        <!-- Content Panel -->
        <div id="navlist" class="tab-content row">         
 <div class="col" id="macy-container">
       @yield('profile-content')
</div>
        </div>
    </div>

@endsection
