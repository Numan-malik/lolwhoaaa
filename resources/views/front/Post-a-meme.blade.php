@error('title')
<strong>{!! $errors->first('title', ':message') !!}</strong>
<script>
    $(document).ready(function () {
        $('#postaMeme').modal('show');
    });
</script>
@enderror
<div class="modal fade" id="postaMeme" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content main-theme-color">
            <div>
                <button type="button" class="close text-red float-right m-r-10" data-dismiss="modal">&times;</button>
            </div>
            <div id="beforeTitle">
                <h1 class="clr-wht m-l-20">Post a meme</h1>
                <small class="m-l-20 clr-wht">Choose how you want to upload your meme</small>
            </div>
            <div id="afterTitle">
                <h1 class="clr-wht m-l-20">Enter the details</h1>
                <small class="m-l-20 clr-wht">Give an accurate description of your post.
                </small>
            </div>

            <div class="">
                <form method="post" action="{{route('MemeStore')}}" enctype="multipart/form-data">
                    @csrf
                    <div id="beforePostMeme">
                        <div class="form-group files color m-30 card image-meme-upload navbar-black text-center center-block" style="height: 120px">
                             <label for="file">
                                    <img src="{{asset('images/img.png')}}" class="img-fluid"
                                         width="60px" style="margin-top: 10px" />
                                </label>
                            <input type="file" name="img" id="file" accept="image/x-png,image/gif,image/jpeg" class="form-control memeImage" style="opacity: 0.25;">
                            <div><h6 class="clr-wht text-center m-t-10 mb-font">Upload Image from here...</h6></div>
                        </div>
                        <div class="row m-l-0">
                            <div class="card m-30 upload-meme navbar-black w-37" id="uploadURl">
                                <div>
                                    <img src="{{asset('images/img.png')}}" class="img-fluid upload-img-modal"
                                         width="40px">
                                </div>
                                <div>
                                    <h6 class="clr-wht text-center m-t-10 mb-font">Upload from URL</h6>
                                </div>
                            </div>

                            <div class="card m-30 upload-meme navbar-black w-37">
                                <a href="{{route('MemeGallery')}}">
                                    <div>
                                        <img class="img-fluid upload-img-modal" src="{{asset('images/memeicon.png')}}" width="40px">
                                    </div>
                                    <div>
                                        <h6 class="clr-wht text-center m-t-10 mb-font">Create a meme
                                           
                                        </h6>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>

                    <div id="afterPostMeme">
                        <div class="form-group color m-20 mb-w">
                            <img id="myImg" src="" alt="" class="img-fluid"/>
                            <p id="img-err" class="text-red"></p>
                        </div>

                        <div class="m-20">
                            <div class="form-group">
                                <label class="control-label clr-wht">Title</label>
                                <input type="text" class="form-control" name="title"
                                       placeholder="e.g. it really do be like that" maxlength="50" required/>
                            </div>
                            <div class="form-group">
                                <label class="control-label clr-wht">Tags</label><br>
                                <input id="tags" class="form-control" data-role="tagsinput" type="text"
                                       name="tags" placeholder="tags 1 , tags 2 , tags 3" required="required">
                            </div>
                            <div class="form-group">
                                <label for="multiple" class="control-label clr-wht">Categories</label><br>
                                <select name="category" data-placeholder="Choose a Category..." class="form-control"
                                        required>
                                    <option value=""></option>
                                    @foreach(@categoryForMemeDetails() as $cat)
                                        <option value="{{$cat->id}}">{{$cat->name}}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="form-group">
                                <button type="submit" class="butn btn-primary btn-width butn-color-black next m-b-20
                                m-l-10" name="submitPost">
                                    Post
                                </button>
                                <button type="button" href="javascript:void(0)" class='butn btn-primary btn-width butn-color-black
                                next m-b-20 GetMasterMeme'>Back
                                </button>
                            </div>
                        </div>
                    </div>

                </form>
                <form method="post" action="{{route('MemeStore')}}" enctype="multipart/form-data">
                    @csrf
                    <div id="UrlContent">
                        <div>
                            <h1 class="clr-wht m-l-20">Upload from URL</h1>
                            <small class="m-l-20 clr-wht">Type or paste the image URL, PNG, GIF, JPEG allowed .</small>
                        </div>
                        <div class="m-20">
                            <h6 class="clr-wht">Image URL</h6>
                            <div class="form-group">
                                <input type="text" class="form-control" id="imglink" pattern="https?://.+" name="imgUrl"
                                       placeholder="https://" required/>
                            </div>
                            <span class="validUrlMsg text-red">Please enter valid url !</span>

                            <div class="form-group mb-w">
                                <img id="ImgURL" src="" alt="" class="img-fluid"/>
                            </div>
                            <p id="img-err" class="text-red"></p>
                        </div>
                        <div class="m-20">
                            <div class="form-group">
                                <label class="control-label clr-wht">Title</label>
                                <input type="text" class="form-control" name="title"
                                       placeholder="e.g. it really do be like that" maxlength="50" required/>
                            </div>
                            <div class="form-group">
                                <label for="multiple" class="control-label clr-wht">Tags</label><br>
                                <input id="tags" class="form-control" data-role="tagsinput" type="text" required="required"
                                       name="tags" placeholder="tags 1 , tags 2 , tags 3">
                            </div>
                            <div class="form-group">
                                <label for="multiple" class="control-label clr-wht">Categories</label><br>
                                <select name="category" data-placeholder="Choose a Category..." class="form-control"
                                        required>
                                    <option value=""></option>
                                    @foreach(@categoryForMemeDetails() as $cat)
                                        <option value="{{$cat->id}}">{{$cat->name}}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="form-group">
                                <button type="submit" class="butn btn-primary btn-width butn-color-black next m-l-10 m-b-20"
                                        name="submitPost">Post
                                </button>
                                <button type="button" href="javascript:void(0)"
                                        class='butn btn-primary btn-width butn-color-black next m-b-20 GetMasterMeme'>Back
                                </button>

                            </div>
                        </div>
                    </div>
                </form>

            </div>
        </div>
    </div>
</div>
