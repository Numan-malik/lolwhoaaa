@extends('front.main')
@section('top-title','| Setting')

@section('content')
    <div class="row">
        <div class="col">
            <h5 class="clr-wht font-h5">Settings</h5>
            <hr class="hr-bg" style="margin-bottom: 20px;">
            <div id="msg"></div>
        </div>
    </div>
    
    <div class="row justify-content-center">
        <div class="col-10">
            <h5 class="clr-wht font-h5 marg-60">Account Settings</h5>
            <hr class="hr-bg" style="margin-top: 0px;">
            <div id="msg"></div>
        </div>
    </div>

    <?php $user = Auth::user(); ?>
    <form method="post" action="{{route('update-setting',$user->id)}}" id="setting-form"
          data-url="{{route('update-setting',$user->id)}}"
          data-id="{{$user->id}}">
        @csrf
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="formGroupExampleInput" class="clr-wht">Name</label>
                    <input type="text" name="name" class="form-control" placeholder="Please enter name"
                           value="{{$user->name}}"
                           id="name" onkeypress="return aplhaOnly(this,event)" maxlength="30">
                    <small class="navbar-color font-italic">No special characters, numbers are allowed</small>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="email" class="clr-wht">Email</label>
                    <input type="email" name="email" class="form-control email" id="Emailvalidate"
                           value="{{$user->email}}"
                           placeholder="Please enter email">
                    <small class="navbar-color font-italic">Make sure to use an email address that is active.</small>
                </div>

            </div>

            <!--

            <div class="col-md-12">
                <div class="form-group">
                    <button type="submit" name="settingForm"
                            class="btn btn-primary btn-width navbar-bg float-right m-r-0">Save
                    </button>
                </div>
            </div> -->
        </div>
    </form>

      <form method="post" action="{{route('update-password',$user->id)}}" id="js-profile-password"
          data-url="{{route('update-password',$user->id)}}"
          data-id="{{$user->id}}">
        @csrf

        <div class="row justify-content-center">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="" class="clr-wht">Old Password</label>
                    <input type="password" name="old_password" id="old_password" class="form-control"
                           placeholder="Enter your old password" minlength="8" maxlength="20">
                    <span class="text-danger">{{ $errors->first('old_password') }}</span>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="formGroupExampleInput" class="clr-wht">New Password</label>
                    <input type="password" name="password" class="form-control" id="new_password"
                           placeholder="Enter your new password" minlength="8" maxlength="20">
                    <span class="text-danger">{{ $errors->first('password') }}</span>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="formGroupExampleInput" class="clr-wht">Confirm New Password</label>
                    <input type="password" name="password_confirmation" class="form-control" id="password_confirmation"
                           placeholder="Confirm your new password" autocomplete="password" minlength="8" maxlength="20">
                    <span class="text-danger">{{ $errors->first('password_confirmation') }}</span>
                </div>
            </div>
            <div class="col-md-6 col-xs-6 center-block">
                <div class="form-group">
                    <button type="submit" id="send_form" class="send-butn butn next clr-wht marg-30">
                        Save
                    </button>
                </div>
            </div>
        </div>
    </form>

     <div class="row justify-content-center">
        <div class="col-10">
            <h5 class="clr-wht font-h5 marg-60">Profile Settings</h5>
            <hr class="hr-bg" style="margin-top: 0px;">
        </div>
    </div>

    <form method="post" action="{{route('update-profile',$user->id)}}" enctype="multipart/form-data" id="js-profile"
          data-url="{{route('update-profile',$user->id)}}"
          data-id="{{$user->id}}">
        @csrf

        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="formGroupExampleInput" class="clr-wht">Full Name</label>
                    <input type="text" name="full_name" class="form-control" id="UserfullName"
                           placeholder="Please enter full name"
                           value="{{$user->full_name}}" onkeypress="return aplhaOnly(this,event)" maxlength="30">
                    <small class="navbar-color font-italic">No special characters, numbers are allowed</small>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="gender" class="clr-wht">Gender</label>
                    <select name="gender" class="form-control" id="gender">
                        @if($user->gender != null)
                            <option value="male" @if($user->gender == 'male') {{'selected'}} @endif>Male</option>
                            <option value="female" @if($user->gender == 'female') {{'selected'}} @endif>Female</option>
                        @else
                            <option value="male">Male</option>
                            <option value="female">Female</option>
                        @endif
                    </select>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="birthday" class="clr-wht">Birthday</label>
                    <div class="col-md-12">
                        <div class="row">
                            <input name="day" id="day" class="date form-control w-20" placeholder="DD" maxlength="2"
                                   minlength="2"
                                   @if($user->birthday !=='') value="{{@$user->birthday[3]
                                   .@$user->birthday[4]}}"@endif>
                            <input name="month" id="month" class="date form-control w-20 m-l-10" placeholder="MM"
                                   maxlength="2" minlength="2"
                                   @if($user->birthday !=='') value="{{@$user->birthday[0]
                                   .@$user->birthday[1]}}"@endif>
                            <input name="year" id="year" class="date form-control w-20 m-l-10" placeholder="YYYY"
                                   maxlength="4" minlength="4"
                                   @if($user->birthday !=='') value="{{@$user->birthday[6].@$user->birthday[7]
                                   .@$user->birthday[8].@$user->birthday[9]}}" @endif>
                        </div>
                    </div>
                    <small class="navbar-color font-italic">In DD-MM-YYYY format.</small>
                    <p id="date-msg" class="text-red"></p>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="country" class="clr-wht">Country</label>
                    <select name="country" class="form-control" id="country">
                        <option value="{{@$user->country}}" {{'selected'}}>{{@$user->country}}</option>
                        <option value="Afghanistan">Afghanistan</option>
                        <option value="Albania">Albania</option>
                        <option value="Algeria">Algeria</option>
                        <option value="American Samoa">American Samoa</option>
                        <option value="Andorra">Andorra</option>
                        <option value="Angola">Angola</option>
                        <option value="Anguilla">Anguilla</option>
                        <option value="Antarctica">Antarctica</option>
                        <option value="Antigua &amp; Barbuda">Antigua &amp; Barbuda</option>
                        <option value="Argentina">Argentina</option>
                        <option value="Armenia">Armenia</option>
                        <option value="Aruba">Aruba</option>
                        <option value="Australia">Australia</option>
                        <option value="Austria">Austria</option>
                        <option value="Azerbaijan">Azerbaijan</option>
                        <option value="Bahamas">Bahamas</option>
                        <option value="Bahrain">Bahrain</option>
                        <option value="Bangladesh">Bangladesh</option>
                        <option value="Barbados">Barbados</option>
                        <option value="Belarus">Belarus</option>
                        <option value="Belgium">Belgium</option>
                        <option value="Belize">Belize</option>
                        <option value="Benin">Benin</option>
                        <option value="Bermuda">Bermuda</option>
                        <option value="Bhutan">Bhutan</option>
                        <option value="Bolivia">Bolivia</option>
                        <option value="Bosnia &amp; Herzegovina">Bosnia &amp; Herzegovina</option>
                        <option value="Botswana">Botswana</option>
                        <option value="Bouvet Island">Bouvet Island</option>
                        <option value="Brazil">Brazil</option>
                        <option value="British Indian Ocean Territory">British Indian Ocean Territory</option>
                        <option value="British Virgin Islands">British Virgin Islands</option>
                        <option value="Brunei">Brunei</option>
                        <option value="Bulgaria">Bulgaria</option>
                        <option value="Burkina Faso">Burkina Faso</option>
                        <option value="Burundi">Burundi</option>
                        <option value="Cambodia">Cambodia</option>
                        <option value="Cameroon">Cameroon</option>
                        <option value="Canada">Canada</option>
                        <option value="Cape Verde">Cape Verde</option>
                        <option value="Caribbean Netherlands">Caribbean Netherlands</option>
                        <option value="Cayman Islands">Cayman Islands</option>
                        <option value="Central African Republic">Central African Republic</option>
                        <option value="Chad">Chad</option>
                        <option value="Chile">Chile</option>
                        <option value="China mainland">China mainland</option>
                        <option value="Christmas Island">Christmas Island</option>
                        <option value="Cocos (Keeling) Islands">Cocos (Keeling) Islands</option>
                        <option value="Colombia">Colombia</option>
                        <option value="Comoros">Comoros</option>
                        <option value="Cook Islands">Cook Islands</option>
                        <option value="Costa Rica">Costa Rica</option>
                        <option value="Côte d’Ivoire">Côte d’Ivoire</option>
                        <option value="Croatia">Croatia</option>
                        <option value="Curaçao">Curaçao</option>
                        <option value="Cyprus">Cyprus</option>
                        <option value="Czechia">Czechia</option>
                        <option value="Democratic Republic of the Congo">Democratic Republic of the Congo</option>
                        <option value="Denmark">Denmark</option>
                        <option value="Djibouti">Djibouti</option>
                        <option value="Dominica">Dominica</option>
                        <option value="Dominican Republic">Dominican Republic</option>
                        <option value="Ecuador">Ecuador</option>
                        <option value="Egypt">Egypt</option>
                        <option value="El Salvador">El Salvador</option>
                        <option value="Equatorial Guinea">Equatorial Guinea</option>
                        <option value="Eritrea">Eritrea</option>
                        <option value="Estonia">Estonia</option>
                        <option value="Ethiopia">Ethiopia</option>
                        <option value="Falkland Islands">Falkland Islands</option>
                        <option value="Faroe Islands">Faroe Islands</option>
                        <option value="Fiji">Fiji</option>
                        <option value="Finland">Finland</option>
                        <option value="France">France</option>
                        <option value="French Guiana">French Guiana</option>
                        <option value="French Polynesia">French Polynesia</option>
                        <option value="French Southern Territories">French Southern Territories</option>
                        <option value="Gabon">Gabon</option>
                        <option value="Gambia">Gambia</option>
                        <option value="Georgia">Georgia</option>
                        <option value="Germany">Germany</option>
                        <option value="Ghana">Ghana</option>
                        <option value="Gibraltar">Gibraltar</option>
                        <option value="Greece">Greece</option>
                        <option value="Greenland">Greenland</option>
                        <option value="Grenada">Grenada</option>
                        <option value="Guadeloupe">Guadeloupe</option>
                        <option value="Guam">Guam</option>
                        <option value="Guatemala">Guatemala</option>
                        <option value="Guernsey">Guernsey</option>
                        <option value="Guinea">Guinea</option>
                        <option value="Guinea-Bissau">Guinea-Bissau</option>
                        <option value="Guyana">Guyana</option>
                        <option value="Haiti">Haiti</option>
                        <option value="Heard &amp; Mc Donald Islands">Heard &amp; Mc Donald Islands</option>
                        <option value="Honduras">Honduras</option>
                        <option value="Hong Kong">Hong Kong</option>
                        <option value="Hungary">Hungary</option>
                        <option value="Iceland">Iceland</option>
                        <option value="India">India</option>
                        <option value="Indonesia">Indonesia</option>
                        <option value="Iraq">Iraq</option>
                        <option value="Ireland">Ireland</option>
                        <option value="Isle of Man">Isle of Man</option>
                        <option value="Israel">Israel</option>
                        <option value="Italy">Italy</option>
                        <option value="Jamaica">Jamaica</option>
                        <option value="Japan">Japan</option>
                        <option value="Jersey">Jersey</option>
                        <option value="Jordan">Jordan</option>
                        <option value="Kazakhstan">Kazakhstan</option>
                        <option value="Kenya">Kenya</option>
                        <option value="Kiribati">Kiribati</option>
                        <option value="Kosovo">Kosovo</option>
                        <option value="Kuwait">Kuwait</option>
                        <option value="Kyrgyzstan">Kyrgyzstan</option>
                        <option value="Laos">Laos</option>
                        <option value="Latvia">Latvia</option>
                        <option value="Lebanon">Lebanon</option>
                        <option value="Lesotho">Lesotho</option>
                        <option value="Liberia">Liberia</option>
                        <option value="Libya">Libya</option>
                        <option value="Liechtenstein">Liechtenstein</option>
                        <option value="Lithuania">Lithuania</option>
                        <option value="Luxembourg">Luxembourg</option>
                        <option value="Macao">Macao</option>
                        <option value="Madagascar">Madagascar</option>
                        <option value="Malawi">Malawi</option>
                        <option value="Malaysia">Malaysia</option>
                        <option value="Maldives">Maldives</option>
                        <option value="Mali">Mali</option>
                        <option value="Malta">Malta</option>
                        <option value="Marshall Islands">Marshall Islands</option>
                        <option value="Martinique">Martinique</option>
                        <option value="Mauritania">Mauritania</option>
                        <option value="Mauritius">Mauritius</option>
                        <option value="Mayotte">Mayotte</option>
                        <option value="Mexico">Mexico</option>
                        <option value="Micronesia">Micronesia</option>
                        <option value="Moldova">Moldova</option>
                        <option value="Monaco">Monaco</option>
                        <option value="Mongolia">Mongolia</option>
                        <option value="Montenegro">Montenegro</option>
                        <option value="Montserrat">Montserrat</option>
                        <option value="Morocco">Morocco</option>
                        <option value="Mozambique">Mozambique</option>
                        <option value="Myanmar">Myanmar</option>
                        <option value="Namibia">Namibia</option>
                        <option value="Nauru">Nauru</option>
                        <option value="Nepal">Nepal</option>
                        <option value="Netherlands">Netherlands</option>
                        <option value="Netherlands Antilles">Netherlands Antilles</option>
                        <option value="New Caledonia">New Caledonia</option>
                        <option value="New Zealand">New Zealand</option>
                        <option value="Nicaragua">Nicaragua</option>
                        <option value="Niger">Niger</option>
                        <option value="Nigeria">Nigeria</option>
                        <option value="Niue">Niue</option>
                        <option value="Norfolk Island">Norfolk Island</option>
                        <option value="Northern Mariana Islands">Northern Mariana Islands</option>
                        <option value="North Macedonia">North Macedonia</option>
                        <option value="Norway">Norway</option>
                        <option value="Oman">Oman</option>
                        <option value="Pakistan">Pakistan</option>
                        <option value="Palau">Palau</option>
                        <option value="Palestinian Territories">Palestinian Territories</option>
                        <option value="Panama">Panama</option>
                        <option value="Papua New Guinea">Papua New Guinea</option>
                        <option value="Paraguay">Paraguay</option>
                        <option value="Peru">Peru</option>
                        <option value="Philippines">Philippines</option>
                        <option value="Pitcairn Island">Pitcairn Island</option>
                        <option value="Poland">Poland</option>
                        <option value="Portugal">Portugal</option>
                        <option value="Puerto Rico">Puerto Rico</option>
                        <option value="Qatar">Qatar</option>
                        <option value="Republic of the Congo">Republic of the Congo</option>
                        <option value="Réunion">Réunion</option>
                        <option value="Romania">Romania</option>
                        <option value="Russia">Russia</option>
                        <option value="Rwanda">Rwanda</option>
                        <option value="Saint Kitts &amp; Nevis">Saint Kitts &amp; Nevis</option>
                        <option value="Samoa">Samoa</option>
                        <option value="San Marino">San Marino</option>
                        <option value="Sao Tome &amp; Principe">Sao Tome &amp; Principe</option>
                        <option value="Saudi Arabia">Saudi Arabia</option>
                        <option value="Senegal">Senegal</option>
                        <option value="Serbia">Serbia</option>
                        <option value="Seychelles">Seychelles</option>
                        <option value="Sierra Leone">Sierra Leone</option>
                        <option value="Singapore">Singapore</option>
                        <option value="Sint Maarten">Sint Maarten</option>
                        <option value="Slovakia">Slovakia</option>
                        <option value="Slovenia">Slovenia</option>
                        <option value="Solomon Islands">Solomon Islands</option>
                        <option value="Somalia">Somalia</option>
                        <option value="South Africa">South Africa</option>
                        <option value="South Georgia &amp; South Sandwich Islands">South Georgia &amp; South Sandwich
                            Islands
                        </option>
                        <option value="South Korea">South Korea</option>
                        <option value="Spain">Spain</option>
                        <option value="Sri Lanka">Sri Lanka</option>
                        <option value="St. Pierre &amp; Miquelon">St. Pierre &amp; Miquelon</option>
                        <option value="St Helena">St Helena</option>
                        <option value="St Lucia">St Lucia</option>
                        <option value="St Martin">St Martin</option>
                        <option value="St Vincent &amp; the Grenadines">St Vincent &amp; the Grenadines</option>
                        <option value="Sudan">Sudan</option>
                        <option value="Suriname">Suriname</option>
                        <option value="Svalbard &amp; Jan Mayen Islands">Svalbard &amp; Jan Mayen Islands</option>
                        <option value="Swaziland">Swaziland</option>
                        <option value="Sweden">Sweden</option>
                        <option value="Switzerland">Switzerland</option>
                        <option value="Taiwan">Taiwan</option>
                        <option value="Tajikistan">Tajikistan</option>
                        <option value="Tanzania">Tanzania</option>
                        <option value="Thailand">Thailand</option>
                        <option value="Timor-Leste">Timor-Leste</option>
                        <option value="Togo">Togo</option>
                        <option value="Tokelau">Tokelau</option>
                        <option value="Tonga">Tonga</option>
                        <option value="Trinidad &amp; Tobago">Trinidad &amp; Tobago</option>
                        <option value="Tunisia">Tunisia</option>
                        <option value="Turkey">Turkey</option>
                        <option value="Turkmenistan">Turkmenistan</option>
                        <option value="Turks &amp; Caicos Islands">Turks &amp; Caicos Islands</option>
                        <option value="Tuvalu">Tuvalu</option>
                        <option value="Uganda">Uganda</option>
                        <option value="Ukraine">Ukraine</option>
                        <option value="United Arab Emirates">United Arab Emirates</option>
                        <option value="United Kingdom">United Kingdom</option>
                        <option value="United States">United States</option>
                        <option value="United States Minor Outlying">United States Minor Outlying</option>
                        <option value="Uruguay">Uruguay</option>
                        <option value="Uzbekistan">Uzbekistan</option>
                        <option value="Vanuatu">Vanuatu</option>
                        <option value="Vatican City State">Vatican City State</option>
                        <option value="Venezuela">Venezuela</option>
                        <option value="Vietnam">Vietnam</option>
                        <option value="Virgin Islands (U.S.)">Virgin Islands (U.S.)</option>
                        <option value="Wallis &amp; Futuna Islands">Wallis &amp; Futuna Islands</option>
                        <option value="Western Sahara">Western Sahara</option>
                        <option value="Yemen">Yemen</option>
                        <option value="Zambia">Zambia</option>
                        <option value="Zimbabwe">Zimbabwe</option>
                        <option value="Others">Others</option>
                    </select>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="bio" class="clr-wht">Bio</label>
                    <textarea cols="4" class="form-control col-md-12 bio" name="bio">{{$user->bio}}</textarea>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group image-meme-upload">
                    <label for="avatar" class="clr-wht">Avatar</label><br>
                    @if($user->avatar == null)
                        <img src="{{asset('images/random_profile_pic.jpg')}}" id="ProfileImg"
                             style="border-radius: 10px;width: 60px">
                    @else
                        <img src="{{asset('uploads/users/'.$user->avatar)}}" id="ProfileImg"
                             style="border-radius: 10px;width: 60px"/>
                    @endif
                    {{-- <input type="file" name="avatar" class="clr-wht ProfileImg" accept="image/x-png,image/gif,image/jpeg"><br>--}}
                    
                        <label for="file" style="vertical-align: bottom;">
                                    <img src="{{asset('images/plus.png')}}" class="img-fluid"
                                         width="10px" style="margin-top: 10px" />
                        </label>
                        <input type="file" class="ProfileImg" name="avatar"
                               accept="image/x-png,image/gif,image/jpeg" id="file">
        
                   <!-- <small class="navbar-color font-italic">Only use PNG, GIF or JPEG.2MB max filesize.</small> -->
                    <!--<div class="image-meme-upload">
                        <label for="file">
                                    <img src="{{asset('images/plus.png')}}" class="img-fluid"
                                         width="10px" style="margin-top: 10px" />
                        </label>
                        <input type="file" class="form-control ProfileImg" name="avatar" style=""
                               accept="image/x-png,image/gif,image/jpeg">
                    </div>-->
                    <p id="img-err" class="text-red"></p>
                </div>
            </div>

            <div class="col-md-6">
                <div class="form-group">
                    <div id="profile-msg"></div>
                </div>
            </div>
            <div class="col-md-6 col-xs-6 center-block">
                <div class="form-group">
                    <button type="submit" id="send_form" class="send-butn butn next clr-wht" style="margin-top: 16px" name="profileSubmit"> Save
                    </button>
                </div>
            </div>

        </div>
    </form>

    <!-- <form method="post" action="{{route('update-password',$user->id)}}" id="js-profile-password"
          data-url="{{route('update-password',$user->id)}}"
          data-id="{{$user->id}}">
        @csrf

        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="" class="clr-wht">Old Password</label>
                    <input type="password" name="old_password" id="old_password" class="form-control"
                           placeholder="Enter your old password" minlength="8" maxlength="20">
                    <span class="text-danger">{{ $errors->first('old_password') }}</span>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="formGroupExampleInput" class="clr-wht">New Password</label>
                    <input type="password" name="password" class="form-control" id="new_password"
                           placeholder="Enter your new password" minlength="8" maxlength="20">
                    <span class="text-danger">{{ $errors->first('password') }}</span>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="formGroupExampleInput" class="clr-wht">Confirm New Password</label>
                    <input type="password" name="password_confirmation" class="form-control" id="password_confirmation"
                           placeholder="Confirm your new password" autocomplete="password" minlength="8" maxlength="20">
                    <span class="text-danger">{{ $errors->first('password_confirmation') }}</span>
                </div>
            </div>
            <div class="col-md-12">
                <div class="form-group">
                    <button type="submit" id="send_form" class="btn btn-primary btn-width navbar-bg float-right m-r-0">
                        Save
                    </button>
                </div>
            </div>
        </div>
    </form> -->

<!--
    <div class="row justify-content-center">
        <div class="col-10">
            <h5 class="clr-wht font-h5 marg-60">Theme Settings</h5>
            <hr class="hr-bg" style="margin-top: 0px;">
        </div>
    </div>
    <div class="row justify-content-center">
        <div class="col">
            <div class="theme-switch-wrapper">
                <span class="clr-wht" style="margin-right: 7px">Change Theme : </span>
                <label class="theme-switch" for="checkbox" style="margin-bottom: 0px">
                    <input type="checkbox" id="checkbox" />
                    <div class="tb round"></div>
                </label>
            </div>
            
        </div>
    </div> -->

@endsection

