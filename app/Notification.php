<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Notification extends Model
{
    protected $table = 'notifications';
    protected $fillable = ['id', 'user_id', 'comment_vote_id','comment_id','meme_vote_id','status','created_at','updated_at'];

    public function memeVote()
    {
        return $this->belongsTo(MemeVote::class);
    }

    public function CommentVote()
    {
        return $this->belongsTo(CommentVote::class);
    }

    public function comment()
    {
        return $this->belongsTo(Comment::class);
    }
    public function meme()
    {
        return $this->belongsTo(Meme::class);
    }
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
