<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;


class Meme extends Model
{
    use Notifiable;

    protected $table = 'memes';
    protected $fillable = ['id', 'file', 'title'];

    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function comments()
    {
        return $this->morphMany(Comment::class, 'meme')->whereNull('parent_id');
    }

    public function votes() {
        return $this->hasMany(MemeVote::class);
    }
    public function getLink()
    {
        return url($this->id);
    }

}
