<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class AdminRoute
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(Auth::check() AND Auth::user()->user_role=='admin')
        {
            return $next($request);
        }
        else
        {
            return redirect("/admin");
        }
//        return $next($request);
    }
}
