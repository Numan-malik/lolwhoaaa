<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use Mail;
use Illuminate\Support\Facades\Validator;

class ContactController extends Controller
{
    public function ContactUs()
    {
        return view('front.contact');
    }

    public function SendContactMsg(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|min:3',
            'UserEmail' => 'required|email',
            'message' => 'required|min:10'
        ]);
        if ($validator->fails())
        {
            return response()->json([
                "errors" => $validator->errors()->all(),
                "status" => 400
            ]);
        }
        else
            {
            $data = array(
                'email' => $request->UserEmail,
                'name' => $request->name,
                'bodyMessage' => $request->message
            );
            Mail::send('front.emails.contact', $data, function ($message) use ($data) {
                $message->from("contact@lolwhoa.com");
                $message->to("contact@lolwhoa.com");
                $message->subject("Message from Lolwhoa");
            });

            return response()->json([
                "success" => true,
                "status" => 200
            ]);

        }

    }
}
