<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Database\Eloquent\Model;
use Socialite;
use App\SocialProvider;
use Illuminate\Support\Str;

class ApiController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
        ]);
    }

    public function redirectToProvider($provider)
    {
        return Socialite::driver($provider)->redirect();
    }

    public function handleProviderCallback($provider)
    {
        try {
            $socialUser = Socialite::driver($provider)->user();
        } catch (\Exception $e) {
            return response()->json([
                "msg" => "Bad Request",
                "data" => $e,
            ]);
        }
        //check if we have logged provider
        $socialProvider = SocialProvider::where('provider_id', $socialUser->getId())->first();

        if (!$socialProvider) {
            //create a new user and provider
           // $img = 'random_profile_pic.jpg';
            if ($provider == 'twitter') {
                $user = User::firstOrCreate(
                    ['name' => $socialUser->getName()],
                    ['avatar' => $socialUser->getAvatar()]
                );
            } else {
                $user = User::firstOrCreate(
                    ['email' => $socialUser->getEmail()],
                    ['name' => $socialUser->getName()],
                    ['avatar' => $socialUser->getAvatar()]
                );
            }

            $social = new SocialProvider();
            $social->user_id = $user->id;
            $social->provider_id = $socialUser->getId();
            $social->provider = $provider;
            $social->save();

            auth()->login($user);
            return response()->json([
                "status" => 200,
                "msg" => "Register Successfully",
                "data" => $user,
            ]);

        } else {
            $user = $socialProvider->user;
            auth()->login($user);

            return response()->json([
                "status" => 200,
                "msg" => "User already Register",
                "data" => $user,
            ]);
        }

    }

}
