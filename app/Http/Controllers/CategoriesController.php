<?php

namespace App\Http\Controllers;

use App\Category;
use App\MemeCategoryFK;
use App\MemeVote;
use Illuminate\Http\Request;
use Auth;
use Illuminate\Support\Facades\Validator;
use App\Meme;
use App\Comment;
use Illuminate\Http\Response;
use View;

class CategoriesController extends Controller
{
    public function ShowCategory()
    {
        $categories = Category::where('status', 1)->get();

        return view('admin.category', compact('categories'));
    }

    public function StoreCategory(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|unique:categories'
        ]);
        if ($validator->fails()) {
            return redirect('/admin/category')
                ->withErrors($validator)
                ->withInput();
        }

        $category = new Category();
        if ($request->img) {
//            $image = time() . '.' . request()->img->getClientOriginalExtension();
//            request()->img->move(public_path('uploads/categoryImage'), $image);
            \Storage::disk('public')->makeDirectory('uploads/categoryImage');

            if (\File::exists(Storage_path('app/public/uploads/categoryImage/' . $request->img))) {
                \File::delete(Storage_path('app/public/uploads/thumbnails/' . $request->img));
            }

            $image = $request->file('img');
            $destinationPath = Storage_path('app/public/uploads/categoryImage');
            $img = Image::make($image->getRealPath());
            $img->orientate();
            $input['img'] = random_int(1, 99) . date('sihYdm') . random_int(1, 99) . '.' .
                $image->getClientOriginalExtension();

            $img->resize(200, null, function ($constraint) {
                $constraint->aspectRatio();
            })->save($destinationPath . '/' . $input['img']);

            $category->img = $input['img'];
        }
        $category->name = $request->name;
        $category->slug = str_slug($request->name);
        $category->status = 1;
        $category->user_id = Auth::user()->id;

        $category->save();

        return redirect('/admin/category')->with('msg', 'Category Save Successfully');
    }

    public function EditCategory(Request $request, $id)
    {

    }

    public function UpdateCategory(Request $request, $id)
    {
        if ($request->img) {
            $image = time() . '.' . request()->img->getClientOriginalExtension();
            request()->img->move(public_path('uploads/categoryImage'), $image);
        }

        $category = Category::find($id);
        $category->name = $request->name;
        $category->slug = str_slug($request->name);
        if ($request->img) {
            $category->img = $image;
        }
        $category->save();

        return redirect('/admin/category')->with('msg', 'Category Update Successfully');
    }

    public function DeleteCategory($id)
    {
        $category = Category::find($id);
        $category->delete();

        return redirect('/admin/category')->with('msg', 'Category Delete Successfully');
    }

    //front Category function

    public function getCategoryDetail(Request $request, $categorySlug)
    {
        if ($categorySlug) {
            $category = Category::where('slug', $categorySlug)->first();
            if($category)
            {
                $memes = Meme::where('category_id', $category->id)
                    ->latest()
                    ->orderBy('updated_at', 'desc')
                    ->get();

                return view('front/categories/category_details', compact('category', 'data', 'comment', 'memes'));
            }
            return redirect()->back();
        }

    }
}
