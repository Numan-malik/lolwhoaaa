<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Validator;
use Auth;
use Illuminate\Support\Facades\Hash;


class SettingController extends Controller
{
    public function setting()
    {
        return view('front.setting');
    }

    public function updateSetting(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|max:255',
            'email' => 'required|email'
        ]);
        if ($validator->fails()) {
            // return redirect()->back()->withErrors($validator);
//            $u = User::find($id);
//            if($u['name'] == $request->name && $u['email'] == $request->email)
//            {
//                return response()->json([
//                    "errors" => 'You have no any changes',
//                    "status" => 402
//                ]);
//            }
//            else{
//                return response()->json([
//                    "errors" => $validator->errors()->all(),
//                    "status" => 400
//                ]);
//            }
            return response()->json([
                "errors" => $validator->errors()->all(),
                "status" => 400
            ]);

        } else {
            $userEmail = User::where('id', '!=', 0)->pluck('email');
            foreach ($userEmail as $email) {
                if ($email !== $request->email) {
                    $user = User::find($id);
                    $user->name = str_replace(' ', ' ', ucwords(str_replace('', ' ', strtolower($request->name))));
                    $user->email = $request->email;
                    $user->save();
                    return response()->json([
                        "success" => true,
                        "status" => 200
                    ]);
                    //        return redirect('/setting')->with('success-msg', 'Setting update successfully !');
                } else {
                    return response()->json([
                        "errors" => ["This Email has already taken"],
                        "status" => 400
                    ]);
                    //return redirect('/setting')->with('error-msg', $validator);
                }
            }

        }

    }

    public function updateProfile(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'full_name' => 'required|string|max:255',
            'avatar' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',

        ]);
        if ($validator->fails()) {
            if ($request->year < 1900 || $request->year > 2009 || $request->month > 12 || $request->day > 31) {
                return response()->json([
                    "errors" => $validator->errors()->all(),
                    "status" => 400
                ]);
                // return redirect()->back()->withErrors($validator);
            } else {
                return response()->json([
                    "errors" => $validator->errors()->all(),
                    "status" => 400
                ]);
//                return redirect()->back()->withErrors($validator);
            }

        } else {

            $user = User::find($id);

            if ($request->avatar)
            {
                $img = time() . '.' . request()->avatar->getClientOriginalExtension();
                request()->avatar->move(public_path('uploads/users'), $img);

                $user->avatar = $img ? $img : null;
            }
            $user->full_name = str_replace('', '', ucwords(str_replace('', '', strtolower($request->full_name))));
            $user->gender = $request->gender;

            if ($request->day !== null && $request->month !== null && $request->year !== null) {
                $user->birthday = $request->day . '-' . $request->month . '-' . $request->year;
            }
            $user->country = $request->country;
            $user->bio = $request->bio;
            $user->save();

            return response()->json([
                "success" => true,
                "status" => 200
            ]);

            //return redirect('/setting')->with('success-msg', 'Profile update successfully !');

        }

    }

    public function updatePassword(Request $request, $id)
    {

        $validator = Validator::make($request->all(), [
            'password' => 'required|confirmed|min:8|max:20',
            'old_password' => 'required|min:8|max:20',
        ]);
        if ($validator->fails()) {
            //return redirect()->back()->withErrors($validator);
            return response()->json([
                "errors" => $validator->errors()->all(),
                "status" => 400
            ]);
        }
        else {
            $user = Auth::user();

            if (Hash::check($request->old_password, $user->password)) {
                if (!Hash::check($request->password, $user->password)) {
                    if ($request->password == $request->password_confirmation) {

                        $users = User::find(Auth::user()->id);
                        $users->password = bcrypt($request->password);
                        User::where('id', Auth::user()->id)->update(array('password' => $users->password));

                        //session()->flash('success-msg', 'New Password updated successfully !');
                        //return redirect()->back();
                        return response()->json([
                            "success" => true,
                            "status" => 200
                        ]);
                    } else {

                        //session()->flash('error-msg', 'Confirm password does not matched');
                        //return redirect()->back();
                        return response()->json([
                            "errors" => 'Confirm password does not matched',
                            "status" => 401
                        ]);
                    }

                } else {

                    //session()->flash('error-msg', 'New password can not be the old password!');
                    //return redirect()->back();
                    return response()->json([
                        "errors" => 'New password can not be the old password!',
                        "status" => 402
                    ]);
                }

            } else {

               // session()->flash('error-msg', 'Old password does not matched ');
                //return redirect()->back();
                return response()->json([
                    "errors" => 'Old password does not matched',
                    "status"=>403
                ]);
            }
        }

    }
}
