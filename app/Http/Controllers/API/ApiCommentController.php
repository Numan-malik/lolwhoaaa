<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Notification;
use App\CommentVote;
use App\Comment;
use App\Meme;
use Auth;
use Redirect, Response, DB, Config;
use App\User;
use Mail;

class ApiCommentController extends Controller
{
    public function StoreCommentVotes(Request $request)
    {
        $user = Auth::user();
        $old = CommentVote::where('user_id', $user->id)
            ->where('comment_id', $request->comment_id)
            ->first();
        if (!$old) {
            $c = Comment::where('id', $request->comment_id)->first();
            $meme = Meme::where('id', $c->meme_id)->first();

            $vote = new CommentVote();
            $vote->user_id = $user->id;
            $vote->comment_id = $request->comment_id;
            $vote->meme_id = $meme->id;
            $vote->value = $request->value;
            $vote->save();
            if ($request->value == 1) {

                $n = new Notification();
                $n->meme_id = $meme->id;
                $n->comment_id = $request->comment_id;
                $n->recieve_by = $c->user_id;
                $n->type = 'Like Your Comment';
                $n->user_id = $user->id; //sender
                $n->status = 1; //Notification Unseen
                $n->save();

                $RecieverUser = User::where('id', $c->user_id)->first();

                $cmntVote = CommentVote::where('comment_id', $request->comment_id)->where('value', 1)->get()->count();

                if ($cmntVote >= 2) {
                    $data['title'] = $user->name . ' ' . 'and' . ' ' . ($cmntVote - 1) . ' ' . 'others Like your comment.';
                } else {
                    $data['title'] = $user->name . ' ' . 'Like your comment.';
                }

                $data['action'] = url($meme->id . '/' . $meme->uuid);
                $data['receiver'] = $RecieverUser['name'];

                Mail::send('front.emails.notifyEmail', $data, function ($message) use ($RecieverUser, $meme) {

                    $message->to($RecieverUser['email'], Auth::user()->name)
                        ->subject('You have new notification on your post.');
                });

            }
            $updateStatus = Notification::where('meme_id', $meme->id)->where('type', 'Like Your Comment')->where('status', 0)
                ->update(array('status' => 1));

        } else {

            $vote = CommentVote::find($old->id);
            $memeId = Comment::where('id', $vote['comment_id'])->pluck('meme_id');

            if ($request->value == $old->value) {
                if ($request->value == 1) {

                    $n = Notification::where('user_id', $vote->user_id)->where('type', 'Like Your Comment')->whereIn('meme_id', $memeId)->first();
                    if ($n) {
                        $n->delete();
                    }

                }
                $vote->delete();
            } else {
                $vote->value = $request->value;
                $vote->save();
            }

        }
        return response()->json([
        	'status'=> true ,
        	'data'=> $vote,
        	'message'=> 'success',
        ]);
    }

    public function apiStore(Request $request)
    {
        $user = Auth::user();

        $comment = new Comment;
        $comment->body = $request->get('comment_body');
        $comment->user()->associate($request->user());
        $post = Meme::find($request->get('meme_id'));
        $post->comments()->save($comment);

        $cmnt = Comment::where('meme_id', $request->meme_id)->get();
        $meme = Meme::whereIn('id', $cmnt->pluck('meme_id'))->first();

        $n = Notification::where('meme_id', $request->meme_id)->where('user_id', $user->id)
            ->where('type', 'Comment on Your Post')
            ->delete(); //delete old notify if user again commented on same post
        if ($meme->user_id !== $user->id) {
            $n = new Notification();
            $n->meme_id = $meme->id;
            $n->comment_id = $comment->id;
            $n->user_id = $user->id;
            $n->recieve_by = $meme->user_id;
            $n->type = 'Comment on Your Post';
            $n->status = 1;
            $n->save();

            //for mail

            $c = Comment::selectRaw('meme_id,COUNT(meme_id) as total')
                ->selectRaw('user_id,COUNT(user_id) as oldUser')->where('meme_id', $meme->id)->where('user_id', '!=', $meme->user_id)
                ->groupBy('meme_id', 'user_id')->get()->count();

            $RecieverUser = User::where('id', $meme->user_id)->first();

            if ($c >= 2) {

                if (@$c->oldUser > 1) {
                    $data['title'] = $user->name . ' ' . 'commented on your post.';
                } else {
                    $data['title'] = $user->name . ' ' . 'and' . ' ' . ($c - 1) . ' ' . 'others commented on your post.';

                }

            } else {
                $data['title'] = $user->name . ' ' . 'commented on your post.';
            }

            $data['action'] = url($meme->id . '/' . $meme->uuid);
            $data['receiver'] = $RecieverUser['name'];

            Mail::send('front.emails.notifyEmail', $data, function ($message) use ($RecieverUser, $meme) {

                $message->to($RecieverUser['email'], Auth::user()->name)
                    ->subject('You have new notification on your post.');
            });
        }

        $updateStatus = Notification::where('meme_id', $request->meme_id)->where('type', 'Comment on Your Post')->where('status', 0)->update(array('status' => 1));


        return response()->json([
                "message" => "Commented successfully",
                "status" => true
            ]);
    }

    public function apiStoreReply(Request $request)
    {
        //return $request;

        $reply = new Comment();
        $reply->body = $request->get('comment_body');
        $reply->user()->associate($request->user());
        $reply->parent_id = $request->comment_id;
        $post = Meme::find($request->get('meme_id'));
        $post->comments()->save($reply);

        $user = Auth::user();

        $ParentComment = Comment::where('id', $request->comment_id)->first();
        $meme = Meme::where('id', $ParentComment->meme_id)->first();

        $notify = Notification::where('meme_id', $request->meme_id)->where('comment_id', $request->comment_id)->where('user_id', $user->id)
            ->where('type', 'Reply on Your Comment')
            ->delete(); //delete old notify if user again reply on same post
        if ($ParentComment->user_id !== $user->id) {
            $n = new Notification();
            $n->meme_id = $meme->id;
            $n->comment_id = $reply->id;
            $n->comment_parent_id = $request->comment_id;
            $n->recieve_by = $ParentComment->user_id;
            $n->type = 'Reply on Your Comment';
            $n->user_id = $user->id;
            $n->status = 1;
            $n->save();

            $RecieverUser = User::where('id', $ParentComment->user_id)->first();

            //for mail

            $c = Comment::selectRaw('meme_id,COUNT(meme_id) as total')
                ->selectRaw('user_id,COUNT(user_id) as oldUser')->where('meme_id', $meme->id)->where('user_id', '!=', $ParentComment->user_id)
                ->groupBy('meme_id', 'user_id')->get()->count();

            if ($c >= 2) {

                if (@$c->oldUser > 1) {
                    $data['title'] = $user->name . ' ' . 'replied on your comment.';
                } else {
                    $data['title'] = $user->name . ' ' . 'and' . ' ' . ($c - 1) . ' ' . 'others replied on your comment.';

                }

            } else {
                $data['title'] = $user->name . ' ' . 'replied on your comment.';
            }

            $data['action'] = url($meme->id . '/' . $meme->uuid);
            $data['receiver'] = $RecieverUser['name'];

            Mail::send('front.emails.notifyEmail', $data, function ($message) use ($RecieverUser, $meme) {

                $message->to($RecieverUser['email'], Auth::user()->name)
                    ->subject('You have new notification on your post.');
            });
        }
        $updateStatus = Notification::where('meme_id', $meme->id)->where('type', 'Reply on Your Comment')->where('status', 0)
            ->update(array('status' => 1));

        return response()->json([
                "message" => "Replied successfully",
                "status" => true
            ]);

    }
}
