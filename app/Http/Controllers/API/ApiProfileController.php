<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Laravel\Passport\Passport;
// use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth; 
use App\User;
use App\Meme;
use PHPUnit\Framework\Constraint\CountTest;

class ApiProfileController extends Controller
{
    public function getUserprofile($parameter=null)
    {

        $user = Auth::user();
        if ($user) {
            if($parameter == null)
            {
            $home = Meme::latest()->where('user_id', $user->id)->get();
            return response()->json([
            	"success" => true,
                "status" => 200,
                'home'=>$home
				]);
			}
            if($parameter == 'posts')
            {
                $posts = Meme::latest()->where('user_id', $user->id)->get();
                return response()->json([
                "success" => true,
                "status" => 200,
                'home'=>$posts
                ]);
            }
            if($parameter == 'upvotes')
            {
                 $upvote = Meme::select('memes.id', 'memes.user_id', 'meme_votes.meme_id', 'memes.file', 'memes.file_description', 'memes.uuid',
                'memes.thumbnail', 'memes.title', 'memes.slug', 'memes.tags', 'memes.view_count', 'memes.share_count', 'memes.category_id',
                'memes.created_at')
                ->where('meme_votes.user_id', $user->id)
                ->join('meme_votes', 'meme_votes.meme_id', 'memes.id')
                ->where('meme_votes.value', 1)
                ->latest('meme_votes.created_at', 'desc')
                ->get();
                return response()->json([
                    "success" => true,
                    "status" => 200,
                    'home'=>$upvote
                    ]);
            }
            if($parameter == 'comments')
            {
                     $CommentedMeme = Meme::selectRaw('comments.meme_id,COUNT(comments.meme_id) as total')
                ->select('memes.*')
                ->where('comments.user_id', $user->id)
                ->join('comments', 'comments.meme_id', 'memes.id')
                ->groupBy('comments.meme_id', 'memes.id', 'memes.user_id', 'memes.file', 'memes.file_description', 'memes.uuid',
                    'memes.thumbnail', 'memes.title', 'memes.slug', 'memes.tags', 'memes.view_count', 'memes.share_count', 'memes.category_id',
                    'memes.created_at', 'memes.updated_at')
                ->orderBy('comments.created_at', 'desc')
                ->get();

                return response()->json([
                    "success" => true,
                    "status" => 200,
                    'home'=>$CommentedMeme
                    ]);
            }
            
        }
        else
            {
            return response()->json([
                    "success" => false,
                    "status" => 404,
                    'message'=>"User not found."
                    ]);
        }

    }
}
