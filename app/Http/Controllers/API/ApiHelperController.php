<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Meme;
use App\Category;
use Carbon\Carbon;

class ApiHelperController extends Controller
{
    
    public function categories(){
    	$m = Meme::selectRaw('category_id,COUNT(category_id) AS total')->orderBy('total','desc')
        ->groupBy('category_id')->pluck('category_id');

    	$c = Category::latest()->whereIn('id', $m)
        ->get();
        return response()->json(['status'=> true ,'categories'=> $c]);
    }

    // trending posts
    public function trending(){
    	$highestView = Meme::orderBy('view_count', 'desc')
        ->first();
    	$lowestView = Meme::orderBy('view_count', 'asc')
        ->first();
    	$trendMems =  Meme::where('created_at','>=',Carbon::now()->subdays(10))
    	->where('view_count', '<', $highestView->view_count)
    	->limit(6)->get();
        return response()->json(['status'=> true ,'trendings'=> $trendMems]);
    }

    // recent memes
    public function recent_memes(){
    	$memes = Meme::latest()->limit(6)->get();
    	return response()->json(['status'=> true ,'recentMemes'=> $memes]);
    }

    //home page main content posts
    public function homeMainPosts()
    {
        $memes = \App\Meme::latest()->whereDate('created_at', '>', Carbon::now()->subDays(30))->get();
        return response()->json(['result'=>$memes]);
    }

    //get posts by a category
    public function getCategoryPosts($categorySlug)
    {   
        if ($categorySlug) {
            $category = Category::where('slug', $categorySlug)->first();
            if($category)
            {
                $memes = Meme::where('category_id', $category->id)
                    ->latest()
                    ->orderBy('updated_at', 'desc')
                    ->get();

                return response()->json(['status'=>true, 'category_posts'=>$memes]);
            }
            return response()->json(['status'=>false, 'message'=>"This Category Doesn't Exist."]);
        }
    }

    public function apiSearch(Request $request)
    {
        $search = $request->search;

        if ($search)
        {
            $data = Category::where('categories.name', 'LIKE', '%' . $search . '%')
                ->orwhere('memes.title', 'LIKE', '%' . $search . '%')
                ->join('memes', 'memes.category_id', 'categories.id')->get();

            $tags = Category::whereIn('name', $data->pluck('name'))->get();
            return response()->json(['status'=>true, 'search'=>$search, 'tags'=>$tags, 'posts'=>$data]);
        }
        return response()->json(['status'=>false, 'message'=>'No results found.']);
    }
}
