<?php
namespace App\Http\Controllers\API;
use Laravel\Passport\Passport;
use Illuminate\Http\Request; 
use App\Http\Controllers\Controller; 
use App\User; 
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth; 
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Support\Str;
//use SendsPasswordResetEmails;
use Validator;
class UserApiController extends Controller 
{
	public $successStatus = 200; 
    
    /** 
     * Register api 
     * 
     * @return \Illuminate\Http\Response 
     */ 
    public function register(Request $request) 
    { 

        $validatedData = $request->validate([ 
            'name' => 'required|string|max:50',
            'email' => 'required|email|unique:users',
            'password' => 'required|confirmed|min:8',
        ]);
        // if ($validatedData->fails()) { 
        //     return response()->json(['status'=> false,'error'=>$validatedData->errors()]);            
        // }

        $validatedData['password'] = Hash::make($request->password);
        $user = User::create($validatedData);
        $user->avatar = 'random_profile_pic.jpg';
        if ($request->name) {
                $user->name = str_replace('', '', ucwords(str_replace('', '', strtolower( $request->name))));
            }
        $accessToken =  $user->createToken('authToken')->accessToken;
        $user->save();
        return response()->json(['status'=> true ,'message'=> 'user registered successfully','user'=>$user, 'accessToken'=>$accessToken]);

      //   if ($validator->fails()) { 
      //       return response()->json(['status'=> false,'error'=>$validator->errors()]);            
      //   }
      //   $user = new User();
      //       if ($request->name) {
      //           $user->name = str_replace('', '', ucwords(str_replace('', '', strtolower( $request->name))));
      //       }
      //       $user->email = $request->email;
      //       $user->avatar = 'random_profile_pic.jpg';
      //       $user->password = Hash::make($request->password);
      //       // $token = Str::random(60);
      //       // $user->api_token = hash('sha256', $token);
    		// // $accessToken =  $user->createToken('authToken')->accessToken;
      //       $user->save();
      //       if (Auth::attempt(['email' => $request['email'], 'password' => $request['password']])) {
      //               $user = Auth::user();
      //               $accessToken = $user->createToken('lolwhoa_api')->accessToken;
      //               return response()->json(['status'=> true ,'message'=> 'user registered successfully','user'=>$user, 'accessToken'=>$accessToken]);
      //           }
      //           else{
      //               return response()->json(['status'=> false ,'message'=> 'something went wrong']);
      //           }
    }

    /** 
     * login api 
     * 
     * @return \Illuminate\Http\Response 
     */
    public function login(Request $request){ 
        $loginData = $request->validate([ 
            'email' => 'required|email', 
            'password' => 'required'
        ]);
        // if ($loginData->fails()) { 
        //     return response()->json(['error'=>$loginData->errors()], 401);            
        // }
        if(!auth()->attempt($loginData)){
            return response()->json(['message'=> 'Invalid credentials']);
        }
        $accessToken =  auth()->user()->createToken('authToken')->accessToken;

        return response()->json(['user'=> auth()->user() ,'access_token'=> $accessToken]);


        // if ($validator->fails()) { 
        //     return response()->json(['error'=>$validator->errors()], 401);            
        // }
        // if(Auth::attempt(['email' => request('email'), 'password' => request('password')])){ 
        //     $user = Auth::user(); 
        //     $token = Str::random(60);
        //     $user->api_token = hash('sha256', $token);
        //     $user->save();
        //     // $success['token'] =  $user->createToken('ApiToken')->accessToken; 
        //     return response()->json(['status'=> true ,'message'=> 'user logged In successfully','user'=>$user]); 
        // } 
        // else{
        //     return response()->json(['status'=> false ,'error'=>'Unauthorised'], 401);
        // }
    }

    public function logoutApi(Request $request)
    {
        // $chek_token = $request->api_token;

        // // $user = User::find($chek_token);
        // User::where('api_token', $chek_token)->update(array('api_token' => null));
        // return response()->json(['status'=> true ,'message'=> 'user logged Out successfully']); 
        // if (Auth::check()) {
        //     Auth::user()->AauthAcessToken()->delete();
        // }
        // return response()->json([
        //     "msg" => "Log-Out Successfully",
        //     "status" => $this->successStatus,
        // ]);

        $request->user()->token()->revoke();
        // $user = Auth::user()->token();
        // $user->revoke();
        return response()->json([
            "msg" => "Log-Out Successfully",
            "status" => $this->successStatus,
        ]); 

    }

    /** 
     * details api 
     * 
     * @return \Illuminate\Http\Response 
     */ 
    public function details() 
    { 
        $user = Auth::user(); 
        return response()->json(['success' => $user], $this->successStatus); 
    }

    public function AuthUserDetails(Request $request)
    {
        //$user = User::find($id);
        // return $request->user();
        $user = Auth::user();
        return response()->json([
            "status" => $this->successStatus,
            "data" => $user
        ]);
    }
    
}