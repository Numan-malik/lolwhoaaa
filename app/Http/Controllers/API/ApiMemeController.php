<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Meme;
use Auth;
use Session;
use App\MemeVote;
use App\Comment;
use App\CommentVote;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Validator;
use Storage;
use Illuminate\Support\Facades\DB;
use Intervention\Image\ImageManagerStatic as Image;


class ApiMemeController extends Controller
{
    public function storeMeme(request $request)
    {
    	$x= $request->all();

    	$validator = Validator::make($request->all(), [
            'title' => 'required|string|max:255',
            'tags' => 'required|string|max:255',
            'category' => 'required'
        ]);
        if ($validator->fails()) {
            return response()->json([
                "errors" => $validator->errors()->all(),
                "status" => 400
            ]);
        }

        $meme = new Meme();
        if ($request->img) {
            \Storage::disk('public')->makeDirectory('uploads/thumbnails');
            \Storage::disk('public')->makeDirectory('uploads/memes');

            if (\File::exists(Storage_path('app/public/uploads/thumbnails/' . $request->img))) {
                \File::delete(Storage_path('app/public/uploads/thumbnails/' . $request->img));
            }
            if (\File::exists(Storage_path('app/public/uploads/memes/' . $request->img))) {
                \File::delete(Storage_path('app/public/uploads/memes/' . $request->img));
            }
            $image = $request->file('img');
            $destinationPath = Storage_path('app/public/uploads/thumbnails');
            $originalImgPath = Storage_path('app/public/uploads/memes');
            $img = Image::make($image->getRealPath());
            $img->orientate();
            $input['file'] = random_int(1, 99) . date('sihYdm') . random_int(1, 99) . '.' .
                $image->getClientOriginalExtension();
            $meme->file_description = 'upload from pc';

            if ($image->getClientOriginalExtension() == 'gif') {
                $img = time() . '.' . request()->img->getClientOriginalExtension();
                request()->img->move(public_path('storage/uploads/thumbnails'), $img);
                $meme->file = $img;
                $meme->thumbnail = $img;
            } else {

                $img->save($originalImgPath . '/' . $input['file']);
                $meme->file = $input['file'];

                $img->resize(250, null, function ($constraint) {
                    $constraint->aspectRatio();
                })->save($destinationPath . '/' . $input['file']);
                $img->save($destinationPath . '/' . $input['file']);

                $meme->thumbnail = $input['file'];
            }
        } else {
            $meme->file = $request->imgUrl;
            $meme->file_description = 'from URL';
        }

        //  $meme->uuid = Str::uuid();
        $meme->uuid = substr(md5(time()), 0, 8); //generate random alpha numeric string
        $meme->title = $request->title ? $request->title : null;
        $meme->slug = str_slug($request->title);
        $meme->user_id = Auth::user()->id;
        $meme->category_id = $request->category ? $request->category : null;
        $meme->tags = $request->tags ? $request->tags : null;
        $meme->save();

        return response()->json(["success" => true,"status" => 200,"message"=> "Meme shared successfully"]);
    }



    public function memeDetail(Request $request, $slug, $Uuiid)
    {
        if ($slug) {
            $meme = Meme::where('uuid', $Uuiid)
                ->Where('slug', $slug)
                ->first();
            if ($meme) {
                $MemeRandom = Meme::inRandomOrder()
                    ->where('id', '!=', $meme->id)
                    ->take(1)
                    ->get();
                $meme['upvotes'] = MemeVote::where('meme_id', $meme->id)
                    ->where('value', 1)
                    ->get()
                    ->count();
                $meme['downvotes'] = MemeVote::where('meme_id', $meme->id)
                    ->where('value', 0)
                    ->get()
                    ->count();
                $meme['totalComments'] = Comment::where('meme_id', $meme->id)->get()->count();
                $comment = $meme->comments;
                foreach ($comment as $key => $value) {
                    if($value->user->avatar !== null){
                        $comment[$key]['UserAvatar'] = $value->user->avatar;   
                    }
                    $comment[$key]['repliesCount'] = Comment::where('parent_id', $value->id)->get()->count();
                    $comment[$key]['upvotes'] = CommentVote::where('comment_id', $value->id)->where('value', 1)->get()->count();
                    $comment[$key]['downvotes'] = CommentVote::where('comment_id', $value->id)->where('value', 0)->get()->count();

                    $comment[$key]['replies'] = Comment::where('parent_id', $value->id)->get();

                    foreach ($comment[$key]['replies'] as $key1 => $value1) {
                        if($value1->user->avatar !== null){
                            $comment[$key]['replies'][$key1]['UserAvatar'] = $value1->user->avatar;   
                        }
                        $comment[$key]['replies'][$key1]['upvotes'] = CommentVote::where('comment_id', $value1->id)->where('value', 1)->get()->count();
                        $comment[$key]['replies'][$key1]['downvotes'] = CommentVote::where('comment_id', $value1->id)->where('value', 0)->get()->count();
                    }
                }
            return response()->json(['status' => true, 'memes' => $meme, 'MemeRandom' => $MemeRandom]);
            }

        }
        return response()->json(['status'=> false, 'messgae' => 'No results.']);
    }
}
