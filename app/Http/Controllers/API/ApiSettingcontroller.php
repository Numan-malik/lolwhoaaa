<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use Laravel\Passport\Passport;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth; 


class ApiSettingcontroller extends Controller
{
    
    // updating name and email.
   	public function updateSetting(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|max:255',
            'email' => 'required|email'
        ]);

        if ($validator->fails()) {
            return response()->json([
                "errors" => $validator->errors()->all(),
                "status" => 400
            ]);
        } else {
            $userEmail = User::where('id', '!=', 0)->pluck('email');
            foreach ($userEmail as $email) {
                if ($email !== $request->email) {
                    $user = Auth::user();
                    $user->name = str_replace(' ', ' ', ucwords(str_replace('', ' ', strtolower($request->name))));
                    $user->email = $request->email;
                    $user->save();
                    return response()->json([
                        "success" => true,
                        "status" => 200
                    ]);
                } else {
                    return response()->json([
                        "errors" => ["This Email has already taken"],
                        "status" => 400
                    ]);
                }
            }
        }
    }




	// updating profile
	public function updateProfile(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'full_name' => 'required|string|max:255',
            'avatar' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',]);
        if ($validator->fails()) {
            if ($request->year < 1900 || $request->year > 2009 || $request->month > 12 || $request->day > 31) {
                return response()->json([
                    "errors" => $validator->errors()->all(),
                    "status" => 400
                ]);
            } else {
                return response()->json([
                    "errors" => $validator->errors()->all(),
                    "status" => 400
                ]);
            }
        } else {
            $user = Auth::user();
            if ($request->avatar)
            {
                $img = time() . '.' . request()->avatar->getClientOriginalExtension();
                request()->avatar->move(public_path('uploads/users'), $img);

                $user->avatar = $img ? $img : null;
            }
            $user->full_name = str_replace('', '', ucwords(str_replace('', '', strtolower($request->full_name))));
            $user->gender = $request->gender;

            if ($request->day !== null && $request->month !== null && $request->year !== null) {
                $user->birthday = $request->day . '-' . $request->month . '-' . $request->year;
            }
            $user->country = $request->country;
            $user->bio = $request->bio;
            $user->save();
            return response()->json([
                "success" => true,
                "status" => 200
            ]);
        }
    }





	// updating password
    public function updatePassword(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'password' => 'required|confirmed|min:8|max:20',
            'old_password' => 'required|min:8|max:20',
        ]);
        if ($validator->fails()) {
            return response()->json([
                "errors" => $validator->errors()->all(),
                "status" => 400
            ]);
        }
        else {
            
            // return response()->json([$user]);
        	// $id = $request->id;
         //    $user = User::find($id);
            $user = Auth::user();
            if (Hash::check($request->old_password, $user->password)) {
                if (!Hash::check($request->password, $user->password)) {
                    if ($request->password == $request->password_confirmation) {

                        $users = Auth::user();
                        // $findId =  $users;
                        // return $findId;
                        $users->password = bcrypt($request->password);
                        User::where('id', $users->id)->update(array('password' => $users->password));
                        return response()->json([
                            "success" => true,
                            "status" => 200
                        ]);
                    } else {
                        return response()->json([
                            "errors" => 'Confirm password does not matched',
                            "status" => 401
                        ]);
                    }

                } else {
                    return response()->json([
                        "errors" => 'New password can not be the old password!',
                        "status" => 402
                    ]);
                }

            } else {
                return response()->json([
                    "errors" => 'Old password does not matched',
                    "status"=>403
                ]);
            }
        }
    }



}
