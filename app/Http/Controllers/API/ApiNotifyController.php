<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\CommentVote;
use App\MemeVote;
use App\Notification;
use App\Comment;
use Auth;

class ApiNotifyController extends Controller
{
    //
    public function getAllNotify()
    {
        if (Auth::check()) {
            $user = Auth::user();
            $notifications = \App\Notification::latest()
                ->where('user_id', '!=', $user->id)
                ->where('recieve_by', $user->id)
                ->get();
            if (isset($notifications)) {
                return response()->json(['status'=> true ,'notifications'=> $notifications]);
            }
        }
        else{
            return response()->json(['message'=> 'No any notification exists']);
        }
    }


    // get recent notifications
    public function getRecentNotify()
    {
        if (Auth::check()) {
            $user = Auth::user();
            $recentNotifications = \App\Notification::latest()
                ->where('user_id', '!=', $user->id)
                ->where('recieve_by', $user->id)
                ->where('status', 1)
                ->where('isRead', 1)
                ->where('notifyStatus', 1)
                ->get();
            $t_notify = $recentNotifications->count(); 
            if(isset($recentNotifications)){
                return response()->json(['status'=> true ,'totalRecentNotify'=>$t_notify,'recentNotifications'=> $recentNotifications]);
            }
        }
        
    }



    // update notify status
    public function updateNotifyStatus(Request $request)
    {
        if ($request->ajax()) {
            $user = Auth::user();
            $n = Notification::where('recieve_by', $user->id)->where('status', 1)->update(array('status' => 0,
                'notifyStatus' => 0));
           return response()->json([
               "status" => true,
               "data" => $n,
               "message" => "Notify status updated"
           ]);
        }

    }

    //change isRead status if notification seen

    public function updateIsReadStatus(Request $request)
    {
        if ($request->ajax()) {
            $user = Auth::user();

            if ($request->comment_id) {
                $n = Notification::where('meme_id', $request->meme_id)->where('comment_id', $request->comment_id)->where('type', $request->attr)
                    ->where('recieve_by', $user->id)
                    ->where('isRead', 1)->update(array('isRead' => 0));
            } else {
                $n = Notification::where('meme_id', $request->meme_id)->where('type', $request->attr)->where
                ('recieve_by', $user->id)->where('isRead', 1)->update(array('isRead' => 0));
            }

           return response()->json([
               "status" => true,
               "data" => $n,
               "message" => "read status updated"
           ]);
        }

    }
}
