<?php

namespace App\Http\Controllers;

use App\Meme;
use App\MemeVote;
use App\MemeNotification;
use App\Notification;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Response;
use Mail;
use App\User;

class MemeVotesController extends Controller
{
    public function storeMemeVotes(Request $request)
    {
        $user = Auth::user();
        $old = MemeVote::where('user_id', $user->id)
            ->where('meme_id', $request->meme_id)
            ->first();
        $meme = Meme::where('id', $request->meme_id)->first();

        if (!$old) {
            $vote = new MemeVote();
            $vote->user_id = $user->id;
            $vote->meme_id = $request->meme_id;
            $vote->value = $request->value;
            $vote->save();

            if ($request->value == 1 && $meme->user_id !== $user->id) {

                $n = new Notification();
                $n->meme_id = $meme->id;
                $n->recieve_by = $meme->user_id;
                $n->type = 'Like Your Post';
                $n->user_id = $user->id;
                $n->status = 1; //Notification Unseen
                $n->save();

                $memeVote = MemeVote::where('meme_id', $meme->id)->where('value', 1)->get()->count();

                $RecieverUser = User::where('id', $meme->user_id)->first();

                if ($memeVote >= 2) {
                    //return 5;
                    $data['title'] = $user->name . ' ' . 'and' . ' ' . ($memeVote - 1) . ' ' . 'others Like your post.';
                } else {
                    //return $memeVote;
                    $data['title'] = $user->name . ' ' . 'Like your post.';
                }
                $data['action'] = url($meme->id . '/' . $meme->uuid);
                $data['receiver'] = $RecieverUser['name'];

                Mail::send('front.emails.notifyEmail', $data, function ($message) use ($RecieverUser, $meme) {

                    $message->to($RecieverUser['email'], Auth::user()->name)
                        ->subject('You have new notification on your post.');
                });

            }
            $updateStatus = Notification::where('meme_id', $request->meme_id)->where('type', 'Like Your Post')->where('status', 0)
                ->update(array('status' => 1, 'isRead' => 1));
        } else {

            $vote = MemeVote::find($old->id);
            if ($request->value == $old->value) {

                if ($request->value == 1) {
                    $n = Notification::where('user_id', $vote->user_id)->where('type', 'Like Your Post')->where('meme_id', $vote['meme_id'])->first();
                    if ($n) {
                        $n->delete();
                    }
                }
                $vote->delete();
            } else {
                $vote->value = $request->value;
                $vote->save();
            }

        }
//        return response()->json([
//            "status" => true,
//            "data" => $vote,
//            "msg" => "Success"
//        ]);

    }

}
