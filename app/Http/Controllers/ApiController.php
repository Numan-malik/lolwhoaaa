<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use App\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;


class ApiController extends Controller
{
    public $successStatus = 200;

    public function signUp(
        $request)
    {
        if ($request->isMethod('post')) {
            $validator = Validator::make($request->all(), [
                'name' => 'required|string|max:255',
                'email' => 'required|email|string|max:255|unique:users',
                'password' => 'required|string|min:8'
            ]);
            if ($validator->fails()) {
                return response()->json([
                    "msg" => 'Register Failed',
                    "errors" => $validator->errors(),
                    "status" => 400, //Bad request
                    "data" => $request->all(),
                ]);

            } else {
                $user = new User();
                if ($request->name) {
                    $user->name = str_replace('', '', ucwords(str_replace('', '', strtolower($request->name))));
                }
                $user->email = $request->email;
                $user->avatar = 'random_profile_pic.jpg';
                $user->password = Hash::make($request->password);
                $user->save();

                $success['token'] =  $user->createToken('MyApp')-> accessToken;
                $user->save();

                if (Auth::attempt(['email' => $request['email'], 'password' => $request['password']])) {
                    $user = Auth::user();
                    $success['token'] = $user->createToken('LOLWHOA')->accessToken;
                    return response()->json([
                        "success" => $success,
                        "msg" => "Register Successfully",
                        "status" => $this->successStatus,
                    ]);
                }

            }
        } else {
            // return view('/');
        }

    }

    public function login(Request $request)
    {
        if ($request->isMethod('post')) {
            $data = $request->input();
            if ($data) {
                if (Auth::attempt(['email' => $data['email'], 'password' => $data['password']])) {
                    $user = Auth::user();
                    $success['token'] = $user->createToken('LOLWHOA')->accessToken;
                    return response()->json([
                        "success" => $success,
                        "msg" => "Login Successfully",
                        "status" => $this->successStatus,
                    ]);
                } else {
                    return response()->json([
                        "msg" => "Invalid Email or password",
                        "status" => 400, //Bad request
                        "data" => $data
                    ]);
                }
            }

        }
        return view('/');
    }

    public function logout()
    {
        if (Auth::check()) {
            Auth::user()->AauthAcessToken()->delete();
        }
        return response()->json([
            "msg" => "Log-Out Successfully",
            "status" => $this->successStatus,
        ]);
    }

    public function AuthUserDetails()
    {
        //$user = User::find($id);
        $user = Auth::user();
        return response()->json([
            "status" => $this->successStatus,
            "data" => $user
        ]);
    }

}
