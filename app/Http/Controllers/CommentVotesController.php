<?php

namespace App\Http\Controllers;

use App\Comment;
use App\Meme;
use App\Notification;
use App\CommentVote;
use Illuminate\Http\Request;
use Auth;
use App\User;
use Redirect, Response, DB, Config;
use Mail;

class CommentVotesController extends Controller
{
    public function StoreCommentVotes(Request $request)
    {
        $user = Auth::user();
        $old = CommentVote::where('user_id', $user->id)
            ->where('comment_id', $request->comment_id)
            ->first();
        if (!$old) {
            $c = Comment::where('id', $request->comment_id)->first();
            $meme = Meme::where('id', $c->meme_id)->first();

            $vote = new CommentVote();
            $vote->user_id = $user->id;
            $vote->comment_id = $request->comment_id;
            $vote->meme_id = $meme->id;
            $vote->value = $request->value;
            $vote->save();
            if ($request->value == 1) {

                $n = new Notification();
                $n->meme_id = $meme->id;
                $n->comment_id = $request->comment_id;
                $n->recieve_by = $c->user_id;
                $n->type = 'Like Your Comment';
                $n->user_id = $user->id; //sender
                $n->status = 1; //Notification Unseen
                $n->save();

                $RecieverUser = User::where('id', $c->user_id)->first();

                $cmntVote = CommentVote::where('comment_id', $request->comment_id)->where('value', 1)->get()->count();

                if ($cmntVote >= 2) {
                    $data['title'] = $user->name . ' ' . 'and' . ' ' . ($cmntVote - 1) . ' ' . 'others Like your comment.';
                } else {
                    $data['title'] = $user->name . ' ' . 'Like your comment.';
                }

                $data['action'] = url($meme->id . '/' . $meme->uuid);
                $data['receiver'] = $RecieverUser['name'];

                Mail::send('front.emails.notifyEmail', $data, function ($message) use ($RecieverUser, $meme) {

                    $message->to($RecieverUser['email'], Auth::user()->name)
                        ->subject('You have new notification on your post.');
                });

            }
            $updateStatus = Notification::where('meme_id', $meme->id)->where('type', 'Like Your Comment')->where('status', 0)
                ->update(array('status' => 1));

        } else {

            $vote = CommentVote::find($old->id);
            $memeId = Comment::where('id', $vote['comment_id'])->pluck('meme_id');

            if ($request->value == $old->value) {
                if ($request->value == 1) {

                    $n = Notification::where('user_id', $vote->user_id)->where('type', 'Like Your Comment')->whereIn('meme_id', $memeId)->first();
                    if ($n) {
                        $n->delete();
                    }

                }
                $vote->delete();
            } else {
                $vote->value = $request->value;
                $vote->save();
            }

        }

//        return $response = array(
//            'status' => true,
//            'data' => $vote,
//            'msg' => 'success'
//        );
    }


}
