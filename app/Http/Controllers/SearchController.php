<?php

namespace App\Http\Controllers;

use App\Category;
use App\Meme;
use Illuminate\Http\Request;

class SearchController extends Controller
{
    public function index(Request $request)
    {
        $search = $request->search;

        if ($search)
        {
            $data = Category::where('categories.name', 'LIKE', '%' . $search . '%')
                ->orwhere('memes.title', 'LIKE', '%' . $search . '%')
                ->join('memes', 'memes.category_id', 'categories.id')->get();

            $tags = Category::whereIn('name', $data->pluck('name'))->get();
            return view('front.search', compact('data', 'search','tags'));
        }
        return back();
    }

    public function search(Request $request)
    {
        if ($request->ajax()) {

            $search = $request->all();
            if ($search['query'] !== null) {
                $data = Category::where('categories.name', 'LIKE', '%' . $search['query'] . '%')
                    ->orwhere('memes.title', 'LIKE', '%' . $search['query'] . '%')
                    ->join('memes', 'memes.category_id', 'categories.id')->get();

                $output = '';
                if (count($data) > 0) {
                    foreach ($data as $row) {
                        $url = url($row->slug . '/' . $row->uuid);
                        $output .= '<div class="m-2"><a  href=' . $url . ' class="navbar-color">'. $row->title.'</div>';
                    }
                } else {
                    $output .= '<div class="m-20 text-center clr-wht">No results</div>';
                }
                return $output;

            }

        }


    }
}
