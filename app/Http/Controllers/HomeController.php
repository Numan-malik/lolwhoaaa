<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Response;
use Illuminate\Http\Request;
use Auth;
use Mail;
use Carbon\Carbon;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        $CurrentMonth = date('m');
         $memes = \App\Meme::latest()->whereDate('created_at', '>', Carbon::now()->subDays(30))->get();
        if ($request->ajax()) {
            $user = Auth::user();

            $html = view('front.memes.master-meme', compact('memes'))->render();
            return response()->json([
               // 'AuthUser' => $user,
                'html' => $html
            ]);
        }

        return view('home', compact('memes'));
    }

    public function ContactUs()
    {
        return view('front.contact');
    }

    public function SendContactMsg(Request $request)
    {
        $this->validate($request,[
            'name'  => 'min:3',
            'email'    => 'required|email',
            'message'  => 'min:10'
        ]);
        $data = array(
            'email'       => $request->email,
            'name'        => $request->name,
            'bodyMessage' => $request->message
        );
        Mail::send('front.emails.contact', $data,  function ($message) use ($data){
            $message->from("belawalumer4@gmail.com");
            $message->to("belawalumer4@gmail.com");
            $message->subject("Message from Lolwhoa");
        });
        return redirect()->back()->with('message', 'thanks for the message! We will get back to you soon!');
    }

}
