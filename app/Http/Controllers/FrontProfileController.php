<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Auth;
use App\Meme;
use PHPUnit\Framework\Constraint\CountTest;

class FrontProfileController extends Controller
{
    public function getUserprofile($username, $id,$parameter=null)
    {
        $user = User::where('name', $username)->where('id', $id)->first();
        if ($user) {
            if($parameter == null)
            {
            $home = Meme::latest()->where('user_id', $user->id)->get();
            return view('front.profile.home', compact('home','user'));
            }
            if($parameter == 'posts')
            {
                $posts = Meme::latest()->where('user_id', $user->id)->get();
                return view('front.profile.posts', compact('posts','user'));
            }
            if($parameter == 'upvotes')
            {
                 $upvote = Meme::select('memes.id', 'memes.user_id', 'meme_votes.meme_id', 'memes.file', 'memes.file_description', 'memes.uuid',
                'memes.thumbnail', 'memes.title', 'memes.slug', 'memes.tags', 'memes.view_count', 'memes.share_count', 'memes.category_id',
                'memes.created_at')
                ->where('meme_votes.user_id', $user->id)
                ->join('meme_votes', 'meme_votes.meme_id', 'memes.id')
                ->where('meme_votes.value', 1)
                ->latest('meme_votes.created_at', 'desc')
                ->get();
             
            return view('front.profile.upvote', compact('upvote','user'));
            }
            if($parameter == 'comments')
            {
                     $CommentedMeme = Meme::selectRaw('comments.meme_id,COUNT(comments.meme_id) as total')
                ->select('memes.*')
                ->where('comments.user_id', $user->id)
                ->join('comments', 'comments.meme_id', 'memes.id')
                ->groupBy('comments.meme_id', 'memes.id', 'memes.user_id', 'memes.file', 'memes.file_description', 'memes.uuid',
                    'memes.thumbnail', 'memes.title', 'memes.slug', 'memes.tags', 'memes.view_count', 'memes.share_count', 'memes.category_id',
                    'memes.created_at', 'memes.updated_at')
                ->orderBy('comments.created_at', 'desc')
                ->get();

            return view('front.profile.comments', compact('CommentedMeme','user'));
            }
            
        }
        else
            {
            return view('front.errors.404');
        }

    }
}
