<?php

namespace App\Http\Controllers;

use App\Category;
use App\Comment;
use App\CommentVote;
use App\MemeVote;
use App\Notification;
use App\SocialProvider;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use App\User;
use App\Meme;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use Intervention\Image\ImageManagerStatic as Image;

class AdminController extends Controller
{
    public function adminlogin(Request $request)
    {
        if ($request->isMethod('post')) {
            $data = $request->input();
            if (Auth::attempt(['email' => $data['email'], 'password' => $data['password'], 'user_role' => 'admin'])) {

                return redirect('/admin/dashboard');
            } else {

                return redirect('/admin/login')->with('login-error', 'Invalid Email or password');
            }
        }
        return view('admin/admin_login');
    }

    public function user()
    {
        return view('admin.user');
    }

    public function adminlogout()
    {
        Session::flush();
        return redirect('/admin/login')->with('logout-success', 'Log-Out Successfully');
    }

    public function Users()
    {
        $users = User::latest()->get();
        return view('admin.user', compact('users'));
    }
    public function DeleteUser($id)
    {
        $user = User::find($id);
        Meme::where('user_id',$id)->delete();
        MemeVote::where('user_id',$id)->delete();
        Category::where('user_id',$id)->delete();
        Comment::where('user_id',$id)->delete();
        CommentVote::where('user_id',$id)->delete();
        Notification::where('user_id',$id)->delete();
        Notification::where('recieve_by',$id)->delete();
        SocialProvider::where('user_id',$id)->delete();
        $user->delete();

        return redirect('/admin/user')->with('msg', 'User Delete Successfully');
    }

    public function Category()
    {
        return view('admin.category');
    }

    public function AllMemes()
    {
        $memes = Meme::latest()->get();
        $categories = Category::where('status', 1)->get();
        return view('admin.memes', compact('categories', 'memes'));
    }
    public function EditMeme($id)
    {
        $meme = Meme::find($id);
        $memes = Meme::latest()->get();
        $categories = Category::where('status', 1)->get();
        return view('admin.memes', compact('categories','meme','memes'));
    }

    public function CreateMemes(Request $request)
    {
        request()->validate([
            'title' => 'required|string|max:255',
            'tags' => 'required|string|max:255',
        ]);

        $meme = new Meme();
        if ($request->img) {

            \Storage::disk('public')->makeDirectory('uploads/thumbnails');
            \Storage::disk('public')->makeDirectory('uploads/memes');

            if (\File::exists(Storage_path('app/public/uploads/thumbnails/' . $request->img))) {
                \File::delete(Storage_path('app/public/uploads/thumbnails/' . $request->img));
            }
            if (\File::exists(Storage_path('app/public/uploads/memes/' . $request->img))) {
                \File::delete(Storage_path('app/public/uploads/memes/' . $request->img));
            }
            $image = $request->file('img');
            $destinationPath = Storage_path('app/public/uploads/thumbnails');
            $originalImgPath = Storage_path('app/public/uploads/memes');
            $img = Image::make($image->getRealPath());
            $img->orientate();
            $input['file'] = random_int(1, 99) . date('sihYdm') . random_int(1, 99) . '.' .
                $image->getClientOriginalExtension();

            $meme->file_description = 'upload from pc';

            if ($image->getClientOriginalExtension() == 'gif') {
                $img = time() . '.' . request()->img->getClientOriginalExtension();
                request()->img->move(public_path('storage/uploads/thumbnails'), $img);
                $meme->file = $img;
                $meme->thumbnail = $img;
            } else {
                $img->resize(500, null, function ($constraint) {
                    $constraint->aspectRatio();
                })->save($originalImgPath . '/' . $input['file']);

                $meme->file = $input['file'];

                $img->resize(200, null, function ($constraint) {
                    $constraint->aspectRatio();
                })->save($destinationPath . '/' . $input['file']);

                $meme->thumbnail = $input['file'];
            }
        } else {
            $meme->file = $request->imgUrl;
            $meme->file_description = 'from URL';
        }

        //  $meme->uuid = Str::uuid();
        $meme->uuid = substr(md5(time()), 0, 8); //generate random alpha numeric string
        $meme->title = $request->title ? $request->title : null;
        $meme->slug = str_slug($request->title);
        $meme->user_id = Auth::user()->id;
        $meme->category_id = $request->category ? $request->category : null;
        $meme->tags = $request->tags ? $request->tags : null;
        $meme->save();

        return redirect('/admin/all_memes')->with('message', 'Meme Save Successfully !');
    }

    public function UpdateMeme(Request $request,$id)
    {
        //return $request->all();
        request()->validate([
            'title' => 'required|string|max:255',
            'tags' => 'required|string|max:255',
        ]);

        $meme = Meme::find($id);

        $meme->title = $request->title ? $request->title : null;
        $meme->slug = str_slug($request->title);
        //$meme->user_id = Auth::user()->id;
        $meme->category_id = $request->category ? $request->category : null;
        $meme->tags = $request->tags ? $request->tags : null;
        $meme->save();

        return redirect('/admin/all_memes')->with('message', 'Meme Update Successfully !');
    }

    public function deleteMemes($id)
    {
        $meme = Meme::find($id);
        MemeVote::where('meme_id',$id)->delete();
        Comment::where('meme_id',$id)->delete();
        CommentVote::where('meme_id',$id)->delete();
        Notification::where('meme_id',$id)->delete();
        $meme->delete();

        return redirect('/admin/all_memes')->with('message', 'Meme Delete Successfully!');
    }

    //admin profile

    public function AdminProfile()
    {
        $user = Auth::user();
        return view('admin.adminProfile', compact('user'));
    }

    public function updateAdminSetting(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required|unique:users'
        ]);
        if ($validator->fails()) {
            $userEmail = User::where('id', '!=', 0)->pluck('email');
            foreach ($userEmail as $email) {
                if ($email !== $request->email) {
                    $user = User::find($id);
                    $user->name = str_replace('\' ', '\'', ucwords(str_replace('\'', '\' ', strtolower($request->name))));
                    $user->email = $request->email;
                    $user->save();
                    return redirect('/admin/admin-profile')->with('success-msg', 'Setting update successfully !');
                } else {

                    return redirect('/admin/admin-profile')->with('error-msg', 'This Email has already taken!');
                }
            }

        } else {
            $user = User::find($id);
            $user->name = str_replace('\' ', '\'', ucwords(str_replace('\'', '\' ', strtolower($request->name))));
            $user->email = $request->email;
            $user->save();

            return redirect('/admin/admin-profile')->with('success-msg', 'Setting update successfully !');
        }

    }

    public function updateAdminProfile(Request $request, $id)
    {
        if ($request->avatar) {
            $img = time() . '.' . request()->avatar->getClientOriginalExtension();
            request()->avatar->move(public_path('uploads/users'), $img);
        }

        $user = User::find($id);
        $user->full_name = str_replace(' ', '', ucwords(str_replace('', ' ', strtolower($request->full_name))));
        $user->gender = $request->gender;
        $user->birthday = $request->birthday;
        $user->country = $request->country;
        $user->bio = $request->bio;
        if ($request->avatar) {
            $user->avatar = $img ? $img : null;
        }

        $user->save();

        return redirect('/admin/admin-profile')->with('success-msg', 'Profile update successfully !');
    }

    public function updateAdminPassword(Request $request, $id)
    {

        $validator = Validator::make($request->all(), [
            'password' => 'required|confirmed|min:6',
            'old_password' => 'required',
        ]);

        $user = Auth::user();

        if (Hash::check($request->old_password, $user->password)) {
            if (!Hash::check($request->password, $user->password)) {
                if ($request->password == $request->password_confirmation) {

                    $users = User::find(Auth::user()->id);
                    $users->password = bcrypt($request->password);
                    User::where('id', Auth::user()->id)->update(array('password' => $users->password));

                    session()->flash('success-msg', 'New Password updated successfully !');
                    return redirect()->back();
                } else {

                    session()->flash('error-msg', 'Confirm password does not matched');
                    return redirect()->back();
                }

            } else {

                session()->flash('error-msg', 'New password can not be the old password!');
                return redirect()->back();
            }

        } else {

            session()->flash('error-msg', 'Old password does not matched ');
            return redirect()->back();
        }
    }

}
