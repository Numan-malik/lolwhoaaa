<?php

namespace App\Http\Controllers;

use App\Category;
use App\Comment;
use App\GenerateMemeImage;
use App\Meme;
use App\MemeVote;
use Illuminate\Http\Request;
use Auth;
use Session;
use Intervention\Image\ImageManagerStatic as Image;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Validator;
use Storage;
use Illuminate\Support\Facades\DB;

class MemeController extends Controller
{
    public function getMemes()
    {
        $memes = Meme::where('id', '!=', 0)->get();
        $categories = Category::where('id', '!=', 0)->get();
        return view('front.memes.meme', compact('memes', 'categories'));
    }

    public function StoreMemes(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'title' => 'required|string|max:255',
            'tags' => 'required|string|max:255',
        ]);
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }

        $meme = new Meme();
        if ($request->img) {

            \Storage::disk('public')->makeDirectory('uploads/thumbnails');
            \Storage::disk('public')->makeDirectory('uploads/memes');

            if (\File::exists(Storage_path('app/public/uploads/thumbnails/' . $request->img))) {
                \File::delete(Storage_path('app/public/uploads/thumbnails/' . $request->img));
            }
            if (\File::exists(Storage_path('app/public/uploads/memes/' . $request->img))) {
                \File::delete(Storage_path('app/public/uploads/memes/' . $request->img));
            }
            $image = $request->file('img');
            $destinationPath = Storage_path('app/public/uploads/thumbnails');
            $originalImgPath = Storage_path('app/public/uploads/memes');
            $img = Image::make($image->getRealPath());
            $img->orientate();
            $input['file'] = random_int(1, 99) . date('sihYdm') . random_int(1, 99) . '.' .
                $image->getClientOriginalExtension();

//            \Storage::disk('s3')->put($input['file'], file_get_contents($image));
//            \Storage::disk('s3')->setVisibility($input['file'], 'public');

            $meme->file_description = 'upload from pc';

            if ($image->getClientOriginalExtension() == 'gif') {
                $img = time() . '.' . request()->img->getClientOriginalExtension();
                request()->img->move(public_path('storage/uploads/thumbnails'), $img);
                $meme->file = $img;
                $meme->thumbnail = $img;
            } else {
//                $img->resize(500, null, function ($constraint) {
//                    $constraint->aspectRatio();
//                })->save($originalImgPath . '/' . $input['file']);

                $img->save($originalImgPath . '/' . $input['file']);
                $meme->file = $input['file'];

                $img->resize(250, null, function ($constraint) {
                    $constraint->aspectRatio();
                })->save($destinationPath . '/' . $input['file']);
                $img->save($destinationPath . '/' . $input['file']);

                $meme->thumbnail = $input['file'];
            }
        } else {
            $meme->file = $request->imgUrl;
            $meme->file_description = 'from URL';
        }

        //  $meme->uuid = Str::uuid();
        $meme->uuid = substr(md5(time()), 0, 8); //generate random alpha numeric string
        $meme->title = $request->title ? $request->title : null;
        $meme->slug = str_slug($request->title);
        $meme->user_id = Auth::user()->id;
        $meme->category_id = $request->category ? $request->category : null;
        $meme->tags = $request->tags ? $request->tags : null;
        $meme->save();

        return redirect()->back();
    }

    public function memeDetail(Request $request, $slug, $Uuiid)
    {
//        $url = 'https://s3.' . env('AWS_DEFAULT_REGION') . '.amazonaws.com/' . env('AWS_BUCKET') . '/';
//        $files = Storage::disk('s3')->files('images');
//        foreach ($files as $file) {
//            $images[] = [
//                'name' => str_replace('images/', '', $file),
//                'src' => $url . $file
//            ];
//        }
//        return $images;

//        $user = Auth::user();
//
//         $notify = \App\Notification::latest()
//            ->where('user_id', '!=', $user->id)
//            ->where('recieve_by', $user->id)
//            ->get();
//
//        $not = array();
//        $meme_id = '';
//        $action_type = '';
//        $counter = 0;
//        $a = array();
//        foreach ($notify as $key => $value) {
//
//            if (!in_array($key, $a)) {
//                $meme_id = $value['meme_id'];
//                $action_type = $value['type'];
//                $user_id = $value['user_id'];
//
//                foreach ($notify as $key1 => $value1) {
//
//                    if ($value['comment_id'] != null) {
//                        if ($value1['type'] == $value['type'] && $value1['comment_id'] == $value['comment_id']
//                            && $value1['comment_parent_id'] == $value['comment_parent_id']
//                            && $value1['meme_id'] == $value['meme_id']) {
//                            $counter++;
//                            $a[] = $key1;
//                        }
//                        elseif ($value1['type'] == $value['type'] && $value1['comment_id'] !== $value['comment_id']
//                            && $value1['comment_parent_id'] !== $value['comment_parent_id']
//                            && $value1['meme_id'] == $value['meme_id'])
//                        {
//                            $counter++;
//                            $a[] = $key1;
//                        }
//                    } else {
//                        if ($value1['type'] == $value['type'] && $value1['meme_id'] == $value['meme_id']) {
//                            $counter++;
//                            $a[] = $key1;
//                        }
//                    }
//                }
//            } else {
//                continue;
//            }
//
//            if ($counter == 1) {
//                $not[] = "$user_id $action_type on $meme_id";
//            }
//            if ($counter >= 2) {
//                $c = $counter - 1;
//                $not[] = "$user_id & $c other $action_type on $meme_id";
//            }
//            if ($counter = 0) {
//                $not[] = "$user_id $action_type on $meme_id";
//            }
//        }
//        return $not;

        if ($slug) {
            $meme = Meme::where('uuid', $Uuiid)
                ->Where('slug', $slug)
                ->first();

            if ($meme) {
                $MemeRandom = Meme::inRandomOrder()
                    ->where('id', '!=', $meme->id)
                    ->take(1)
                    ->get();
                $upvotes = MemeVote::where('meme_id', $meme->id)
                    ->where('value', 1)
                    ->get()
                    ->count();
                $downvotes = MemeVote::where('meme_id', $meme->id)
                    ->where('value', 0)
                    ->get()
                    ->count();
                $totalComments = Comment::where('meme_id', $meme->id)->get()->count();

                $memeKey = 'meme_' . $meme->id;

                if (!session::has($memeKey)) {
                    $meme->increment('view_count');
                    Session::put($memeKey, 1);
                }

                return view('front.memes.meme_details', compact('totalComments', 'comment', 'likes', 'dislikes', 'upvotes', 'downvotes'))
                    ->with(['memes' => $meme, 'MemeRandom' => $MemeRandom]);
            }

            return redirect()->back();

        }

    }

    public function MemeShareCount(Request $request)
    {
        if ($request->ajax()) {
            $meme = Meme::where('id', $request->meme_id)->first();

            $m = Meme::find($meme->id);
            $m->share_count = $meme->share_count + 1;
            $m->save();

            return $request->json($m);

        }

    }

    public function MemeGallery($id=null)
    {
        $SelectedMeme = GenerateMemeImage::find($id);
        $memes = GenerateMemeImage::select('id', 'file')->get()->take(100);
        return view('front.memes.meme-gallery', compact('memes','SelectedMeme'));
    }

    public function CreateMeme(Request $request)
    {
        if (Auth::check()) {
            if ($request->meme) {
                $validator = Validator::make($request->all(), [
                    'title' => 'required|string|max:255',
                    'tags' => 'required|string|max:255',
                    'category' => 'required',
                ]);
                if ($validator->fails()) {
                    return response()->json([
                        "errors" => $validator->errors()->all(),
                        "status" => 400
                    ]);
                } else {
                    $m = new Meme();
                    $m->uuid = substr(md5(time()), 0, 8); //generate random alpha numeric string
                    $m->title = $request->title ? $request->title : null;
                    $m->slug = str_slug($request->title);
                    $m->user_id = Auth::user()->id;
                    $m->category_id = $request->category ? $request->category : null;
                    $m->tags = $request->tags ? $request->tags : null;
                    $img = Image::make($request->get('meme'));
                    $img->save(public_path('storage/uploads/memes/' . $m->uuid . '.jpg'));
                    $img->save(public_path('storage/uploads/thumbnails/' . $m->uuid . '.jpg'));
                    $m->file = $m->uuid . '.jpg';
                    $m->thumbnail = $m->uuid . '.jpg';
                    $m->file_description = 'upload from pc';
                    $m->save();

                    return response()->json([
                        "status" => 200,
                        "redirectUrl" => url('/')
                    ]);
                }

            } else {
                return response()->json([
                    "status" => 401,
                ]);
            }
        } else {
            return response()->json([
                "status" => 500,
            ]);
        }

    }

    public function generatedMemeStore(Request $request)
    {
        if (Auth::check()) {
            $m = new GenerateMemeImage();
            $m->user_id = Auth::user()->id;
            $uuid = substr(md5(time()), 0, 8);
            $img = Image::make($request->get('meme'));
            $img->save(public_path('storage/uploads/m',$uuid . '.jpg'));
            $m->file = 'm'.$uuid . '.jpg';
            $m->save();

            return response()->json([
                "status" => 200,
            ]);
        }
    }


}
