<?php

namespace App\Http\Controllers;

use App\CommentVote;
use App\MemeVote;
use Illuminate\Http\Request;
use App\Notification;
use App\Comment;
use Auth;

class NotificationsController extends Controller
{
    //change status if notification seen

    public function updateNotifyStatus(Request $request)
    {
        if ($request->ajax()) {
            $user = Auth::user();
            $n = Notification::where('recieve_by', $user->id)->where('status', 1)->update(array('status' => 0,
                'notifyStatus' => 0));

//            return response()->json([
//                "status" => true,
//                "data" => $n,
//                "msg" => "Success"
//            ]);
        }

    }

    //change isRead status if notification seen

    public function updateIsReadStatus(Request $request)
    {
        if ($request->ajax()) {
            $user = Auth::user();

            if ($request->comment_id) {
                $n = Notification::where('meme_id', $request->meme_id)->where('comment_id', $request->comment_id)->where('type', $request->attr)
                    ->where('recieve_by', $user->id)
                    ->where('isRead', 1)->update(array('isRead' => 0));
            } else {
                $n = Notification::where('meme_id', $request->meme_id)->where('type', $request->attr)->where
                ('recieve_by', $user->id)->where('isRead', 1)->update(array('isRead' => 0));
            }


//            return response()->json([
//                "status" => true,
//                "data" => $n,
//                "msg" => "Success"
//            ]);
        }

    }

    public function getAllNotify()
    {
        if (Auth::check()) {

            $user = Auth::user();
            $notifications = \App\Notification::latest()
                ->where('user_id', '!=', $user->id)
                ->where('recieve_by', $user->id)
                ->get();
//            $arr = collect($notify)->groupBy('type');
//
//            foreach ($arr as $group => $value) {
//                if ($value[0]->type == 'Meme Like' || $value[0]->type == 'Comment' )
//                {
//                    $notifications[] = $value->groupBy('meme_id');
//                }
//                elseif ($value[0]->type == 'Comment Like')
//                {
//                    $notifications[] = $value->groupBy('comment_id');
//                }
//                elseif ($value[0]->type == 'Reply Comment')
//                {
//                    $grouped[] = $value->groupBy('comment_parent_id');
//                }
//            }

            if (isset($notifications)) {
                return view('front.notifications.notify_list', compact('notifications'));
            }
            return view('front.notifications.notify_list');

        } else {
            return view('front.errors.404');
        }
    }
}
