<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use App\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class UserController extends Controller
{
    public function signup(Request $request)
    {
        if ($request->isMethod('post')) {
            $validator = Validator::make($request->all(), [
                'name' => 'required|string|max:50',
                'email' => 'required|email|unique:users',
                'password' => 'required|min:8',
            ]);
            if ($validator->fails()) {

//                return redirect('/')
//                    ->withErrors($validator)
//                    ->withInput();
                return response()->json([
                    "errors" => $validator->errors()->all(),
                    "status" => 400
                ]);

            } else {
                $user = new User();
                if ($request->name) {
                    $user->name = str_replace('', '', ucwords(str_replace('', '', strtolower( $request->name))));
                }
                $user->email = $request->email;
                $user->avatar = 'random_profile_pic.jpg';
                $user->password = Hash::make($request->password);
                $user->save();

                if (Auth::attempt(['email' => $request['email'], 'password' => $request['password']])) {
                   // return redirect('/');
                    return response()->json([
                        "success" => true,
                        "status" => 200,
                        "RedirectUrl" => redirect('/')
                    ]);
                }
            }
        } else {
           // return view('/');
            return redirect('/');
        }

    }

    public function login(Request $request)
    {
        if ($request->isMethod('post')) {
            $data = $request->input();

            if ($data) {
                if (Auth::attempt(['email' => $data['email'], 'password' => $data['password']]))
                {
                    return response()->json([
                        "success" => true,
                        "status" => 200,
                        "RedirectUrl" => redirect('/')
                    ]);
                    //return redirect('/');
                }
                else
                    {
                    return response()->json([
                        "errors" => 'Invalid Email or password',
                        "status" => 400
                    ]);
                   // return redirect('/')->with('login-error', 'Invalid Email or password');
                }
            }

        }
        return view('/');
    }

    public function logout()
    {
        Session::flush();
        return redirect('/')->with('logout-success', 'Log-Out Successfully');
    }

}
