<?php

namespace App\Http\Controllers;

use App\Category;
use App\Comment;
use App\Meme;
use App\MemeVote;
use Illuminate\Http\Request;
use App\User;
use DB;

class Dashboard extends Controller
{
    public function dashboard()
    {
        $registerUser = User::count();
        $NewUsers = User::latest()->get()->take(6);
        $totalMemes = Meme::count();
        $totalView = Meme::selectRaw('SUM(view_count) as total_views')->get();
        $totalLikes = MemeVote::where('value',1)->count();
        $totalComments = Comment::count();
        $categories = Category::count();
        $MemehasMaxViews = Meme::Max('view_count')
            ->orderBy('view_count', 'desc')
            ->limit(1)
            ->get();

        return view('admin.dashboard', compact('registerUser', 'totalMemes', 'totalView', 'categories', 'MemehasMaxViews','totalLikes','totalComments','NewUsers'));
    }
}
