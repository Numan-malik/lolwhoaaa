<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MemeVote extends Model
{
    protected $fillable = [
        'value'
    ];

    public function user() {
        return $this->belongsTo(User::class);
    }

    public function meme() {
        return $this->belongsTo(Meme::class);
    }

    public function notifications()
    {
        return $this->hasMany(Notification::class);
    }

}
