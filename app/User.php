<?php

namespace App;


use function foo\func;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Notifications\ResetPassword as ResetPassword;

class User extends Authenticatable
{
    use HasApiTokens , Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function sendPasswordResetNotification($token)
    {
        $this->notify(new ResetPassword($token));
    }

    public function provider()
    {
        return $this->hasOne(SocialProvider::class);
    }
    public function meme()
    {
        return $this->hasMany(Meme::class);
    }
    public function RegisterFrom()
    {
        return $this->hasMany(SocialProvider::class);
    }
    public function comments()
    {
        return $this->hasMany(Comment::class);
    }
    public function votes() {
        return $this->hasMany(MemeVote::class);
    }
    public function commentVotes() {
        return $this->hasMany(CommentVote::class);
    }

    public function AauthAcessToken(){
        return $this->hasMany('\App\OauthAccessToken');
    }

    // public function sendPasswordResetNotification($token)
    // {
    //     $this->notify(new PasswordResetNotification($token));
    // }
}
