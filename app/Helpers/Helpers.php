<?php


use App\Meme;
use App\Category;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Carbon\Carbon;


function recent_memes()
{
    if(\Route::current()->getName() == '')
    {
        return $memes = Meme::latest()->limit(6)->get();
    }
    else
    {
        return $memes = Meme::latest()->limit(6)->get();
    }

}

function trending_memes()
{
    $highestView = Meme::orderBy('view_count', 'desc')
        ->first();
    $lowestView = Meme::orderBy('view_count', 'asc')
        ->first();
    return Meme::where('created_at','>=',Carbon::now()->subdays(10))
                ->where('view_count', '<', $highestView->view_count)
                ->limit(6)->get();
    // if(\Route::current()->getName() == '')
    // {
    //     dd(DB::table('memes')
    //             ->orderBy('created_at', 'desc')
    //             ->get());
    //     return 'hello';
    //     dd(Meme::orderBy('created_at', 'desc')->get());
    //     return $memes = Meme::where('view_count', '<', $highestView->view_count)
    //         ->where('view_count','!=', $lowestView->view_count)
    //         ->limit(6)
    //         ->get();
    // }
    // else
    // {
    //     return $memes = Meme::where('view_count', '<', $highestView->view_count)
    //         ->where('view_count','!=', $lowestView->view_count)
    //         ->limit(6)
    //         ->get();
    // }
}

function categories()
{
    $m = Meme::selectRaw('category_id,COUNT(category_id) AS total')->orderBy('total','desc')
        ->groupBy('category_id')->pluck('category_id');
    return $c = Category::latest()->whereIn('id', $m)
        ->limit(10)
        ->get();
}

function categoryForMemeDetails()
{
    return $categories = Category::latest()->get();
}

function notify()
{
    if (Auth::check()) {
        $user = Auth::user();

        return $notify = \App\Notification::latest()
            ->where('user_id', '!=', $user->id)
            ->where('recieve_by', $user->id)
            ->where('status', 1)
            ->get();

//        $arr = collect($notify)->groupBy('type');
//
//        foreach ($arr as $group => $value)
//        {
//            if ($value[0]->type == 'Meme Like' || $value[0]->type == 'Comment')
//            {
//                $grouped[] = $value->groupBy('meme_id');
//            }
//            elseif ($value[0]->type == 'Comment Like')
//            {
//                $grouped[] = $value->groupBy('comment_id');
//            }
//            elseif ($value[0]->type == 'Reply Comment')
//            {
//                $grouped[] = $value->groupBy('comment_parent_id');
//            }
//        }
//        return $grouped;
    }

}

function SeenNotify()
{
    if (Auth::check()) {
        $user = Auth::user();

       return $notify = \App\Notification::latest()
            ->where('user_id', '!=', $user->id)
            ->where('recieve_by', $user->id)
            ->where('status', 0)
            ->get();
//        $arr = collect($notify)->groupBy('type');
//
//        foreach ($arr as $group => $value) {
//            if ($value[0]->type == 'Meme Like' || $value[0]->type == 'Comment')
//            {
//                $grouped[] = $value->groupBy('meme_id');
//            }
//            elseif ($value[0]->type == 'Comment Like')
//            {
//                $grouped[] = $value->groupBy('comment_id');
//            }
//            elseif ($value[0]->type == 'Reply Comment')
//            {
//                $grouped[] = $value->groupBy('comment_parent_id');
//            }
//        }
//        return $grouped;
    }

}

function notifyCount()
{
    if (Auth::check()) {
        $user = Auth::user();

        return $notify = \App\Notification::selectRaw('meme_id,COUNT(meme_id) as totalCount')
            ->selectRaw('comment_id,COUNT(comment_id) as totalComment')
            ->selectRaw('type,COUNT(type) as total')
            ->where('user_id', '!=', $user->id)
            ->where('recieve_by', $user->id)
            ->where('notifyStatus', 1)
            ->groupBy('meme_id','type','comment_id')
            ->get();
    }

}

?>
