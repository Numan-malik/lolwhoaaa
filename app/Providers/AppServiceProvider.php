<?php

namespace App\Providers;
use Laravel\Passport\Passport;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\URL;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    // protected $policies = [
    //     'App\Model' => 'App\Policies\ModelPolicy',
    // ];

    public function register()
    {


	}

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
	   //URL::forceScheme('https');
        Schema::defaultStringLength(191);

        Passport::routes();
    }

}
