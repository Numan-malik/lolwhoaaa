<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class Category extends Model
{
    protected $table = 'categories';

    public function meme()
    {
        return $this->hasOne(Meme::class);
    }
    public function user()
    {
        return $this->belongsTo(User::class);
    }

}
