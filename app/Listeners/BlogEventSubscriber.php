<?php

namespace App\Listeners;

use App\Notifications\ArticlePublished;

class BlogEventSubscriber
{
    public function onCreated($blog)
    {
        $blog->notify(new ArticlePublished());
    }

    public function subscribe($events)
    {
        $events->listen(
            'eloquent.created: App\Meme',
            'App\Listeners\BlogEventSubscriber@onCreated'
        );
    }

}
