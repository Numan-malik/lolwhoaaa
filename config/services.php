<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Stripe, Mailgun, SparkPost and others. This file provides a sane
    | default location for this type of information, allowing packages
    | to have a conventional place to find your various credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
        'endpoint' => env('MAILGUN_ENDPOINT', 'api.mailgun.net'),
    ],

    'postmark' => [
        'token' => env('POSTMARK_TOKEN'),
    ],

    'ses' => [
        'key' => env('AWS_ACCESS_KEY_ID'),
        'secret' => env('AWS_SECRET_ACCESS_KEY'),
        'region' => env('AWS_DEFAULT_REGION', 'us-east-1'),
    ],

    'sparkpost' => [
        'secret' => env('SPARKPOST_SECRET'),
    ],

    'stripe' => [
        'model' => App\User::class,
        'key' => env('STRIPE_KEY'),
        'secret' => env('STRIPE_SECRET'),
        'webhook' => [
            'secret' => env('STRIPE_WEBHOOK_SECRET'),
            'tolerance' => env('STRIPE_WEBHOOK_TOLERANCE', 300),
        ],
    ],

    'facebook' => [
        'client_id' => '428753677971754',
        'client_secret' => '547d5c87e5551455ee0a8e267f418437',
        'redirect' => 'https://lolwhoa.com/auth/facebook/callback',
    ],
    'google' => [
        'client_id' => '558415095261-al00i2g9qpuctegflhck3vpbismoacuv.apps.googleusercontent.com',
        'client_secret' => '2B9eeLZvpAlDVXuVHO2RVxuu',
        'redirect' => 'https://lolwhoa.com/auth/google/callback'
    ],
    'linkedin' => [
        'client_id' => '77irvtzjpkdyt7',
        'client_secret' => 'QUR1F7kVQsmB9Oin',
        'redirect' => 'https://lolwhoa.com/auth/linkedin/callback'
    ],
    'twitter' => [
        'client_id' => 'YpVBvKJrVAxAnweIxW56uE1hl',
        'client_secret' => 'l2cNobMHGPj47pG77dur7DGYllyoEs9hYqgyHMvUT7yilmiHWq',
        'redirect' => 'https://lolwhoa.com/auth/twitter/callback'
    ],

];
